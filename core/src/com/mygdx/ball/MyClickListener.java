package com.mygdx.ball;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class MyClickListener extends ClickListener {

    private Callable clickAction;

    public MyClickListener(Callable clickAction) {
        this.clickAction = clickAction;
    }

    @Override
    public void clicked(InputEvent event, float x, float y) {
        if (!Main.loading) {
            clickAction.call();
        }
    }
}
