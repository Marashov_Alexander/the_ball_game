package com.mygdx.ball;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Align;
import com.mygdx.ball.Windows.AWindow;
import com.mygdx.ball.Windows.ActorsScrollPane;

import java.util.ArrayList;

public class Manual extends ActorsScrollPane {

    ArrayList<ManualItem> items;
    ArrayList<String> knownItemNames;
    private AWindow helpWindow;

    private float itemWidth;
    private float itemHeight;
    private float itemSpaceBottom;

    public Manual(float width, float height, int textSize, Color textColor) {
        super(width, height, textSize, textColor);
        items = new ArrayList<>();
        itemWidth = width;
        itemHeight = height * 0.4f;
        itemSpaceBottom = 0.1f * height;
        knownItemNames = new ArrayList<>();
//        setDebug(true, true);
    }

    public void setWindow(AWindow helpWindow) {
        this.helpWindow = helpWindow;
    }

    public void addPlusRow(ManualItem item) {
        items.add(item);
        super.addPlusRow(item);
    }

    public void addPlusRow(ManualItem item,
                           float widthProportion, float heightProportion,
                           float spaceBottomProportion, int align) {
        items.add(item);
        super.addPlusRow(item, widthProportion, heightProportion, spaceBottomProportion, align);
    }

    public void addPlusRow(float width, float height,
                           ManualItem item,
                           float spaceBottom, int align) {
        items.add(item);
        super.addPlusRow(width, height, item, spaceBottom, align);
    }

    public void add(ManualItem item,
                    float widthProportion, float heightProportion, float spaceBottomProportion) {
        items.add(item);
        super.add(item, widthProportion, heightProportion, spaceBottomProportion);
    }

    public void focusOn(int itemIndex) {
        if (itemIndex >= items.size() || itemIndex < 0) {
            throw new ArrayIndexOutOfBoundsException("Индекса " + itemIndex + " не существует в справочнике");
        }
        float y = 0;
        for (int i = 0; i < itemIndex; ++i) {
            y += items.get(i).getHeight() + 0.1 * getHeight();
        }
        scrollTo(
                0, getMaxY() - y,
                getWidth(), getHeight()
        );
    }


    public void addItem(Actor actor, Drawable image, String enText, String ruText) {
        if (actor.getName() == null || actor.getName().equals("")) {
            throw new RuntimeException("Попытка добавить объект " + actor.getClass() + " в справочник без имени!");
        }

        int index = -1;
        boolean contains = false;
        String tmp = actor.getName() + "_manual";
        for (int i = 0; i < items.size(); ++i) {
            if (items.get(i).getName().equals(tmp)) {
                index = i;
//                System.out.println("Объект " + actor.getClass() + " уже есть в справочнике!");
                contains = true;
            }
        }

        if (!contains) {
            addKnown(tmp);

            ManualItem manualItem = new ManualItem(
                    itemWidth, itemHeight,
                    80, Color.WHITE, null,
                    image,
                    enText, ruText,
                    Progress.getSubjectInfo(true, actor.getName()), Progress.getSubjectInfo(false, actor.getName()),
                    actor.getName()
            );
            addPlusRow(itemWidth, itemHeight, manualItem, itemSpaceBottom, Align.left);
            index = items.indexOf(manualItem);
        }

        int finalIndex = index;
        actor.addListener(new ActorGestureListener(50f, 0.4f, 1.1f, 0.15f) {
            @Override
            public boolean longPress(Actor actor, float x, float y) {
                helpWindow.show();
                focusOn(finalIndex);
                return super.longPress(actor, x, y);
            }

            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                if (count == 3) {
                    System.out.println("Show " + finalIndex);
                    helpWindow.show();
                    focusOn(finalIndex);
                }
            }
        });
    }

    public void addKnown(String knownItemName) {
        if (!knownItemNames.contains(knownItemName)) {
            knownItemNames.add(knownItemName);
        }
    }
}
