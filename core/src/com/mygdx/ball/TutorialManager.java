package com.mygdx.ball;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.mygdx.ball.actor_classes.ALabel;
import com.mygdx.ball.actor_classes.TextImageButtonGroup;

import static com.mygdx.ball.Main.helpWindow;
import static com.mygdx.ball.Main.screenWidth;

public class TutorialManager {

    TutorialManager() {
        blackImage = new Image(Main.backgroundDrawable);
        tuneBlackImage(0.5f);
        blackImage.setZIndex(BLACK_IMAGE_Z_INDEX);

        label = new ALabel("", "", 60, Color.WHITE, Align.center);
        label.setZIndex(LABEL_Z_INDEX);
        label.setWrap(true);
    }

//    public void startTutorial(Progress progress) {
//        Main.infinityItems = true;
//    }

    public void tuneBlackImage(float alpha) {
        blackImage.setColor(0, 0, 0, alpha);
    }

    // чем будем затемнять остальное?
    // можно добавлять на сцену blackImage с прозрачностью.
    // выделенным актёрам ставить высокий ZIndex
    private final int BIG_Z_INDEX = 1000;
    private final int BLACK_IMAGE_Z_INDEX = 900;
    private Image blackImage;

    private Actor[] activeActors;
    private int[] oldZIndexActors;
    private Group activeGroup;
    private int oldZIndexGroup;
    private Vector2 tmpVector = new Vector2();

    public void setActive(Group group, Actor... actors) {
        if (activeActors != null) {
            resetActive();
        }
        activeActors = actors;
        activeGroup = group;

        oldZIndexActors = new int[actors.length];
        oldZIndexGroup = group.getZIndex();
        group.toFront();
        group.addActor(blackImage);

        tmpVector.x = 0;
        tmpVector.y = 0;
        tmpVector = group.stageToLocalCoordinates(tmpVector);
        blackImage.setBounds(
                tmpVector.x - 1000,
                tmpVector.y - 1000,
                screenWidth * 10,
                Main.screenHeight * 10
        );
        blackImage.setZIndex(BLACK_IMAGE_Z_INDEX);

        for (int i = 0; i < actors.length; ++i) {
            oldZIndexActors[i] = actors[i].getZIndex();
            actors[i].toFront();
        }
    }

    public void resetActive() {
        blackImage.remove();
        blackImage.clearListeners();
        if (activeGroup != null && oldZIndexGroup >= 0) {
            activeGroup.setZIndex(oldZIndexGroup);
            activeGroup = null;
        }
        if (oldZIndexActors != null) {
            for (int i = 0; i < oldZIndexActors.length; ++i) {
                activeActors[i].setZIndex(oldZIndexActors[i]);
            }
        }
        activeActors = null;
        oldZIndexActors = null;
        scriptWorld.resultsWindow.toFront();
    }

    private ALabel label;
    private final int LABEL_Z_INDEX = 950;
    private void showText(String enText, String ruText) {
        label.setText(enText, ruText);
        scriptStage.getRoot().addActor(label);
        label.setZIndex(LABEL_Z_INDEX);
    }

    private void moveText(boolean isBottom) {
        if (isBottom) {
            label.setBounds(scriptWorld.actionsGroup.getX() + scriptWorld.actionsGroup.getWidth() + 20,
                    scriptWorld.actionsGroup.getY(),
                    screenWidth - scriptWorld.actionsGroup.getWidth() - scriptWorld.centerButton.getWidth() - 20, 0);
            label.setAlignment(Align.bottomLeft);
        } else {
            label.setBounds(scriptWorld.actionsGroup.getX() + scriptWorld.actionsGroup.getWidth() + 20,
                    scriptWorld.ballsLeftLabel.getY(),
                    screenWidth - scriptWorld.actionsGroup.getWidth()  - scriptWorld.centerButton.getWidth() - 20, 0);
            label.setAlignment(Align.topLeft);
        }
    }

    private void hideText() {
        label.remove();
        label.setText("");
    }

    int firstLevelIndex(int state) {
        if (state >= simulatorParams.length || simulatorParams[state].length == 0)
            return 1;
        return -(simulatorParams[state].length - 1);
    }

    SimulatorParams[][] simulatorParams = {
            new SimulatorParams[]{
                    new SimulatorParams(
                            1,
                            new int[]{3},
                            new int[]{0},
                            new int[]{4},
                            new int[]{3},
                            5, 5,
                            true, true, true, false
                    ),
                    new SimulatorParams(
                            2,
                            new int[]{0, 0},
                            new int[]{0, 4},
                            new int[]{4, 0},
                            new int[]{1, 1},
                            5, 5,
                            true, false, true, false
                    ),
                    new SimulatorParams(
                            1,
                            new int[]{0},
                            new int[]{4},
                            new int[]{0},
                            new int[]{0},
                            30, 2,
                            false, false, false, false
                    ),
                    new SimulatorParams(
                            1,
                            new int[]{2},
                            new int[]{3},
                            new int[]{4},
                            new int[]{0},
                            5, 5,
                    false, false, false, false
                    ),
                    new SimulatorParams(
                            3,
                            new int[]{0, 1, 2},
                            new int[]{0, 1, 2},
                            new int[]{0, 0, 0},
                            new int[]{0, 0, 0},
                            3, 1,
                            false, false, false, false
                    ),
                    new SimulatorParams(
                            1,
                            new int[]{0},
                            new int[]{1},
                            new int[]{0},
                            new int[]{0},
                            5, 5,
                            false, false, false, false
                    )
            },
            {

            },
            {

            },
            {

            },
            {

            }
    };

    class SimulatorParams {
        int ballsCount;
        int[] ballColors;
        int[] ballCordsX;
        int[] ballCordsY;
        int[] ballSteps;

        int worldSizeX;
        int worldSizeY;

        boolean exitEnabled;
        boolean ballGameEnabled;
        boolean wallEnabled;
        boolean magicEnabled;

        SimulatorParams(int ballsCount, int[] ballColors, int[] ballCordsX, int[] ballCordsY,
                               int[] ballSteps, int worldSizeX, int worldSizeY,
                        boolean exitEnabled, boolean ballGameEnabled, boolean wallEnabled, boolean magicEnabled) {
            this.ballsCount = ballsCount;
            this.ballColors = ballColors;
            this.ballCordsX = ballCordsX;
            this.ballCordsY = ballCordsY;
            this.ballSteps = ballSteps;
            this.worldSizeX = worldSizeX;
            this.worldSizeY = worldSizeY;

            this.exitEnabled = exitEnabled;
            this.ballGameEnabled = ballGameEnabled;
            this.wallEnabled = wallEnabled;
            this.magicEnabled = magicEnabled;
        }
    }

    public void resetScreen() {
//        setZoom(0.15f);
        resetActive();
        hideText();
        tuneBlackImage(0.5f);
    }

    // 1) чёрный экран + сообщение
    // 2) выделить предмет + сообщение
    // 3) выделить клетку + сообщение

    // [stage][level][step]
    private Callable[][][] steps = new Callable[][][] {

            // stage 1
            new Callable[][] {

                    // level 5
                    new Callable[] {
                            () -> {
                                bonusesVisibility(false);
                                setBonus(3, 3, 1, true);
                                moveText(true);
                                worldStep(
                                        "Meet: bonus levels! Now you control the balls, and the phone will try to catch you :)",
                                        "Знакомься: бонусные уровни! Теперь управляешь мячами ты, а телефон будет пытаться поймать тебя :)"
                                );
                            },
                            () -> {
                                worldStep(
                                        "Your task is to help your balls escape (exits will appear in three moves) without getting caught by the computer.",
                                        "Твоя задача - помочь своим мячам сбежать (выходы появятся через три хода), не попавшись при этом компьютеру."
                                );
                            },
                            () -> {
                                cageStep(
                                        "Click on the ball. Each of them has movement points (shown on the left on the buttons).",
                                        "Нажми на мяч. У каждого из них есть очки движения (будут показаны слева на кнопках).",
                                        0, 4
                                );
                            },
                            () -> {
                                cageStep(
                                        "By clicking on the cell next to it, you can move it until you run out of movement points.",
                                        "Нажимая на соседнюю с ним клетку, ты можешь двигать его до тех пор, пока не закончатся очки движения.",
                                        1, 4
                                );
                            },
                            () -> {
                                itemStep(
                                        "If you want to complete all moves ahead of time, click the arrow on the items on the left.",
                                        "Если ты хочешь досрочно завершить все ходы, нажми стрелочку на предметах слева.",
                                        1
                                );
                            },
                            () -> {
                                worldStep(
                                        "In addition to portals, there may be bonuses on the map. Each ball can take one bonus if it stands on it. If the ball is caught, the bonus is lost!",
                                        "Кроме порталов, на карте могут быть бонусы. Каждый мяч может взять по одному бонусу, если встанет на него. Если мяч будет пойман, бонус пропадёт!"
                                );
                            },
                            () -> {
                                worldStep(
                                        "For every runaway ball you get coins and bonuses (if the ball picked them up and if it is not tutorial :) ). I wish you success!",
                                        "За каждый сбежавший мяч ты получаешь монеты и бонусы (если мяч их подобрал и если это не обучение :) ). Желаю успехов!"
                                );
                            }
                    },

                    new Callable[] {
                            () -> {
                                moveText(true);
                                bonusesVisibility(false);
                                messageStep(
                                        "The ball sees the exit-rolls to it! Therefore, if suddenly all the cells you opened were closed at the same time, and some doors appeared on the map, rather catch balls!",
                                        "Мяч видит выход - катится к нему! Поэтому, если вдруг все открытые тобой клетки одновременно закрылись, а на карте появились какие-то дверцы, скорее лови мячи!"
                                );
                            },

                            () -> {
                                messageStep(
                                        "In this game mode, all open cells are closed after three rounds. In this case, each ball will try to get to the nearest exit.",
                                        "В таком режиме игры все открытые клетки закрываются через три раунда. При этом каждый мяч будет стремиться добраться до ближайшего выхода."
                                );
                            },

                            () -> {
                                messageStep(
                                        "Be smart and don't let them escape! If at least one ball reaches the exit, you lose!",
                                        "Прояви смекалку и не дай им сбежать! Если хотя бы один мяч достигнет выхода, ты проиграл!"
                                );
                            },
                    },

                    // level 3
                    new Callable[] {
                            () -> {
                                bonusesVisibility(false);
                                moveText(true);
                                messageStep(
                                        "To learn more about any object in the game, click on it and hold it for a while. If there is information about it, the directory will open (you can also open it by clicking on the question mark on the top right)",
                                        "Чтобы узнать подробнее про любой объект игры - нажми на него и держи некоторое время. Если про него есть информация, откроется справочник (также его можно открыть, нажав на знак вопроса справа сверху)"
                                );
                            },
                            () -> {

                                worldStep(
                                        "And a little hint... If the field does not fit on the screen, do not hesitate to scroll with your finger and zoom in/out:) if anything, the button .Toward the center. normalizes the position of the field on the screen.",
                                        "И ещё, маленькая подсказка... Если поле не помещается на экран, не стесняйся пальцем пролистывать мир ЗА КЛЕТКИ и приближать/отдалять его :) Если что, кнопка .К центру. нормализует положение поля на экране.");
                            },
                            () -> {
                                worldStep(
                                        "For each new stage and level you are given coins-look in the store after each stage - there are useful improvements and bonuses :)",
                                        "За каждую новую стадию и уровень тебе дают монетки - заглядывай в магазин после каждой стадии - там есть полезные улучшения и бонусы :)");
                            },
                            () -> {
                                moveText(false);
                                worldStep("Now go ahead! Find your first ball :)",
                                        "А теперь - вперёд! Найди свой первый мяч :)");
                            }
                    },

                    // level 2
                    new Callable[] {
                            () -> {
                                moveText(false);
                                bonusesVisibility(true);
                                setBonus(2, 2, 3, false);
                                cageStep("Bonuses can be hidden on the cells! Click on the highlighted one...",
                                        "На клетках могут быть спрятаны бонусы! Нажми на выделенную...", 2, 2);
                            },
                            () -> {
                                bonusStep("Great, you found the flashlight! Click on it - it will highlight all the cells with balls, and at the same time indicate their colors.",
                                        "Отлично, ты нашёл фонарик! Нажми на него - он подсветит все клетки с мячами, а заодно и укажет их цвета.", 3);
                            },
                            () -> {
                                cageStep("Business for small! You can find out about other items in the store.",
                                        "Клетка позеленела, значит на ней стоит зелёный мяч... Дело за малым! Про остальные предметы ты можешь узнать в магазине.", 3, 4);
                            }
                    },

                    // level 1
                    new Callable[] {
                            () -> {
                                moveText(true);
                                bonusesVisibility(false);
                                cageStep(
                                        "Balls come in different colors. Each color has its own abilities. Let's open the first cage.",
                                        "Шары бывают разного цвета. У каждого цвета свои способности. Давай откроем первую клетку.",
                                        0, 0
                                );
                            },
                            () -> cageStep(
                                    "You've already seen a normal ball. It just moves at its own speed to the free cells after each of your moves.",
                                    "Обычный шар ты уже видел. Он просто перемещается со своей скоростью на свободные клетки после каждого твоего хода.",
                                    1, 0
                            ),
                            () -> cageStep(
                                    "Blue ball. When hit, it paints the cell in blue, if possible, it moves to any neighboring cell, becoming normal at the same time.",
                                    "Синий шар. Если ты попадёшь в него, то клетка окрасится в синий цвет, а мяч станет обычным и уйдёт на соседнюю клетку - ищи его неподалёку!",
                                    2, 0
                            ),
                            () -> worldStep(
                                    "Green ball. It can even move through activated cells by pressing them. However, it is easier to detect it this way!",
                                    "Зелёный шар. Может перемещаться даже по активированным клеткам, отжимая их. Однако, его так проще обнаружить!"
                            ),
                            () -> worldStep(
                                    "In the first stage, these are all the balls that you will meet, but in the next you will find more interesting ones! Good luck:) (the description of all the balls will open when you click on the question mark on the top right)",
                                    "На первой стадии это все шары, которые встретятся тебе, однако на следующих тебя ждут более интересные! Удачи :)"
                            )
                    },
                    // level 0
                    new Callable[] {
                            () -> {
                                moveText(true);
                                bonusesVisibility(false);
                                messageStep("Hello! It is tutorial. The arrow on the top right allows you to skip a level.",
                                        "Привет! Это обучение. Стрелка справа сверху позволит пропустить уровень.");
                            },
                            () -> {
                                moveText(false);
                                messageStep(
                                        "In front of you is a playing field where balls are hidden. The goal of the game is to find them all with a limited number of items. Are you ready?",
                                        "Перед тобой игровое поле, на котором прячутся мячики. Задача игры - найти их всех при ограниченном количестве предметов. Ты готов?"
                                );
                            },
                            () -> itemStep(
                                    "Great! First, select the search method. Let's start with the arrow - it will show you the direction to the nearest ball.",
                                    "Отлично! Для начала выбери способ поиска. Начнём со стрелки - она укажет тебе направление к ближайшему мячу.",
                                    1
                            ),
                            () -> cageStep(
                                    "Method selected! Now double-click on the selected cell...",
                                    "Способ выбран! Теперь нажми два раза на выделенную клетку...",
                                    2, 2
                            ),
                            () -> cageStep(
                                    "Well, the ball is somewhere below! What will the next cell tell us?",
                                    "Хорошо, мяч где-то ниже! Что же нам скажет следующая клетка?",
                                    1, 1
                            ),
                            () -> itemStep(
                                    "Even lower! Now let's try another search method-the ruler.",
                                    "Ага, ещё ниже! Теперь давай попробуем другой способ поиска - линейку.",
                                    2
                            ),
                            () -> cageStep(
                                    "The ruler will show you the distance to the nearest ball. Don't forget that in the real level, balls can move to free cells!",
                                    "Линейка покажет тебе расстояние до ближайшего мяча. Не забывай, что в реальном уровне мячи могут перемещаться на свободные клетки!",
                                    0, 0
                            ),
                            () -> itemStep(
                                    "Hm... Distance in one cell. The ball is either higher or to the right. Judging by the previous arrow, it should be to the right! Let's try the last method...",
                                    "Хм... Расстояние в одну клетку. Мяч либо выше,  либо правее. Судя по предыдущей стрелочке, он должен быть правее! Давай опробуем последний способ...",
                                    0
                            ),
                            () -> cageStep(
                                    "This is a \"finger\", it will just open the selected cell for you. A great option when you know where the ball is or want to try your luck :)",
                                    "Это - \"палец\", он просто откроет тебе выбранную клетку. Отличный вариант,когда знаешь, где находится мяч или хочешь попытать удачу :)",
                                    1, 0
                            ),
                            () -> worldStep(
                                    "Here he is! But there is still a lot of interesting things ahead of you...",
                                    "Вот он и попался! Но впереди тебя ждёт ещё много интересного..."
                            )
                    }
            },

            new Callable[][] {

            },

            // stage 3
            new Callable[][] {

            },

            new Callable[][] {

            },

            // stage 5
            new Callable[][] {

                    // level 0

            },
    };

    private void bonusesVisibility(boolean visibility) {
        for (TextImageButtonGroup bonus : scriptWorld.bonuses) {
            bonus.setVisible(visibility);
        }
        if (!visibility) {
            for (Cage[] cages : scriptWorld.world) {
                for (Cage cage : cages) {
                    if (cage.cageType == Cage.CageType.bonus) {
                        cage.clearBonus();
                    }
                }
            }
        }
    }

    private void setBonus(int x, int y, int bonusIndex, boolean ballGame) {
        scriptWorld.world[x][y].setBonus(bonusIndex, ballGame);
    }

//    private void setZoom(float scale) {
//        float scaleFactor = (0.025f * screenWidth + (0.2f - 0.025f) * screenWidth * scale) / scriptWorld.world[0][0].getWidth();
//        scriptWorld.worldGroup.setScale(scaleFactor);
//    }

    private Stage scriptStage;
    private World scriptWorld;
    private int stageIndex;
    private int levelIndex;
    private int stepIndex;

    void programmeWorld(Stage stage, World world, Progress progress) {
        this.scriptStage = stage;
        this.scriptWorld = world;

        stageIndex = progress.getState(true);
        levelIndex = Math.abs(progress.getLevel());
        stepIndex = 0;

        nextStep();
    }

    private void nextStep() {
        System.out.println("STEP");
        resetScreen();

        scriptWorld.resultsWindow.toFront();

        if (stepIndex < steps[stageIndex][levelIndex].length) {
            steps[stageIndex][levelIndex][stepIndex++].call();
            scriptWorld.helpButton.toFront();
            scriptWorld.exitButton.toFront();
            scriptWorld.skipButton.toFront();
        } else {

        }

        helpWindow.toFront();
        scriptWorld.exitConfirmation.toFront();
    }

    private void messageStep(String enText, String ruText) {
        System.out.println("message step");

        // всё чёрным
        setActive(scriptStage.getRoot());

        // сообщение
        showText(enText + " Tap to the screen to continue...", ruText + " Нажми на экран для продолжения...");

        blackImage.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                System.out.println("message step triggered");
                blackImage.clearListeners();
                nextStep();
            }
        });
    }

    private void itemStep(String enText, String ruText, int itemIndex) {
        System.out.println("item step");
        setActive(scriptWorld.actionsGroup, scriptWorld.actions[itemIndex]);
        showText(enText, ruText);
        scriptWorld.actions[itemIndex].addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                System.out.println("item step triggered");
                scriptWorld.actions[itemIndex].clearListeners();
                nextStep();
            }
        });
    }

    private void cageStep(String enText, String ruText, int indexX, int indexY) {
        System.out.println("cage step");
        if (Progress.ballGame) {
            setActive(scriptWorld.worldGroup, scriptWorld.world[indexX][indexY], scriptWorld.balls.get(0));

            scriptWorld.balls.get(0).addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    System.out.println("cage step triggered");
                    nextStep();
                    scriptWorld.balls.get(0).toFront();
                }
            });
        } else {
            setActive(scriptWorld.worldGroup, scriptWorld.world[indexX][indexY]);

            scriptWorld.world[indexX][indexY].addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    if (Progress.ballGame || scriptWorld.world[indexX][indexY].button.isChecked()) {
                        System.out.println("cage step triggered");
                        scriptWorld.world[indexX][indexY].clearListeners();
                        nextStep();
                    }
                }
            });
        }
        showText(enText, ruText);
    }

    private void worldStep(String enText, String ruText) {
        System.out.println("world step");
        if (scriptWorld.ballsLeft == 0) {
            scriptWorld.resultsWindow.hide();
        }
        tuneBlackImage(1);
        setActive(scriptStage.getRoot(), scriptWorld.worldGroup);
        showText(enText + " Tap to continue...", ruText + " Нажми для продолжения...");
        scriptWorld.worldGroup.setTouchable(Touchable.disabled);
        blackImage.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                System.out.println("world step triggered");
                if (scriptWorld.ballsLeft == 0) {
                    scriptWorld.resultsWindow.show();
                }
                scriptWorld.worldGroup.setTouchable(Touchable.enabled);
                blackImage.clearListeners();
                nextStep();
            }
        });
    }

    private void bonusStep(String enText, String ruText, int bonusIndex) {
        System.out.println("Bonus step");
        setActive(scriptStage.getRoot(), scriptWorld.bonuses[bonusIndex]);
        showText(enText, ruText);
        scriptWorld.bonuses[bonusIndex].label.setText("1 ");
        scriptWorld.bonuses[bonusIndex].addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                System.out.println("Bonus step triggered");
                scriptWorld.bonuses[bonusIndex].label.setText("");
                scriptWorld.bonuses[bonusIndex].clearListeners();
                nextStep();
            }
        });
    }

}
