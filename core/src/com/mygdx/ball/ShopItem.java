package com.mygdx.ball;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Align;
import com.mygdx.ball.actor_classes.ALabel;
import com.mygdx.ball.actor_classes.ATable;
import com.mygdx.ball.actor_classes.TextImageButtonGroup;

class ShopItem extends ATable {

    static SpriteDrawable COIN_DRAWABLE;
    TextImageButtonGroup costLabel;
    private ALabel descriptionLabel;
    TextImageButtonGroup imageButton;

    ShopItem(float width, float height, String enDescription, String ruDescription, int textSize, Color textColor, int cost,
             Drawable image, InputListener listener) {
        super();
        setBounds(0, 0, width, height);
//        setDebug(true);
        costLabel = new TextImageButtonGroup(COIN_DRAWABLE, null, null, null,
                0, 0, height, height, "" + cost, "" + cost, textSize, Color.BLACK, Align.center, "");
        costLabel.button.setUpDrawable(null);
        costLabel.button.setTouchable(Touchable.disabled);
        descriptionLabel = new ALabel(enDescription, ruDescription, textSize, textColor, Align.left);
        imageButton = new TextImageButtonGroup(
                image, null, null, listener,
                0, 0, height, height, "", "", 50, Color.BLACK, Align.bottomRight, null
        );
        add(costLabel, height, height).space(30).align(Align.left);
        add(imageButton, height, height).space(30).align(Align.left);
        add(descriptionLabel, width - 2 * height - 120, height).space(30).align(Align.left);
        descriptionLabel.setWrap(true);
    }

    public void setItem(String enDescription, String ruDescription, int cost,
                        Drawable image) {
        costLabel.label.setText(cost);
        descriptionLabel.setText(enDescription, ruDescription);
        imageButton.setImage(image);
    }

    public void setText(int bonusCount) {
        imageButton.label.setText(bonusCount + " ");
    }
}
