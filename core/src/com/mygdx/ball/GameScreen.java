package com.mygdx.ball;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.mygdx.ball.Windows.AWindow;
import com.mygdx.ball.Windows.TextScrollPane;
import com.mygdx.ball.Windows.YesNoScrollPane;
import com.mygdx.ball.actor_classes.ADrawable;
import com.mygdx.ball.actor_classes.AImageButton;
import com.mygdx.ball.actor_classes.AImageTextButton;
import com.mygdx.ball.actor_classes.ALabel;
import com.mygdx.ball.actor_classes.LevelResults;
import com.mygdx.ball.actor_classes.TextImageButtonGroup;

import static com.mygdx.ball.Cage.BUTTONS_COLOR;
import static com.mygdx.ball.Main.ARROW_PATH_RIGHT;
import static com.mygdx.ball.Main.EXIT_ICON_PATH;
import static com.mygdx.ball.Main.HELP_ICON_PATH;
import static com.mygdx.ball.Main.helpWindow;
import static com.mygdx.ball.Main.manual;
import static com.mygdx.ball.Main.screenHeight;
import static com.mygdx.ball.Main.screenWidth;

class GameScreen implements Screen {
    private Main main;
    private Stage stage;
    private World world;
    private Progress progress;
    private float groupScale;
    private float size;
    private Vector2 tmpVector;

    GameScreen(Main main, Progress progress) {

        this.progress = progress;
        progress.time = 0;
        this.size = progress.getCageSize();
        this.main = main;
        tmpVector = new Vector2();
        stage = new Stage(new StretchViewport(screenWidth, screenHeight, main.camera)) {
            @Override
            public boolean keyDown(int keyCode) {
                if(keyCode == Input.Keys.BACK){
                    if (helpWindow.isShowed()) {
                        helpWindow.hide();
                    } else if (world.resultsWindow.isShowed()) {
                        if (world.ballsLeft == 0)
                            progress.levelUp(world.starsCount, world.reward);
                        main.setScreen(Main.menuScreen);
                    } else if (world.exitConfirmation.isShowed()) {
                        world.exitConfirmation.hide();
                    } else {
                        world.exitConfirmation.show();
                    }
                    return true;
                }
                return super.keyDown(keyCode);
            }
        };

        // масштабирование мира
        stage.addListener(new ActorGestureListener() {
            @Override
            public void zoom(InputEvent event, float initialDistance, float distance) {
                if (event.getStageX() > world.actionsGroup.getWidth() * 1.5f 
                        && event.getStageX() < screenWidth - world.actionsGroup.getWidth() / 1.5f 
                        && event.getStageY() > world.actionsGroup.getWidth() / 1.5f 
                        && event.getStageY() < screenHeight - world.actionsGroup.getWidth() / 1.5f) {
                    final float scaleFactor = groupScale * distance / initialDistance;
                    if (scaleFactor * size > 0.025f * screenWidth
                            && scaleFactor * size < 0.2f * screenWidth) {
                        world.worldGroup.setOrigin(
                                world.worldGroup.getWidth() / 2f,
                                world.worldGroup.getHeight() / 2f
                        );
                        world.worldGroup.setScale(scaleFactor);
                    }
                }
            }

            @Override
            public void touchDown(InputEvent event, float x, float y, int pointer, int button) {
                groupScale = world.worldGroup.getScaleX();
            }
        });

        world = new World(main, progress);

        // перемещение мира
        world.worldGroup.addListener(new ActorGestureListener() {
            @Override
            public void pan(InputEvent event, float x, float y, float deltaX, float deltaY) {
                tmpVector.x = x;
                tmpVector.y = y;
                tmpVector = world.worldGroup.localToStageCoordinates(tmpVector);
                if (tmpVector.x > world.actionsGroup.getWidth() * 1.5f && tmpVector.x < screenWidth - world.actionsGroup.getWidth() / 1.5f) {
                    world.worldGroup.moveBy(deltaX * world.worldGroup.getScaleX(), 0);
                }
                if (tmpVector.y > world.actionsGroup.getWidth() / 1.5f && tmpVector.y < screenHeight - world.actionsGroup.getWidth() / 1.5f) {
                    world.worldGroup.moveBy(0, deltaY * world.worldGroup.getScaleY());
                }
            }
        });

        final float worldGroupWidth = progress.getSizeX() * (progress.getCageSize() + progress.getSpaceSize());
        final float worldGroupHeight = progress.getSizeY() * (progress.getCageSize() + progress.getSpaceSize());

        world.actionsGroup = new Group();
        world.actionsGroup.setBounds(0, 0, screenWidth * 0.2f, screenHeight);

        world.worldGroup.setBounds(
                (screenWidth - worldGroupWidth + world.actionsGroup.getWidth()) / 2f,
                (screenHeight - worldGroupHeight) / 2f,
                worldGroupWidth,
                worldGroupHeight
        );

        // способ выбора - определяется объектом progress
        Image background = new Image(Main.assetManager.get(Main.PLATE_PATH, Texture.class));
        background.setBounds(0, 0, world.actionsGroup.getWidth(), world.actionsGroup.getHeight());
        world.actionsGroup.addActor(background);
        world.actions = new TextImageButtonGroup[3];

        world.balls = progress.getBallsAndCalculateActionsCount(world.world);

        for (int i = 0; i < world.actions.length; ++i) {
            world.actions[i] = new TextImageButtonGroup(
                    Progress.getActionDrawable(i), null, null, null,
                    world.actionsGroup.getWidth() / 5f,
                    world.actionsGroup.getHeight() / 3f * i + 50,
                    world.actionsGroup.getWidth() * 3f / 5f,
                    world.actionsGroup.getWidth() * 3f / 5f,
                    progress.actions[i].remains + " ", progress.actions[i].remains + " ",
                    48, Color.BLACK, Align.bottomRight, Progress.getItemName(!Main.isRussian, i)
            );
            world.actions[i].button.setCheckedDrawable(new ADrawable(new Sprite(Main.assetManager.get(Main.BUTTON_UP_PATH, Texture.class)), new Color(1f, 0.78f, 0.50f, 1f)));
            world.actions[i].button.setColor(BUTTONS_COLOR);
            world.actions[i].addListener(new MyClickListener(() -> {
                Main.musicManager.playSound(MusicManager.SoundTrack.cageTouch);
                Main.vibrate(25);
            }));
            manual.addItem(
                    world.actions[i],
                    world.actions[i].imageUp,
                    "", ""
            );
//            world.actions[i].button.addListener();
            world.actions[i].button.setDisabled(false);
            world.actionsGroup.addActor(world.actions[i]);
        }

        if (!Progress.ballGame)
            world.actions[0].button.setChecked(true);
        Button[] buttons = new Button[3];
        buttons[0] = world.actions[0].button;
        buttons[1] = world.actions[1].button;
        buttons[2] = world.actions[2].button;
        world.buttonGroup = new ButtonGroup<>(buttons);

        if (Progress.ballGame) {
            world.sumStep = 0;
            for (Ball ball : world.balls) {
                world.programmeBall(ball);
            }
        }

        world.ballsLeft = world.balls.size();
        for (Ball ball : world.balls) {
            world.worldGroup.addActor(ball);
        }

        Sprite sprite = new Sprite(Main.assetManager.get(Main.BACKGROUND_PATH, Texture.class));
        stage.addActor(new Image(new SpriteDrawable(sprite)));
        stage.addActor(world.worldGroup);
        stage.addActor(world.actionsGroup);

        world.centerButton = new AImageTextButton("To center", "К центру", 80, Color.BLACK, new MyClickListener(() -> {
            world.worldGroup.setScale(1);
            world.worldGroup.setOrigin(Align.center);
            world.worldGroup.setPosition((screenWidth - worldGroupWidth + world.actionsGroup.getWidth()) / 2f, (screenHeight - worldGroupHeight) / 2f);
        }));
        world.centerButton.setColor(BUTTONS_COLOR);
        world.centerButton.setSize(400, 120);

        AImageTextButton levelLabel = new AImageTextButton(
                progress.enGetProgress(false),
                progress.ruGetProgress(false),
                65, Color.BLACK, Align.center
        );
        levelLabel.setColor(BUTTONS_COLOR);
        levelLabel.setSize(400, 120);
        levelLabel.setPosition(world.actionsGroup.getWidth(), screenHeight - levelLabel.getHeight());
        stage.addActor(levelLabel);

        world.ballsLeftLabel = new AImageTextButton(
                "Balls: " + world.ballsLeft,
                "Мячей: " + world.ballsLeft,
                65, Color.BLACK,
                Align.center);
        world.ballsLeftLabel.setColor(BUTTONS_COLOR);
        world.ballsLeftLabel.setSize(350, 120);
        world.ballsLeftLabel.setPosition(world.actionsGroup.getWidth() + levelLabel.getWidth(), screenHeight - world.ballsLeftLabel.getHeight());

        world.centerButton.setPosition(screenWidth - world.centerButton.getWidth(), 0);
        stage.addActor(world.centerButton);
        stage.addActor(world.ballsLeftLabel);

        world.timeLabel = new AImageTextButton(
                "0", "0", 80, Color.BLACK, Align.center
        );
        world.timeLabel.setColor(BUTTONS_COLOR);
        world.timeLabel.setSize(300, 120);
        world.timeLabel.setPosition(world.actionsGroup.getWidth() + levelLabel.getWidth() + world.ballsLeftLabel.getWidth(), screenHeight - world.timeLabel.getHeight());
        stage.addActor(world.timeLabel);

        world.exitButton = new AImageTextButton(
                "", "", 80, Color.BLACK,
                () -> world.exitConfirmation.show(),
                new SpriteDrawable(new Sprite(Main.assetManager.get(EXIT_ICON_PATH, Texture.class))),
                null,
                null
        );

        world.helpButton = new AImageTextButton(
                "", "", 80, Color.BLACK,
                () -> helpWindow.show(),
                new SpriteDrawable(new Sprite(Main.assetManager.get(HELP_ICON_PATH, Texture.class))),
                null,
                null
        );

        world.skipButton = new AImageTextButton(
                "", "", 80, Color.BLACK,
                () -> {
                    System.out.println("wow.");
                    Progress.tutorialManager.resetScreen();
                    world.winGame();
                },
                new SpriteDrawable(new Sprite(Main.assetManager.get(ARROW_PATH_RIGHT, Texture.class))),
                null,
                null
        );

        world.exitButton.setBounds(screenWidth - 120, screenHeight - 120, 120, 120);
        world.helpButton.setBounds(screenWidth - 120, screenHeight - 240, 120, 120);
        world.skipButton.setBounds(screenWidth - 120, screenHeight - 360, 120, 120);
        stage.addActor(world.exitButton);
        stage.addActor(world.helpButton);
        stage.addActor(world.skipButton);

        world.bonuses = new TextImageButtonGroup[Progress.FREE_ITEM_INDEX];
        final float bonusesX = world.actionsGroup.getWidth() + 6;
        final float bonusesY = 6;
        final float bonusesSize = 200;
        final float bonusesOffset = 12;
        for (int i = 0; i < Progress.FREE_ITEM_INDEX; ++i) {
            int finalI = i;
            world.bonuses[i] = new TextImageButtonGroup(
                    Progress.getBonusDrawable(i),
                    null,
                    null,
                    new MyClickListener(() -> {
                        if (helpWindow.isShowed()) {
                            System.out.println("Ложная тревога");
                            return;
                        }
                        world.useBonus(finalI);
                    }),
                    bonusesX + i * (bonusesSize + bonusesOffset),
                    bonusesY,
                    bonusesSize, bonusesSize,
                    progress.getLevel() > 0 ? progress.getBonusCount(i) + " " : "", progress.getLevel() > 0 ? progress.getBonusCount(i) + " " : "",
                    60, Color.BLACK, Align.bottomRight, Progress.getBonusName(!Main.isRussian, i)
            );
            world.bonuses[i].button.setColor(BUTTONS_COLOR);
            manual.addItem(
                    world.bonuses[i],
                    world.bonuses[i].imageUp,
                    "", ""
            );
            stage.addActor(world.bonuses[i]);
        }

        world.resultsWindow = new AWindow(
                "You won!", "Вы победили!", "", "",
                world.resultScrollPane = new TextScrollPane("", "", Align.center),
                1f, 1f, true, 120, 120, 120, 120, 60, 60
        );
        world.resultsWindow.addBtnAction(new MyClickListener(() -> {

            if (world.ballsLeft == 0)
                progress.levelUp(world.starsCount, world.reward);
            main.setScreen(Main.menuScreen);
        }));
        world.resultsWindow.addActor(world.levelResults = new LevelResults(
                430, 220,
                1150, 580));

        ALabel itemsLeftLabel = new ALabel("Items left:", "   Остаток:", 70, Color.WHITE, Align.left);
        itemsLeftLabel.setBounds(world.levelResults.getX() / 2.7f, world.levelResults.getY() + world.levelResults.getHeight() / 1.5f, 0, 0);
        world.resultsWindow.addActor(itemsLeftLabel);

        ALabel levelRating = new ALabel("Level rating:", "     Рейтинг:", 70, Color.WHITE, Align.left);
        levelRating.setBounds(world.levelResults.getX() / 4f, world.levelResults.getY() + world.levelResults.getHeight() / 5.7f, 0, 0);
        world.resultsWindow.addActor(levelRating);

        float arrowWidth = 125;
        float arrowHeight = 360;
        world.arrRight = new AImageButton(
                new ADrawable(
                        new Sprite(Main.assetManager.get(Main.SLIDER_PATH, Texture.class)),
                        arrowWidth, arrowHeight,
                        Color.WHITE,
                        false, false
                ),
                new ADrawable(
                        new Sprite(Main.assetManager.get(Main.SLIDER_PATH, Texture.class)),
                        arrowWidth, arrowHeight,
                        Color.BLUE,
                        false, false
                ),
                () -> {
                    progress.levelUp(world.starsCount, world.reward);
                    main.setScreen(new GameScreen(main, progress));
                });
        world.arrRight.setBounds(1670, 410, arrowWidth, arrowHeight);

        world.resultsWindow.addActor(world.arrRight);
        stage.addActor(world.resultsWindow);

        world.exitConfirmation = new AWindow(
                "Confirmation", "Подтверждение",
                new YesNoScrollPane(
                        "Do you really want to leave this game? The current level will have to start over.", "Вы хотите покинуть игру? Текущий уровень придётся начать сначала.",
                        () -> main.setScreen(Main.menuScreen),
                        () -> world.exitConfirmation.hide()
                ),
                0.75f, 0.75f,
                true
        );
        stage.addActor(world.exitConfirmation);
        levelLabel.addListener(new MyClickListener(() -> {
            helpWindow.show();
        }));
        stage.addActor(helpWindow);

        // включить обучение, если уровень не положительный
        if (progress.getLevel() <= 0) {
            Main.infinityItems = true;
            Progress.tutorialManager.programmeWorld(stage, world, progress);

            if (Progress.ballGame) {
                world.focusBall();
                world.actions[1].button.addListener(new MyClickListener(() -> {
                    world.makeBallGameStep();
                }));
                for (TextImageButtonGroup bonus : world.bonuses) {
                    bonus.setVisible(false);
                }
            }

        } else {
            Main.infinityItems = false;
            if (Progress.ballGame) {
                world.focusBall();
                world.actions[1].button.addListener(new MyClickListener(() -> {
                    world.makeBallGameStep();
                }));
                for (TextImageButtonGroup bonus : world.bonuses) {
                    bonus.setVisible(false);
                }
                for (int i = 0; i < progress.getBallGameBonusCount(); ++i) {
                    int rx = progress.getRandomX(), ry = progress.getRandomY();
                    world.world[rx][ry].setBonus();
                }
            }
        }
        world.skipButton.setVisible(Main.infinityItems);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
        Main.musicManager.playMusic(MusicManager.Melody.game);
    }

    @Override
    public void render(float delta) {
        progress.time += delta;
        main.readyWhite();
        stage.act();
        stage.draw();
        if (!Main.infinityItems) {
            world.timeLabel.setText("" + (int) progress.time);
        } else {
            world.timeLabel.setText("-");
        }

        if (Progress.ballStrategyGame) {
            world.playerAI.strategyStep(delta);
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {
        Main.musicManager.playMusic(MusicManager.Melody.game);
    }

    @Override
    public void hide() {
        Main.musicManager.stopMusic();
    }

    @Override
    public void dispose() {
        System.out.println("Disposed");
    }
}
