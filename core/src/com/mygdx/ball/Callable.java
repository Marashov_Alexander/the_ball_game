package com.mygdx.ball;

public interface Callable {
    void call();
}
