package com.mygdx.ball;

import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

public class PlayerAI {
    private World world;
    private World.NearestBall currentGoal;
    private ArrayList<Cage> possibleCages;
    private Cage prevCage;

    public PlayerAI(World world) {
        this.world = world;
        this.possibleCages = new ArrayList<>();
        this.hintBalls = new CopyOnWriteArrayList<>();
        attackTime = 0;
        delay = 0f;
    }

    public void makeStep() {
        ballGameStep();
    }

    private final float ATTACK_TIME = 10f;
    private final float DELAY_TIME = 1.5f;
    private float attackTime;
    private float delay;
    private Ball ballUnderAttack;
    private CopyOnWriteArrayList<Ball> hintBalls;

    void addHintBall(Ball ball) {
        hintBalls.add(ball);
    }

    void strategyStep(float delta) {
        if (delay >= 0) {
            delay -= delta;
            return;
        }
        delay = DELAY_TIME;

        if (attackTime <= 0) {
            if (ballUnderAttack != null) {
                hintBalls.remove(ballUnderAttack);
            }
        }

        if (hintBalls.isEmpty()) {
            // нет подсказок - тыкаем куда угодно, в свободные клетки

            int xGoal;
            int yGoal;
            try {
                do {
                    xGoal = world.progress.getRandomX();
                    yGoal = world.progress.getRandomY();
                } while (world.world[xGoal][yGoal].isChecked() && !world.world[xGoal][yGoal].cageType.equals(Cage.CageType.safe));

                world.botActivateUsedItem(0, xGoal, yGoal);

                if (world.world[xGoal][yGoal].usedItem != Cage.UsedItem.bomb) {
                    world.hitCage(xGoal, yGoal);
                }

            } catch (Exception e) {
                System.out.println("Blyat =(\n" + e.toString());
            }

        } else {
            attackTime -= delta;

            if (ballUnderAttack == null || attackTime <= 0f) {
                chooseTheGoal();
            }

            int newDistance = (int) (Math.random() * 5);
            possibleCages.clear();
            for (Cage[] cages : world.world) {
                for (Cage cage : cages) {
                    if (world.getDist(cage.xIndex, cage.yIndex, ballUnderAttack.x, ballUnderAttack.y) <= newDistance
                            && cage.cageType != Cage.CageType.safe && cage.cageType != Cage.CageType.exit
                            && !cage.isChecked()) {
                        possibleCages.add(cage);
                    }
                }
            }
            if (possibleCages.isEmpty()) {
                return;
            }
            Cage chosenCage = possibleCages.get((int) (Math.random() * possibleCages.size()) % possibleCages.size());
            world.botActivateUsedItem(0, chosenCage.xIndex, chosenCage.yIndex);

            if (chosenCage.usedItem != Cage.UsedItem.bomb) {
                chosenCage.button.setChecked(true);
                world.hitCage(chosenCage.xIndex, chosenCage.yIndex);
            }

            if (newDistance == 0) {
                hintBalls.remove(ballUnderAttack);
                ballUnderAttack = null;
            }
        }
    }

    private void chooseTheGoal() {
        ballUnderAttack = hintBalls.get((int) (Math.random() * hintBalls.size()) % hintBalls.size());
        attackTime = (float) Math.random() * ATTACK_TIME;
    }

    private void ballGameStep() {
        if (currentGoal == null || Main.infinityItems) {
            // цели нет - будем случайно тыкать

            int xGoal;
            int yGoal;
            try {
                do {
                    xGoal = world.progress.getRandomX();
                    yGoal = world.progress.getRandomY();
                } while (world.world[xGoal][yGoal].button.isChecked());

                world.botActivateUsedItem(1, xGoal, yGoal);

                if (world.world[xGoal][yGoal].usedItem != Cage.UsedItem.bomb) {
                    world.hitCage(xGoal, yGoal);
                }
                currentGoal = world.getNearestBall(xGoal, yGoal);
                prevCage = world.world[xGoal][yGoal];

            } catch (Exception e) {
                System.out.println("Blyat =(\n" + e.toString());
            }

        } else {
            int newDistance = Math.max(0, world.getDist(prevCage.xIndex, prevCage.yIndex, currentGoal.ball.x, currentGoal.ball.y) - Math.max((int) (Math.random() * currentGoal.dist / 2f), 1));
            possibleCages.clear();
            for (Cage[] cages : world.world) {
                for (Cage cage : cages) {
                    if (world.getDist(cage.xIndex, cage.yIndex, currentGoal.ball.x, currentGoal.ball.y) <= newDistance) {
                        possibleCages.add(cage);
                    }
                }
            }
            Cage chosenCage = possibleCages.get((int) (Math.random() * possibleCages.size()) % possibleCages.size());
            currentGoal = world.getNearestBall(chosenCage.xIndex, chosenCage.yIndex);
            int itemIndex;
            if (currentGoal.dist <= 2 && Progress.getPower(0) > 0 && Math.random() > 0.5) {
                itemIndex = 0;
            } else {
                itemIndex = (int) Math.min(2, Math.round(Math.random() + 1));
            }

            world.botActivateUsedItem(itemIndex, chosenCage.xIndex, chosenCage.yIndex);

            if (chosenCage.usedItem != Cage.UsedItem.bomb) {
                chosenCage.button.setChecked(true);
                world.hitCage(chosenCage.xIndex, chosenCage.yIndex);
            }

            if (newDistance == 0 || currentGoal.ball == null) {
                currentGoal = null;
            }

            prevCage = chosenCage;
        }
    }
}
