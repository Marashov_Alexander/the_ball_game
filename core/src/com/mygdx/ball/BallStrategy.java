package com.mygdx.ball;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

class BallStrategy {

    private final static int INFINITY = 10000;

    private int[][] dist;
    private World world;
    private DistHeap heap;
    private boolean debug = false;

    BallStrategy(World world, int sizeX, int sizeY) {
        this.world = world;
        dist = new int[sizeX][];
        heap = new DistHeap(sizeX * sizeY, dist);
        for (int i = 0; i < sizeX; ++i) {
            dist[i] = new int[sizeY];
            for (int j = 0; j < sizeY; ++j) {
                dist[i][j] = INFINITY;
            }
        }
    }

    Action[] getActions(Ball ball, Cage to) {

        if (debug) {
            System.out.println("DEBUG");
        }

        ball.world = world;
        resetDist();
        heap.clear();
        ball.wantToDoMagic = false;

        Cage start = world.world[ball.x][ball.y];
        heap.insert(start);
        dist[ball.x][ball.y] = 0;

        while (!heap.isEmpty()) {
            Cage current = heap.extractMin();
            int x = current.xIndex, y = current.yIndex;

            if (x - 1 >= 0 && canBallMove(ball, world.world[x - 1][y]) && dist[x - 1][y] > dist[x][y] + 1) {
                dist[x - 1][y] = dist[x][y] + 1;
                if (world.world[x - 1][y].equals(to)) {
                    break;
                }
                heap.insert(world.world[x - 1][y]);
            }

            if (x + 1 < world.world.length && canBallMove(ball, world.world[x + 1][y]) && dist[x + 1][y] > dist[x][y] + 1) {
                dist[x + 1][y] = dist[x][y] + 1;
                if (world.world[x + 1][y].equals(to)) {
                    break;
                }
                heap.insert(world.world[x + 1][y]);
            }

            if (y - 1 >= 0 && canBallMove(ball, world.world[x][y - 1]) && dist[x][y - 1] > dist[x][y] + 1) {
                dist[x][y - 1] = dist[x][y] + 1;
                if (world.world[x][y - 1].equals(to)) {
                    break;
                }
                heap.insert(world.world[x][y - 1]);
            }

            if (y + 1 < world.world[0].length && canBallMove(ball, world.world[x][y + 1]) && dist[x][y + 1] > dist[x][y] + 1) {
                dist[x][y + 1] = dist[x][y] + 1;
                if (world.world[x][y + 1].equals(to)) {
                    break;
                }
                heap.insert(world.world[x][y + 1]);
            }

            if (world.world[x][y].cageType.equals(Cage.CageType.magic)) {
                boolean exitFlag = false;
                for (Cage portalCage : world.portalCages) {
                    if (canBallMove(ball, portalCage) && dist[portalCage.xIndex][portalCage.yIndex] > dist[x][y] + 1) {
                        dist[portalCage.xIndex][portalCage.yIndex] = dist[x][y] + 1;
                        if (portalCage.equals(to)) {
                            exitFlag = true;
                            break;
                        }
                        heap.insert(portalCage);
                    }
                }
                if (exitFlag) {
                    break;
                }
            }

        }

        if (dist[to.xIndex][to.yIndex] == INFINITY) {
            System.out.println("Ball cant do this");
            return null;
        }

        float stepDuration = 1f / ball.step;
        float stepSize = Main.cageSize + Main.cageSpace;
        float rotation = -360;

        Action[] actions = new Action[dist[to.xIndex][to.yIndex]];
        Cage tmp = to;

        int cnt = 0;
        System.out.println("started: " + cnt);
        while (!tmp.equals(start)) {
            int x = tmp.xIndex;
            int y = tmp.yIndex;
            ++cnt;

            if (cnt > 100) {
                debug = true;
                System.out.println("wow:" + start.xIndex + "; " + start.yIndex + " | " + to.xIndex + "; " + to.yIndex);
                return null;
            }

            if (tmp.cageType.equals(Cage.CageType.magic)) {
                boolean usedPortal = false;
                for (Cage portalCage : world.portalCages) {
                    if (dist[portalCage.xIndex][portalCage.yIndex] < dist[x][y]) {
                        Cage finalTmp = tmp;
                        actions[dist[portalCage.xIndex][portalCage.yIndex]] = (
                                Actions.sequence(
                                        Actions.run(() -> {
                                            if (canBallMove(ball, finalTmp)) {
                                                System.out.println("I can move to this cage");
                                                ball.cageRotating(finalTmp.xIndex, finalTmp.yIndex);
                                            } else {
                                                System.out.println("I cant move to this cage!");
                                                ball.setActions(to.xIndex, to.yIndex);
                                            }
                                        }),
                                        Actions.scaleTo(0, 0, 0.25f),
                                        Actions.run(() -> {
                                            ball.prefPortal = finalTmp;
                                            ball.wantToDoMagic = true;
                                            ball.cagesEffects();
                                            ball.prefPortal = null;
                                            ball.wantToDoMagic = false;
                                        }),
                                        Actions.scaleTo(1, 1, 0.25f))
                        );
                        tmp = portalCage;
                        usedPortal = true;
                        break;
                    }
                }
                if (usedPortal) {
                    continue;
                }
            }

            if (x - 1 >= 0 && dist[x - 1][y] < dist[x][y]) {
                tmp = world.world[x - 1][y];
                addAction(actions, x - 1, y, rotation, stepDuration, stepSize, 0, ball, x, y, to);
            } else if (x + 1 < world.world.length && dist[x + 1][y] < dist[x][y]) {
                tmp = world.world[x + 1][y];
                addAction(actions, x + 1, y, -rotation, stepDuration, -stepSize, 0, ball, x, y, to);
            } else if (y - 1 >= 0 && dist[x][y - 1] < dist[x][y]) {
                tmp = world.world[x][y - 1];
                addAction(actions, x, y - 1, rotation, stepDuration, 0, stepSize, ball, x, y, to);
            } else if (y + 1 < world.world[0].length && dist[x][y + 1] < dist[x][y]) {
                tmp = world.world[x][y + 1];
                addAction(actions, x, y + 1, -rotation, stepDuration, 0, -stepSize, ball, x, y, to);
            }

        }

        for (int i = 0; i < actions.length; ++i) {
            if (actions[i] == null) {
                System.out.println("Null!");
            }
        }

        System.out.println("finished: " + cnt);

        return actions;
    }

    private void addAction(Action[] actions, int x, int y, float rotation, float stepDuration, float xStepSize, float yStepSize, Ball ball, int newBallX, int newBallY, Cage to) {
        actions[dist[x][y]] = (
                Actions.sequence(
                        Actions.run(() -> {
                            // пре-обработка клетки
                            if (canBallMove(ball, world.world[newBallX][newBallY])) {
                                ball.cageRotating(newBallX, newBallY);
                                ball.buildBridge(newBallX, newBallY);
                            } else {
                                ball.setActions(to.xIndex, to.yIndex);
                            }
                        }),
                        Actions.parallel(
                                Actions.rotateBy(rotation, stepDuration),
                                Actions.moveBy(xStepSize, yStepSize, stepDuration)
                        ), Actions.run(() -> {
                            ball.moveTo(newBallX, newBallY);
                            // пост-обработка клетки
                            ball.cagesEffects();
                        }))
        );
    }

    private boolean canBallMove(Ball ball, Cage cage) {

        boolean canBreakWalls = ball.colorIndex % 2 == 0 && ball.colorIndex > 1;
        boolean canSeeMines = ball.colorIndex > 7;
        boolean canBuildBridges = ball.colorIndex == 1;

        if (cage.isTrap && canSeeMines) {
            return false;
        }

        if (cage.isChecked() && !canBreakWalls) {
            return false;
        }

        if (cage.getColor() == null && !canBuildBridges) {
            return false;
        }

        return world.freeFromBalls(cage.xIndex, cage.yIndex);
    }

    private void resetDist() {
        for (int i = 0; i < dist.length; ++i) {
            for (int j = 0; j < dist[0].length; ++j) {
                dist[i][j] = INFINITY;
            }
        }
    }
}
