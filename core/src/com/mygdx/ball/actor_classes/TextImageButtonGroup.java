package com.mygdx.ball.actor_classes;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.mygdx.ball.Main;

public class TextImageButtonGroup extends AImageButton {
    public AImageButton button;
    public ALabel label;
    public Drawable imageUp;

    public TextImageButtonGroup(Drawable imageUp, Drawable imageDown, Drawable imageChecked, InputListener listenerAction,
                         float x, float y, float width, float height,
                         String enText, String ruText,int textSize, Color color, int align, String name
                         ) {
        super(imageUp, imageDown, imageChecked, listenerAction);
        this.imageUp = imageUp;
        setName(name);
        button = this;
        button.setBounds(x, y, width, height);
        button.setDisabled(true);
        label = new ALabel(enText, ruText, textSize, color, align, 2, Color.WHITE);
        label.setBounds(0, 0, width, height);
        label.setTouchable(Touchable.disabled);
        addActor(label);
    }

    public void setArrow(String direction) {
        ImageButton.ImageButtonStyle style = button.getStyle();
        Texture texture = Main.assetManager.get(Main.ARROW + direction + ".png", Texture.class);
        Sprite sprite = new Sprite(texture);
        style.imageUp = new TextureRegionDrawable(sprite);
        imageUp = style.imageUp;
        button.setStyle(style);
    }

    public void setImage(Drawable drawable) {
        ImageButton.ImageButtonStyle style = button.getStyle();
        style.imageUp = drawable;
        imageUp = style.imageUp;
        button.setStyle(style);
    }

    public void setImage(Sprite sprite, Color color) {
        ADrawable aDrawable = new ADrawable(sprite, color);
        ImageButton.ImageButtonStyle style = button.getStyle();
        style.imageUp = aDrawable;
        imageUp = style.imageUp;
        button.setStyle(style);
    }
}
