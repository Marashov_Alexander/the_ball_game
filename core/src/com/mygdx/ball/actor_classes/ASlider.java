package com.mygdx.ball.actor_classes;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;

public class ASlider extends Group {

    private AImageLabel foreground;
    private Image background;

    private float
            maxValue,
            minValue,
            value;

    public boolean isLocked;

    public ASlider (Sprite sprite, Sprite background, String enText, String ruText,int textSize, Color color, int align,
                    float minValue, float maxValue, final boolean isVertical) {
        this.foreground = new AImageLabel(enText, ruText, textSize, color, align, sprite, true);
        this.background = new Image(background);
        this.value = minValue;
        this.isLocked = false;
        this.maxValue = maxValue;
        this.minValue = minValue;
        sprite.setColor(Color.ORANGE);

        addListener(new ActorGestureListener() {
            @Override
            public void pan(InputEvent event, float x, float y, float deltaX, float deltaY) {
                super.pan(event, x, y, deltaX, deltaY);
                if (!isLocked) {
                    if (isVertical)
                        value -= deltaY / 50f;
                    else
                        value -= deltaX / 50f;
                }
            }

        });

        addActor(this.background);
        addActor(this.foreground);
    }

    @Override
    public void setBounds(float x, float y, float width, float height) {
        super.setBounds(x, y, width, height);
        this.background.setBounds(x, y, width, height);
        this.foreground.setBounds(x, y, width, height);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (Math.floor(value) > maxValue) {
            value = maxValue;
        } else if (Math.floor(value) < minValue) {
            value = minValue;
        }
        value = Math.max(value, minValue);
        int intValue = getValue();
        String txt = ((intValue == minValue) ? "  " : ("" + (intValue - 1))) + " " +
                (((intValue == 9) || (intValue == 10)) ? (" " + intValue) : intValue)
                + (intValue == maxValue ? "    " : " " + (intValue + 1));
        foreground.setText(
                txt, txt
        );
//            if (Math.abs(speed) > 0.01) {
//                value += speed * Gdx.graphics.getDeltaTime() / 100;
//                if (Math.floor(value) > maxValue) {
//                    value = maxValue;
//                } else if (Math.floor(value) < minValue) {
//                    value = minValue;
//                }
//                speed -= speed * Gdx.graphics.getDeltaTime();
//                setText("" + Math.floor(value));
//            }
    }

    public int getValue() {
        return (int) Math.floor(value);
    }

    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
    }
}
