package com.mygdx.ball.actor_classes;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.mygdx.ball.Main;
import com.mygdx.ball.Progress;

public class LevelResults extends Group {

    private TripleItems items;
    private LevelStars stars;

    public LevelResults(float x, float y, float width, float height) {
        setBounds(x, y, width, height);
        stars = new LevelStars(170, 980, 270);
        stars.setPosition(0, 0);
        addActor(stars);

        items = new TripleItems(250, 980, height - 270);
        items.setPosition(0, 270);
        addActor(items);
    }

    public void setData(int starsCount, float... data) {
        for (int i = 0; i < 3; i++) {
            stars.setDrawable(i, (i + 1) <= starsCount ? Main.GOLD_STAR_DRAWABLE : Main.WHITE_STAR_DRAWABLE);
            if (Main.infinityItems) {
                items.setData(i, Progress.getActionDrawable(i), "oo  ");
            } else {
                items.setData(i, Progress.getActionDrawable(i), (int) (data[i] * 100) + "% ");
            }
        }
    }
}
