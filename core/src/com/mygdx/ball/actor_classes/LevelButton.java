package com.mygdx.ball.actor_classes;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Align;
import com.mygdx.ball.Cage;
import com.mygdx.ball.Callable;
import com.mygdx.ball.Main;
import com.mygdx.ball.MyClickListener;

public class LevelButton extends Group {
    private AImageButton button;
    private ALabel label;
    private LevelStars stars;
    private int level;

    public LevelButton(float buttonSize, float labelHeight, float starSize, Callable action) {
        button = new AImageButton(null, null, null, null, false);
        button.setBounds(0, labelHeight + starSize, buttonSize, buttonSize);
        button.setDisabled(false);
        button.addListener(new MyClickListener(action));

        label = new ALabel("", "", 80, Color.WHITE, Align.center);
        label.setBounds(0, starSize, buttonSize, labelHeight);
        stars = new LevelStars(starSize, buttonSize, starSize);

        addActor(stars);
        addActor(label);
        addActor(button);
    }

    public void setData(Drawable statusDrawable, int level, int starsCount) {
        button.setImage(statusDrawable);
        label.setText("lvl " + level);
        for (int i = 0; i < 3; i++) {
            stars.setDrawable(i, (i + 1) <= starsCount ? Main.GOLD_STAR_DRAWABLE : Main.WHITE_STAR_DRAWABLE);
        }
        this.level = level;

        switch (starsCount) {
            case -1: {
                button.setColor(Color.WHITE);
                break;
            }
            default: {
                button.setColor(Cage.BUTTONS_COLOR);
            }
        }

    }
}
