package com.mygdx.ball.actor_classes;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

public class LevelStars extends Group {

    private Image[] stars;

    public LevelStars(float starSize, float width, float height) {
        setSize(width, height);
        stars = new Image[3];
        final float starsDist = (width - starSize * 3) / 4f;
        for (int i = 0; i < 3; ++i) {
            stars[i] = new Image();
            stars[i].setBounds(i * starSize + (i + 1) * starsDist, 0, starSize, starSize);
            addActor(stars[i]);
        }
    }

    public void setDrawable(int index, Drawable drawable) {
        stars[index].setDrawable(drawable);
    }
}
