package com.mygdx.ball.actor_classes;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.mygdx.ball.Callable;
import com.mygdx.ball.Main;
import com.mygdx.ball.MyClickListener;
import com.mygdx.ball.Progress;

public class StageSlider extends Group {

    private Group buttons;
    private Array<LevelButton> buttonArray;

    private float buttonWidth, buttonHeight;
    private float labelWidth, labelHeight;
    private float starsWidth, starsHeight;

    private float buttonDistance, stageLabelDistance;

    public int currentPage;
    private Progress progress;
    private ALabel title;

    public StageSlider(float width, float height, Progress progress) {
        setSize(width, height);

        buttonArray = new Array<>();
        this.progress = progress;
        System.out.println("Last state: " + Progress.getLastState(true));
        this.currentPage = Progress.getLastState(true);
        System.out.println("Current page: " + currentPage);
        this.title = new ALabel(
                "State " + currentPage, "Стадия " + currentPage,
                120, Color.WHITE, Align.bottom
        );

        buttonWidth = 220;
        buttonHeight = buttonWidth;

        buttonDistance = 40;
        stageLabelDistance = 160;

        labelWidth = buttonWidth;
        labelHeight = 75;

        starsWidth = buttonWidth;
        starsHeight = 40;


        final float arrowWidth = 85;
        final float arrowHeight = 380;
        AImageButton arrLeft = new AImageButton(
                new ADrawable(
                        new Sprite(Main.assetManager.get(Main.SLIDER_PATH, Texture.class)),
                        arrowWidth, arrowHeight,
                        Color.WHITE,
                        true, false
                ),
                new ADrawable(
                        new Sprite(Main.assetManager.get(Main.SLIDER_PATH, Texture.class)),
                        arrowWidth, arrowHeight,
                        Color.BLUE,
                        true, false
                ),
                () -> {
                    if (currentPage > 0) {
                        System.out.println(currentPage);
                        setState(--currentPage);
                    }
                });

        AImageButton arrRight = new AImageButton(
                new ADrawable(
                        new Sprite(Main.assetManager.get(Main.SLIDER_PATH, Texture.class)),
                        arrowWidth, arrowHeight,
                        Color.WHITE,
                        false, false
                ),
                new ADrawable(
                        new Sprite(Main.assetManager.get(Main.SLIDER_PATH, Texture.class)),
                        arrowWidth, arrowHeight,
                        Color.BLUE,
                        false, false
                ),
                () -> {
                    if (Progress.getLastState(true) != currentPage) {
                        System.out.println(currentPage + "(++)");
                        setState(++currentPage);
                    }
                });

        arrLeft.setBounds(0, (height - arrowHeight) / 2f, arrowWidth, arrowHeight);
        arrRight.setBounds(width - arrowWidth, (height - arrowHeight) / 2f, arrowWidth, arrowHeight);

        addActor(arrRight);
        addActor(arrLeft);

        title.setBounds((width - 420) / 2, height - 120, 420, 80);
        addActor(title);

        addActor(buttons = new Group());
        buttons.setBounds(arrowWidth + 120, 0, width - (arrowWidth + 120) * 2, buttonHeight + labelHeight + starsHeight);
        buttons.setTouchable(Touchable.childrenOnly);
        buttons.addListener(new MyClickListener(() -> System.out.println("aaa")));
    }

    public void setState(int stateIndex) {
        if (!Progress.setState(stateIndex)) {
            System.out.println("Failed to set state: " + stateIndex);
            return;
        }
        currentPage = stateIndex;
        System.out.println("State: " + stateIndex);
        for (int i = 0; i < buttonArray.size; i++) {
            int realLevelIndex = Progress.getLevelIndex(stateIndex, i);
            int starsCount = Progress.getStarsCount(realLevelIndex);
            buttonArray.get(i).setData(
                    starsCount == 0
                            ? Main.CURRENT_DRAWABLE
                            : starsCount == -1
                                    ? Main.LOCKED_DRAWABLE
                                    : Main.BALL_DRAWABLE,
                    realLevelIndex + 1,
                    starsCount
            );
        }
        title.setText("State " + progress.getState(false), "Стадия " + progress.getState(false));
    }

    public void addButton(Callable action) {
        LevelButton levelButton = new LevelButton(buttonWidth, labelHeight, starsHeight, action);
        levelButton.setPosition(buttonArray.size * (buttonWidth + buttonDistance), 100);
        buttonArray.add(levelButton);
        buttons.addActor(levelButton);
    }
}
