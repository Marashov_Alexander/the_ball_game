package com.mygdx.ball;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.PixmapIO;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.TextureData;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.freetype.FreeType;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.imageio.ImageIO;

import static com.mygdx.ball.Main.skipTutorials;

public class Progress {

    public static final int LEVEL_IN_STAGE_COUNT = 5;
    static TutorialManager tutorialManager;

    float time;

    private static int level;
    private static int state;

    private static int sizeX;
    private static int sizeY;

    private float cageSize;
    private float spaceSize;

    static boolean wallEnabled;
    static boolean exitEnabled;
    private static boolean magicEnabled;
    static boolean ballGame;
    static boolean ballStrategyGame;
    static int ballCount;

    public enum GameMode {
        findTheBalls,
        catchTheBalls,
        bonusClassic,
        bonusExtra
    }

    static public GameMode gameMode;

    static int money;
    private static int[] power;

    private static HashMap<String, String> enSubjectInfo;
    private static HashMap<String, String> ruSubjectInfo;

   public Progress(float cageSize, float spaceSize) {
        itemsMap = new TreeMap<>();
        enSubjectInfo = new HashMap<>();
        ruSubjectInfo = new HashMap<>();

        for (int i = 0; i < FREE_ITEM_INDEX; ++i) {
            itemsMap.put(i, 0);
            enSubjectInfo.put(enBonusNames[i], enBonusDescription[i]);
            ruSubjectInfo.put(ruBonusNames[i], ruBonusDescription[i]);
        }

        for (int power = 0; power < 3; ++power) {
            for (int itemIndex = 0; itemIndex < 3; ++itemIndex) {
                enSubjectInfo.put(enItemNames[itemIndex] + power, enItemDescription[itemIndex][power]);
                ruSubjectInfo.put(ruItemNames[itemIndex] + power, ruItemDescription[itemIndex][power]);
            }
        }

        for (int i = 0; i < colors.length; ++i) {
            enSubjectInfo.put(colors[i].toString(), enBallInfo[i]);
            ruSubjectInfo.put(colors[i].toString(), ruBallInfo[i]);
        }

        for (int i = 0; i < ruCageInfo.length; ++i) {
            enSubjectInfo.put(enCageNames[i], enCageInfo[i]);
            ruSubjectInfo.put(ruCageNames[i], ruCageInfo[i]);
        }

        money = 0;
        power = new int[3];

        tutorialManager = new TutorialManager();
        this.cageSize = cageSize;
        this.spaceSize = spaceSize;

        stars = new ArrayList<>();

        loadProgress();
        // HERE
       if (stars.size() < 30) {
           for (int i = 0; i < 30; ++i) {
               stars.add(3);
           }
       }

    }

    private static String[] ruBallInfo = {
            "Перемещается каждый Ваш ход по свободным клеткам со своей скоростью, нет особых способностей.", // 0
            "Перемещается. При обнаружении красит клетку синим цветом и пытается сбежать в обличии обычного мяча..", // 1
            "Может перемещаться даже по открытым клеткам, закрывая их.", // 2 зелёный
            "При попадании мяч взрывается, открывая и соседние клетки.", // 3 красный
            "Очень шустрый мяч, иногда красит клетки в свой цвет, закрывает открытые клетки.", // 4 жёлтый
            "Мяч при попадании создаёт на соседних свободных клетках белые мячи.", // 5 голубой
            "Ходит даже по открытым клеткам, взрывается при попадании, предметы не дают информации вблизи мяча.", // 6 чёрный
            "Обходит мины стороной.", // 7 розовый
            "Обходит мины стороной, шустро бегает по открытым клеткам и оставляет следы.", // 8 лаймовый
            "Обходит мины стороной и ходит даже по открытым клеткам.", // 9 лаймовый
            "Обходит мины, может перемещаться по открытым клеткам и создаёт мячи при обнаружении.", // 10 магента
            "Обходит мины стороной. Иногда открывает порталы.", // 11 фиолетовый
            "Обходит мины, ходит даже по открытым клеткам, шустро бегает, оставляет следы, взрывается при попадании, предметы не дают информации вблизи мяча.", // 12 серый
            "Обходит мины стороной, отражает каждое второе нападение.", // 13 коричневый
            "Перемещается даже по открытым клеткам, обходит мины стороной, поджигает клетки", // 14 оранжевый
    };

    private static String[] enBallInfo = {
            "Moves your every move on the free cells with its own speed, there are no special abilities.", // 0
            "Moves. When detected, it paints the cell blue and tries to escape in the guise of an ordinary ball.", // 1
            "It can even move through open cells, closing them.", // 2 зелёный
            "When hit, the ball explodes, opening and neighboring cells.", // 3 красный
            "Very nimble ball, sometimes paints the cells in their own color, closes the open cells.", // 4 жёлтый
            "When the ball hits, it creates white balls on adjacent free cells.", // 5 голубой
            "Walks even on open cells, explodes when hit, items do not give information near the ball.", // 6 чёрный
            "Avoid mines.", // 7 розовый
            "Avoid mines, quickly runs through open cells and leaves traces.", // 8 лаймовый
            "Avoid mines, can move through open cells.", // 9 лаймовый
            "Avoid mines, can move through open cells, and creates balls when detected.", // 10 магента
            "Avoid mines. Sometimes opens portals.", // 11 фиолетовый
            "Avoid mines, can move through open cells, nimbly runs, leaves traces, explodes when hit, items do not give information near the ball.", // 12 серый
            "Avoid mines, reflects every second attack.", // 13
            "Avoid mines, can move through open cells. Be careful: it sets fire to the cages!", // 14
    };

    static String getSubjectInfo(boolean isEnglish, String actorID) {
        return isEnglish ? enSubjectInfo.get(actorID) : ruSubjectInfo.get(actorID);
    }

    public static String[] enCageNames = {
            "Cage",
            "Opened_cage",
            "Wall_cage",
            "Arrow_cage",
            "Distance_cage",
            "Distance_cage2",
            "Bomb_cage",
            "Color_cage",
            "Portal_cage",
            "Exit_cage",
            "Bonus_cage",
            "Fire_cage",
    };

    public static String[] ruCageNames = {
            "Клетка",
            "Открытая_клетка",
            "Клетка_стена",
            "Клетка_стрелка",
            "Клетка_расстояние",
            "Клетка_расстояние2",
            "Клетка_бомба",
            "Цветная_клетка",
            "Клетка_портал",
            "Клетка_выход",
            "Клетка_бонус",
            "Клетка_огонь",
    };

    public static String[] enCageInfo = {
            "An ordinary cage, it can hide a ball or a bonus. Use one of the items to open it.",
            "An open cell, after opening it, there was nothing and no one on it. Most balls can't walk on it now, but some are able to close it.",
            "Wall cage-originally an open cage added just to make it easier to find balls on a large field",
            "Indicates the direction to the nearest ball, but without improvements, this information becomes outdated due to the movement of the balls.",
            "Shows the distance to the nearest ball, but this information may become outdated without improvements due to the movement of the balls.",
            "The cell shows the distance to the nearest ball and its speed, but this information is outdated due to the movement of the balls.",
            "The cage is a mine. A ball that steps on a cell next to it automatically loses. You can activate it with double touch.",
            "Any cell can change its color: because of the improved item of the Arrow (indicates the color of the nearest ball); because of some balls that color the cells when moving; because of the blue ball, if you hit it, but it escaped; because of the black ball, if it blocked your move.",
            "Cell-portal. The ball that hits the portal moves to any free or any square if all the portals are occupied or pressed.",
            "The cage is the exit. All the balls are trying to hit it to escape.",
            "The bonus cell contains something very useful :)",
            "Cell-fire does not allow you to click on the cell until the fire is extinguished. Be careful: the fire is spreading!"
    };

    public static String[] ruCageInfo = {
            "Обычная клетка, на ней может прятаться мяч или бонус. Используйте один из предметов, чтобы открыть её.",
            "Большинство мячей не могут ходить по открытой клетке, но некоторые способны закрыть её.",
            "Клетка-стена - изначально открытая клетка.",
            "Направление к ближайшему мячу, однако без улучшений эта информация устаревает из-за движения мячей.",
            "Расстояние до ближайшего мяча, но эта информация без улучшений может устареть из-за движения мячей.",
            "Расстояние до ближайшего мяча и его скорость, но эта информация устаревает из-за движения мячей.",
            "Клетка-мина. Мяч, наступивший на соседню с ней клетку, автоматически проигрывает. Можно активировать вручную двойным касанием.",
            "Клетка окрасилась либо из-за сбежавшего синего мяча, либо из-за следов быстрых мячей, либо ещё что...",
            "Мяч, попавший в портал, перемещается в любой свободный или на любую клетку, если все порталы заняты или нажаты.",
            "Клетка-выход. Все мячи пытаются попасть в неё, чтобы сбежать.",
            "Клетка-бонус содержит что-то очень полезное :)",
            "Клетка-огонь не даёт нажать на клетку, пока огонь не потушен. Будьте осторожны: огонь распространяется!",
    };

    private static String[] enItemNames = {
            "Finger",
            "Arrow",
            "Ruler"
    };

    private static String[] ruItemNames = {
            "Палец",
            "Стрелка",
            "Линейка"
    };

    public static String getItemName(boolean isEnglish, int index) {
        return (isEnglish ? enItemNames[index] : ruItemNames[index]) + power[index];
    }

    private static String[] enBonusNames = {
            "Snowflake",
            "Fire",
            "Wall",
            "Flashlight",
            "Plus"
    };

    private static String[] ruBonusNames = {
            "Снежинка",
            "Огонь",
            "Стена",
            "Фонарь",
            "Плюсик",
    };

    public static String getBonusName(boolean isEnglish, int index) {
        return isEnglish ? enBonusNames[index] : ruBonusNames[index];
    }

    public static Color getRandomColor() {
        return colors[(int) (Math.random() * colors.length)];
    }

    public static int getLevelIndex(int stateFromZero, int levelIndex) {
        return stateFromZero * LEVEL_IN_STAGE_COUNT + levelIndex;
    }

    public static int getStarsCount(int levelIndex) {
        if (stars.size() == levelIndex) {
            return 0;
        }

        if (stars.size() > levelIndex) {
            return stars.get(levelIndex);
        } else {
            return -1;
        }
    }

    public static boolean hasTutorial(int levelIndex) {
        return tutorialManager.firstLevelIndex(state) != 1 && levelIndex == 0;
    }

    public static boolean levelAvailable(int stateIndex, int levelIndex) {
        return getLastState(true) >= stateIndex && Progress.getStarsCount(Progress.getLevelIndex(stateIndex, levelIndex)) != -1;
    }

    public int getState(boolean isIndex) {
        return isIndex ? state : state + 1;
    }

    int getLevel() {
        return level;
    }

    int getUncheckCount(Cage cage) {
        if (exitEnabled || (cage != null && cage.cageType == Cage.CageType.magic)) {
            return 3;
        } else {
            return 100;
        }
    }

    Cage.CageType getCageType(int x, int y) {

        if (level == LEVEL_IN_STAGE_COUNT) {
            int pixel = imageMap.getPixel(x, y);
            int red = pixel >>> 24;
            int green = (pixel & 0xFF0000) >>> 16;
            int blue = (pixel & 0xFF00) >>> 8;

            if (blue < 200 && red < 200 && blue > 56 && red > 56
                    && green < red / 2f && green < blue / 2f
                    && (red/1.2f < blue && blue / 1.2f < red)) {
                return Cage.CageType.magic;
            } else if (green + blue < 0.2f * red) {
                return Cage.CageType.fire;
            } else if (red > 82 && blue < 50 && green < 0.65f * red) {
                return Cage.CageType.wall;
            }
        }

        if (magicEnabled && Math.random() < 0.05) {
            return Cage.CageType.magic;
        } else {
            return Cage.CageType.simple;
        }
    }

    static int getPower(int itemIndex) {
//        int power = 0;
//        while (power + 1 < buffActionsState[itemIndex].length && buffActionsState[itemIndex][power + 1] <= getState(false)) {
//            ++power;
//        }
        return power[itemIndex];
    }

    int getBonusCount(int index) {
        return itemsMap.get(index);
    }

    boolean bonusNeedCage(int index) {
        return index == FIRE_ITEM;
    }

    boolean canUseBonus(int index) {
        System.out.println(index + " != " + WALL_ITEM);
        return index != WALL_ITEM || wallEnabled;
    }

//    void saveBonuses() {
//        FileHandle saveFile = Gdx.files.local("ball.save");
//        saveFile.writeString("\ni", true);
//        for (int value : power) {
//            saveFile.writeString(" " + value, true);
//        }
//        saveFile.writeString(" " + money, true);
//        for (int i = 0; i < FREE_ITEM_INDEX; ++i) {
//            saveFile.writeString(" " + itemsMap.get(i), true);
//        }
//    }

    public static int getLastState(boolean isIndex) {
        return stars.size() / LEVEL_IN_STAGE_COUNT + (isIndex ? 0 : 1);
    }

    public int getUpdateItemCost(int index) {
        if (getPower(index) == 2) {
            return 0;
        }
        return (getPower(index) + 1) * 100;
    }

    public int getUpdateBonusCost(int index) {
        return 300;
    }

    // [предмет] [сила]
    public String[][] ruItemDescription = {
            {
                "Палец открывает нажатую им клетку",
                "Улучшенный палец открывает целых 5 клеток за раз",
                "Красный палец ставит в указанную клетку мину, которая реагирует на мячи или активируется двойным касанием",
                "Других улучшений нет"
            }, // палец
            {
                "Стрелка указывает направление к ближайшему мячу",
                "Радужная стрелка указывает не только направление, но и цвет ближайшего мяча",
                "Красная стрелка показывает направление в реальном времени",
                "Других улучшений нет"
            }, // стрелка
            {
                "Линейка показывает расстояние до ближайшего мяча",
                "Штангенциркуль показывает не только расстояние до шара, но и его скорость",
                "Красный штангенциркуль указывает расстояние в реальном времени",
                "Других улучшений нет"
            }, // линейка
    };

    // [предмет] [сила]
    public String[][] enItemDescription = {
            {
                    "The finger opens the cell it pressed",
                    "Improved finger opens as many as 5 cells at a time",
                    "The red finger puts a mine in the specified cell that reacts to balls or be activated with double touch",
                    "There are no other improvements"
            }, // палец
            {
                    "Arrow indicates the direction to the nearest ball",
                    "The rainbow arrow indicates not only the direction, but also the color of the nearest ball",
                    "The red arrow shows the direction in real time",
                    "There are no other improvements"
            }, // стрелка
            {
                    "The ruler shows the distance to the nearest ball",
                    "The caliper shows not only the distance to the ball, but also its speed",
                    "The red caliper indicates the distance in real time",
                    "There are no other improvements"
            }, // линейка
    };

    String getItemUpdateDescription(boolean isEnglish, int index) {
        return isEnglish
                ? enItemDescription[index][getPower(index) + 1]
                : ruItemDescription[index][getPower(index) + 1];
    }

    public Drawable getUpdateActionDrawable(int index) {
        return actionDrawables[index][Math.min(getPower(index) + 1, 2)];
    }

    public boolean buyItem(int index) {
        if (money < getUpdateItemCost(index) || power[index] == 2) {
            return false;
        }
        money -= getUpdateItemCost(index);
        ++power[index];
        return true;
    }

    private String[] enBonusDescription = {
            "Snowflake reduces the speed of all the balls in half and highlights the cells where they were a move ago",
            "Fire opens an area of cells of random size",
            "The wall opens all the wall cells and their neighbors (if there are walls on the map)",
            "The lantern highlights the cells where the balls were moved back, their colors",
            "Plus sign adds ten units of each item at the current level",
    };
    private String[] ruBonusDescription = {
            "Снежинка уменьшает скорость всех шаров в два раза и подсвечивает клетки, на которых они были ход назад",
            "Огонь открывает область клеток случайного размера",
            "Стена открывает все клетки-стены и их соседей (если на карте есть стены)",
            "Фонарь подсвечивает клетки, на которых шары были ход назад, их цветами",
            "Плюсик добавляет десять единиц каждого предмета на текущем уровне",
    };

    public String getBonusDescription(boolean isEnglish, int index) {
        return isEnglish
                ? enBonusDescription[index]
                : ruBonusDescription[index];
    }

    public int getMoney() {
        return money;
    }

    public boolean buyBonus(int index) {
        if (money < getUpdateBonusCost(index)) {
            return false;
        }
        money -= getUpdateBonusCost(index);
        int tmp = itemsMap.get(index);
        itemsMap.put(index, tmp + 1);
        return true;
    }

    private static int ballGameBonusCount;
    int getBallGameBonusCount() {
        return ballGameBonusCount;
    }

    public class Action {
        int nameIndex;
        int remains;
        int full;

        Action(int nameIndex, int remains) {
            this.nameIndex = nameIndex;
            this.remains = remains;
            full = remains;
        }

        boolean enabled() {
            return remains > 0;
        }

        Drawable getItemDrawable() {
            return getActionDrawable(nameIndex);
        }

        @Override
        public String toString() {
            return super.toString();
        }

        float getRemainsPercent() {
            return (float) remains / (float) full;
        }
    }
    public Action[] actions;

//    private Action[][] stateActions;

    public static ArrayList<Integer> stars;

    public static final Integer     SNOW_ITEM = 0;
    public static final Integer     FIRE_ITEM = 1;
    public static final Integer     WALL_ITEM = 2;
    public static final Integer     FLASHLIGHT_ITEM = 3;
    public static final Integer     GIFT_ITEM = 4;
    public static final Integer     FREE_ITEM_INDEX = 5;
    Map<Integer, Integer> itemsMap;

    static Color[] colors = {
            Color.WHITE, // 0
            Color.BLUE, // 1
            Color.GREEN, // 2
            Color.RED, // 3
            Color.YELLOW, // 4
            Color.CYAN, // 5
            Color.BLACK, // 6

            Color.PINK, // 7 свободно

            Color.LIME, // 8: 2, 4
            Color.MAGENTA, // 9: 3
            Color.VIOLET, // 10: 2, 5
            Color.GRAY, // 11 - простое число - свободно
            Color.GOLD, // 12: 2, 3, 4, 6
            Color.BROWN, // 13 - простое число
            Color.ORANGE, // 14 = 2, 7
    };

    private static SpriteDrawable[][] actionDrawables;
    private static SpriteDrawable[] bonusDrawables;

    static void setDrawables(SpriteDrawable[][] actionDrawables, SpriteDrawable[] bonusDrawables) {
        Progress.actionDrawables = actionDrawables;
        Progress.bonusDrawables = bonusDrawables;
    }

    static int[][] buffActionsState = {
            {
                1,
                4,
                7
            },
            {
                1,
                3,
                6
            },
            {
                1,
                2,
                5
            }
    };

    public static Drawable getActionDrawable(int index) {
        return actionDrawables[index][getPower(index)];
    }

    public static Drawable getBonusDrawable(int index) {
        return bonusDrawables[index];
    }

    private void loadProgress() {
        FileHandle saveFile = Gdx.files.local(Main.PROGRESS_FILE_PATH);
        if (saveFile.exists()) {
            System.out.println("exists");
            String[] strings = saveFile.readString().split("\n");

            for (String string : strings) {
                String[] stringsI = string.split(" ");
                // пустые строки
                if (stringsI[0].equals("")) {
                    continue;
                }
                // предметы и деньги
                if (stringsI[0].equals("i")) {
                    for (int j = 1; j < power.length + 1; ++j) {
                        power[j - 1] = Integer.parseInt(stringsI[j]);
                    }
                    money = Integer.parseInt(stringsI[power.length + 1]);
                    for (int j = power.length + 2; j < stringsI.length; ++j) {
                        itemsMap.put(j - power.length - 2, Integer.valueOf(stringsI[j]));
                    }
                // прогресс уровеней
                } else if (stringsI[0].equals("c")) {
                    Main.clickCount = Integer.parseInt(stringsI[1]);
                } else {
                    for (String s : stringsI) {
                        stars.add(Integer.parseInt(s));
                    }
                }
            }
        } else {
            System.out.println("not exists");
        }
        reset();
//        money = 30000;
    }

    void saveProgress() {
        FileHandle saveFile = Gdx.files.local(Main.PROGRESS_FILE_PATH);
        saveFile.writeString(starsCountString() + "\n", false);

        saveFile.writeString("i", true);
        for (int value : power) {
            saveFile.writeString(" " + value, true);
        }

        saveFile.writeString(" " + money, true);
        for (int i = 0; i < FREE_ITEM_INDEX; ++i) {
            saveFile.writeString(" " + itemsMap.get(i), true);
        }

        saveFile.writeString("\nc " + Main.clickCount, true);
    }

    private String starsCountString() {
        StringBuilder result = new StringBuilder();
        for (Integer star : stars) {
            result.append(star).append(" ");
        }
        return result.toString();
    }

    // обработать
    public static boolean setState(int stateFromZero) {
        if (getLastState(true) >= stateFromZero) {
            state = stateFromZero;
            return true;
        }
        return false;
    }

    void levelUp(int starsCount, int coins) {

        // не обучение
        if (level > 0) {
            money += coins;
            int index = state * LEVEL_IN_STAGE_COUNT + level - 1;
            if  (index > stars.size()) {
                throw new IllegalStateException("Как-то перескочили через уровень!");
            }
            if (index == stars.size()) {
                stars.add(starsCount);
            } else if (stars.get(index) < starsCount) {
                stars.set(index, starsCount);
            }
            saveProgress();
        }
        ++level;
        if (level > LEVEL_IN_STAGE_COUNT) {
            ++state;
            // если открыта новая стадия, то смотрим, есть ли обучение
            if (state == getLastState(true)) {
                level = tutorialManager.firstLevelIndex(state);
                int reward = getStateReward(state);
                System.out.println("You opened new state! Your reward is " + reward);
//                money += reward;
            } else {
                level = 1;
            }
        }
        setLevelParameters();
        Main.showAd();
    }

    int getStateReward(int state) {
        if (state != getLastState(true)) {
            return 0;
        }
        if (state < 4) {
            return 100;
        }

        if (state < 7) {
            return 200;
        }

        return 300;
    }

    public static boolean setLevel(int stateIndex, int levelIndex) {
        return setLevel(stateIndex, levelIndex, skipTutorials);
    }

    public static Pixmap imageMap;
//    public static BufferedImage imageBMPMap;

    public static boolean setLevel(int stateIndex, int levelIndex, boolean customSkipTutorials) {
        if (setState(stateIndex) && Progress.getStarsCount(Progress.getLevelIndex(stateIndex, levelIndex)) != -1) {
            if (levelIndex == 0) {
                if (customSkipTutorials
//                        || getLastState(true) != stateIndex
                ) {
                    level = 1;
                } else {
                    level = tutorialManager.firstLevelIndex(stateIndex);
                }
            } else {
                level = levelIndex + 1;
            }
            setLevelParameters();
            return true;
        } else {
            return false;
        }
    }

    private static void setLevelParameters() {
        if (level <= 0) {
            sizeX = tutorialManager.simulatorParams[state][Math.abs(level)].worldSizeX;
            sizeY = tutorialManager.simulatorParams[state][Math.abs(level)].worldSizeY;
            wallEnabled = tutorialManager.simulatorParams[state][Math.abs(level)].wallEnabled;
            exitEnabled = tutorialManager.simulatorParams[state][Math.abs(level)].exitEnabled;
            magicEnabled = tutorialManager.simulatorParams[state][Math.abs(level)].magicEnabled;
            ballGame = tutorialManager.simulatorParams[state][Math.abs(level)].ballGameEnabled;
            ballStrategyGame = false;
            if (ballGame) {
                ballGameBonusCount = 1;
            }
        } else if (level == LEVEL_IN_STAGE_COUNT) {
            String path;
            if (state > 9) {
                path = "maps/map_00" + ((int) (Math.random() * 10) % 9) + ".bmp";
            }
            else {
                path = "maps/map_00" + state + ".bmp";
            }

            TextureData tmp = new Texture(Gdx.files.internal(path)).getTextureData();
            tmp.prepare();
            imageMap = tmp.consumePixmap();// read(bmpFile);
            sizeX = imageMap.getWidth();
            sizeY = imageMap.getHeight();
            wallEnabled = true;
            // HERE
            ballGame = true;
            ballStrategyGame = true;

            exitEnabled = true;
            magicEnabled = true;
            ballGameBonusCount = Math.max(1, ballCount);

        } else {
            sizeX = getRandomSize();
            sizeY = getRandomSize();
            if (sizeY > sizeX) {
                int tmp = sizeX;
                sizeX = sizeY;
                sizeY = tmp;
            }
            wallEnabled = sizeX >= 8 && sizeY >= 8;
            ballGame = state > 4 && Math.random() < 0.05 && wallEnabled;
            ballStrategyGame = false;
//            ballGame = true;
            if (ballGame) {
                sizeX = Math.min(16, sizeX);
                sizeY = Math.min(10, sizeY);
            }
            exitEnabled = state > 3 && (Math.random() < 0.2 && wallEnabled) || ballGame;
            ballGameBonusCount = ballGame ? Math.max(1, ballCount) : 0;
            magicEnabled = state > 1 && (exitEnabled || Math.random() < 0.2);
            System.out.println("Wall enabled: " + wallEnabled);
            System.out.println("Exit enabled: " + exitEnabled);
            System.out.println("Magic enabled: " + magicEnabled);
            System.out.println("BallGame enabled: " + ballGame);
        }

//        gameMode = ballGame ? GameMode.bonusClassic

    }

    private static int getRandomSize() {
        return (int) Math.round(Math.random() * level + 2 * (state + 2));
    }

    int getSizeX() {
        return sizeX;
    }

    int getSizeY() {
        return sizeY;
    }

    // обработать
    CopyOnWriteArrayList<Ball> getBallsAndCalculateActionsCount(Cage[][] world) {
        // обращение к TutorialManager, если level <= 0?
        int maxSpeed = 0;
        if (level <= 0) {
            System.out.println(state);
            System.out.println(level);
            TutorialManager.SimulatorParams simulatorParams = tutorialManager.simulatorParams[state][Math.abs(level)];
            int size = simulatorParams.ballsCount;
            CopyOnWriteArrayList<Ball> balls = new CopyOnWriteArrayList<>();
            for (int i = 0; i < size; ++i) {
                balls.add(spawnBall(
                        simulatorParams.ballColors[i],
                        simulatorParams.ballCordsX[i],
                        simulatorParams.ballCordsY[i],
                        simulatorParams.ballSteps[i]
                ));
            }
            for (Action action : actions) {
                action.remains = 100;
            }
            return balls;
        }

        // HERE!!!
//        if (level == LEVEL_IN_STAGE_COUNT) {
//            ballCount = sizeX * sizeY;
//        } else {
            ballCount = Math.min((int) (Math.random() * level + 1 + state) * (ballGame ? 3 : 1), Math.min(getSizeX(), getSizeY()) / 2);
//        }
//        ballCount = 64;
//        int size = 1;
        CopyOnWriteArrayList<Ball> balls = new CopyOnWriteArrayList<>();
        for (int i = 0; i < ballCount; ++i) {
//            Ball ball = level == LEVEL_IN_STAGE_COUNT ? spawnBall(world, Math.min(state, colors.length - 1)) : spawnBall(world);
            Ball ball = spawnBall(world);
            balls.add(ball);
            maxSpeed = Math.max(maxSpeed, ball.getStep());
        }

        for (Action action : actions) {
            action.remains = (int) (Math.ceil((Math.max(ballCount - state / 6f, 1) * Math.log(sizeX * sizeY) * Math.max(1.5f - state / 6f, 1f) + maxSpeed)) / 2f);
            action.full = action.remains;
        }

        return balls;
    }

    Ball spawnBall(int colorIndex, int x, int y, int step) {
        Sprite ballSprite = new Sprite(Main.ballTexture);
        ballSprite.setColor(colors[colorIndex]);
        return new Ball(
                ballSprite,
                x, y,
                cageSize, spaceSize, step, colorIndex, ballGame
        );
    }

    Ball spawnBall(int colorIndex, int x, int y) {
        Sprite ballSprite = new Sprite(Main.ballTexture);
        ballSprite.setColor(colors[colorIndex]);
        int step = exitEnabled ? (colorIndex < 5 ? 1 : 2) : (int) (Math.random() * level + 1);
        return new Ball(
                ballSprite,
                x, y,
                cageSize, spaceSize, step, colorIndex, ballGame
        );
    }

    Ball spawnBall(Cage[][] cages, int colorIndex) {
        int x, y;
        do {
            x = getRandomX();
            y = getRandomY();
        } while (cages[x][y].button.isChecked());

        return spawnBall(
                colorIndex,
                x, y
        );
    }

    int getRandomX() {
        return (int) Math.round(Math.random() * sizeX) % sizeX;
    }

    int getRandomY() {
        return (int) Math.round(Math.random() * sizeY) % sizeY;
    }

    // обработать
    Ball spawnBall(Cage[][] cages) {
        return spawnBall(
                cages,
                Math.min(
                        (int) Math.round(Math.random() * 2 * (state + 1)),
                        colors.length - 1
                )
        );
    }

    float getCageSize() {
        return cageSize;
    }

    float getSpaceSize() {
        return spaceSize;
    }

    String enGetProgress(boolean afterLevelUp) {
        return "Level: " + (state + 1) + "." + (afterLevelUp ? (level - 1) : level);
    }
    String ruGetProgress(boolean afterLevelUp) {
        return "Уровень: " + (state + 1) + "." + (afterLevelUp ? (level - 1) : level);
    }

    boolean noActions() {
        for (Action action: actions) {
            if (action.enabled()) {
                return false;
            }
        }
        return true;
    }

    private void reset() {
        setState(0);
        --level;
        Main.adsTimer = 0;
        levelUp(0, 0);
        actions = new Action[3];
        actions[0] = new Action(0, 123);
        actions[1] = new Action(1, 123);
        actions[2] = new Action(2, 123);
    }

    char[][][] mazes = new char[][][] {
            new char[][]{
                    new char[]{'0', '0', '0', '0', '0', '0', '0', '0'},
                    new char[]{'0', '1', '1', '0', '0', '1', '1', '0'},
                    new char[]{'0', '1', '0', '0', '0', '0', '1', '0'},
                    new char[]{'0', '0', '0', '1', '0', '0', '0', '0'},
                    new char[]{'0', '0', '0', '0', '1', '0', '0', '0'},
                    new char[]{'0', '1', '0', '0', '0', '0', '1', '0'},
                    new char[]{'0', '1', '1', '0', '0', '1', '1', '0'},
                    new char[]{'0', '0', '0', '0', '0', '0', '0', '0'},
            },
            new char[][]{
                    new char[]{'0', '0', '0', '0', '0', '0', '0', '0'},
                    new char[]{'0', '1', '0', '0', '0', '0', '1', '0'},
                    new char[]{'0', '0', '1', '0', '0', '1', '0', '0'},
                    new char[]{'0', '0', '0', '0', '0', '0', '0', '0'},
                    new char[]{'0', '0', '0', '0', '0', '0', '0', '0'},
                    new char[]{'0', '0', '1', '0', '0', '1', '0', '0'},
                    new char[]{'0', '1', '0', '0', '0', '0', '1', '0'},
                    new char[]{'0', '0', '0', '0', '0', '0', '0', '0'},
            },
            new char[][]{
                    new char[]{'0', '0', '0', '0', '0', '0', '0', '0'},
                    new char[]{'0', '0', '1', '0', '0', '1', '0', '0'},
                    new char[]{'0', '1', '0', '0', '0', '0', '1', '0'},
                    new char[]{'0', '0', '0', '0', '0', '0', '0', '0'},
                    new char[]{'0', '0', '0', '0', '0', '0', '0', '0'},
                    new char[]{'0', '1', '0', '0', '0', '0', '1', '0'},
                    new char[]{'0', '0', '1', '0', '0', '1', '0', '0'},
                    new char[]{'0', '0', '0', '0', '0', '0', '0', '0'},
            },
            new char[][]{
                    new char[]{'1', '0', '1', '0', '0', '1', '0', '1'},
                    new char[]{'0', '0', '0', '0', '0', '0', '0', '0'},
                    new char[]{'1', '0', '1', '0', '0', '1', '0', '1'},
                    new char[]{'0', '0', '0', '0', '0', '0', '0', '0'},
                    new char[]{'0', '0', '0', '0', '0', '0', '0', '0'},
                    new char[]{'1', '0', '1', '0', '0', '1', '0', '1'},
                    new char[]{'0', '0', '0', '0', '0', '0', '0', '0'},
                    new char[]{'1', '0', '1', '0', '0', '1', '0', '1'},
            }
    };
}
