package com.mygdx.ball;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Align;
import com.mygdx.ball.Windows.AWindow;
import com.mygdx.ball.Windows.YesNoScrollPane;
import com.mygdx.ball.actor_classes.ADrawable;
import com.mygdx.ball.actor_classes.AImageTextButton;
import com.mygdx.ball.actor_classes.ALabel;
import com.mygdx.ball.actor_classes.TextImageButtonGroup;

import static com.mygdx.ball.Cage.BUTTONS_COLOR;
import static com.mygdx.ball.Main.screenHeight;
import static com.mygdx.ball.Main.screenWidth;

class MenuScreenTemplate extends Group {

    AWindow exitConfirmation;
    private AImageTextButton button1;
    private AImageTextButton button2;
    private AImageTextButton button3;
    private AImageTextButton button4;
    private TextImageButtonGroup exitButton;
    private Image imageLabel;
    private Image ballImage;

    MenuScreenTemplate(
            Stage addTo,
            String enTitle, String ruTitle,
            String enTextBtn1, String ruTextBtn1, ClickListener listener1,
            String enTextBtn2, String ruTextBtn2, ClickListener listener2,
            String enTextBtn3, String ruTextBtn3, ClickListener listener3,
            String enTextBtn4, String ruTextBtn4, ClickListener listener4,
            Callable ifYesAction, Callable helpAction,
            boolean needExitConfirmation
    ) {
        super();
        setBounds(0, 0, screenWidth, screenHeight);

        exitConfirmation = new AWindow(
                "Confirmation",
                "Подтверждение",
                new YesNoScrollPane(
                        "Do you really want to exit?",
                        "Вы действительно хотите выйти?",
                        ifYesAction,
                        () -> exitConfirmation.hide()
                ),
                0.75f, 0.75f,
                true
        );

        Image background = new Image(Main.backgroundDrawable);
        background.setBounds(0, 0, screenWidth, screenHeight);
        addActor(background);

        // 10 мячей
        int ballSize = 90;
        int distance = 16;

        for (int i = distance; i <= screenWidth - ballSize; i = i + ballSize + distance) {
            for (int j = distance; j <= screenHeight - ballSize - distance; j = j + ballSize + distance) {
                Color color = Progress.getRandomColor();
                Image image = new Image(new ADrawable(new Sprite(Main.ballTexture), ballSize, ballSize, color));
                image.setBounds(i, j, ballSize, ballSize);
                image.setOrigin(Align.center);

//                float mathRandom = (float) Math.random() * 2;
                float mathRandom = 0.1f;

                image.addAction(Actions.fadeOut(0));
                image.addAction(Actions.forever(
                        Actions.parallel(
                            Actions.sequence(
                                    Actions.scaleTo(1f, 1f),
                                    Actions.delay((float) Math.random() * 40),
                                    Actions.fadeIn((float) Math.random() + 0.3f),
                                    Actions.delay((float) Math.random() * 3),
                                    Actions.fadeOut((float) Math.random() + 0.3f),

                                    Actions.scaleTo(1f, 1f),
                                    Actions.delay((float) Math.random() * 40 + 5),
                                    Actions.fadeIn((float) Math.random() + 0.3f),
                                    Actions.delay((float) Math.random() * 3),
                                    Actions.fadeOut((float) Math.random() + 0.3f),

                                    Actions.scaleTo(1f, 1f),
                                    Actions.delay((float) Math.random() * 40 + 5),
                                    Actions.fadeIn((float) Math.random() + 0.3f),
                                    Actions.delay((float) Math.random() * 3),
                                    Actions.fadeOut((float) Math.random() + 0.3f)
                            ),
                                Actions.sequence(
                                        Actions.delay(0.5f),
                                        Actions.scaleBy(mathRandom, mathRandom, mathRandom * 5),
                                        Actions.scaleBy(-mathRandom, -mathRandom, mathRandom * 5)
                                )
                        )
                ));
                addActor(image);
                image.addListener(new ClickListener() {
                    @Override
                    public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                        image.addAction(Actions.parallel(
                                Actions.scaleTo(1.3f, 1.3f, 0.25f),
                                Actions.alpha(0, 0.25f)
                        ));
                    }
                });
            }
        }



        ballImage = new Image(new Sprite(Main.assetManager.get(Main.BALL_PATH, Texture.class)));
        ballImage.setBounds(
                562,
                44,
                802,
                802
        );
        ballImage.setTouchable(Touchable.disabled);
        ballImage.addAction(Actions.forever(
                Actions.sequence(
                Actions.color(Progress.colors[0], 5f),
                Actions.color(Progress.colors[1], 5f),
                Actions.color(Progress.colors[2], 5f),
                Actions.color(Progress.colors[3], 5f),
                Actions.color(Progress.colors[4], 5f),
                Actions.color(Progress.colors[5], 5f),
                Actions.color(Progress.colors[6], 5f),
                Actions.color(Progress.colors[7], 5f),
                Actions.color(Progress.colors[8], 5f),
                Actions.color(Progress.colors[9], 5f),
                Actions.color(Progress.colors[10], 5f),
                Actions.color(Progress.colors[11], 5f),
                Actions.color(Progress.colors[12], 5f),
                Actions.color(Progress.colors[13], 5f)
                        )
        ));
        ballImage.setOrigin(Align.center);
        ballImage.addCaptureListener(new ClickListener() {
            int cnt = 0;
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ++cnt;
                if (cnt == 10) {
                    cnt = 0;
                    ballImage.addAction(
                            Actions.sequence(
                                    Actions.parallel(
                                            Actions.rotateBy(-360, 2),
                                            Actions.moveBy(1600, 0, 2)
                                    ),
                                    Actions.moveTo(562 - 1600, 44),
                                    Actions.parallel(
                                            Actions.rotateBy(-360, 2),
                                            Actions.moveBy(1600, 0, 2)
                                    )
                            )
                    );
                }
            }
        });

        // EXIT BUTTON
        exitButton = new TextImageButtonGroup(
                new SpriteDrawable(new Sprite(Main.assetManager.get(Main.EXIT_ICON_PATH, Texture.class))),
                null, null,
                new MyClickListener(
                        needExitConfirmation
                                ? () -> exitConfirmation.show()
                                : ifYesAction),
                screenWidth - 135,
                screenHeight - 135,
                135, 135,
                "", "", 100, Color.BLACK, Align.center, ""
        );
        addActor(exitButton);

        imageLabel = new Image(new SpriteDrawable(new Sprite(Main.assetManager.get(Main.TITLE_PATH, Texture.class))));
        imageLabel.setPosition(116, 787);
        imageLabel.setTouchable(Touchable.disabled);
        addActor(imageLabel);

        button1 = new AImageTextButton(enTextBtn1, ruTextBtn1, 80, Color.BLACK, listener1);
        button1.setColor(BUTTONS_COLOR);
        button1.setBounds(70, 333, 500, 115);
        addActor(button1);

        button3 = new AImageTextButton(enTextBtn3, ruTextBtn3, 80, Color.BLACK, listener3);
        button3.setColor(BUTTONS_COLOR);
        button3.setBounds(140, 163, 500, 115);
        addActor(button3);

        button2 = new AImageTextButton(enTextBtn2, ruTextBtn2, 80, Color.BLACK, listener2);
        button2.setColor(BUTTONS_COLOR);
        button2.setBounds(screenWidth - 70 - 500, 333, 500, 115);
        addActor(button2);

        button4 = new AImageTextButton(enTextBtn4, ruTextBtn4, 80, Color.BLACK, listener4);
        button4.setBounds(screenWidth - 140 - 500, 163, 500, 115);
        button4.setColor(BUTTONS_COLOR);
        addActor(button4);

        button1.addAction(Actions.alpha(0));
        button2.addAction(Actions.alpha(0));
        button3.addAction(Actions.alpha(0));
        button4.addAction(Actions.alpha(0));
        exitButton.addAction(Actions.alpha(0));
        imageLabel.addAction(Actions.alpha(0));

        ballImage.addAction(
                Actions.forever(
                        Actions.sequence(
                                Actions.parallel(
                                        Actions.rotateBy(-360, 0.75f),
                                        Actions.moveBy(1600, 0, 0.75f)
                                ),
                                Actions.moveTo(562 - 1600, 44),
                                Actions.parallel(
                                        Actions.rotateBy(-360, 0.75f),
                                        Actions.moveBy(1600, 0, 0.75f)
                                )
                        )
                )
        );

        exitButton.button.setColor(BUTTONS_COLOR);

        loadingLabel = new ALabel("loading", "загрузка", 240, Color.BLACK, Align.center, 8, Color.WHITE);
        loadingLabel.setBounds(0, 0, screenWidth, screenHeight);
        loadingLabel.setTouchable(Touchable.disabled);

        addTo.addActor(this);
        addActor(ballImage);
        ballImage.setTouchable(Touchable.enabled);
        addTo.addActor(exitConfirmation);
        imageLabel.setZIndex(65536);

        addActor(loadingLabel);
    }

    public void showExitConfirmation() {
        exitConfirmation.show();
    }

    private ALabel loadingLabel;

    void showButtons() {
        button1.addAction(Actions.alpha(1f, 2f));
        button2.addAction(Actions.alpha(1f, 2f));
        button3.addAction(Actions.alpha(1f, 2f));
        button4.addAction(Actions.alpha(1f, 2f));
        exitButton.addAction(Actions.alpha(1f, 2f));
        imageLabel.addAction(Actions.alpha(1f, 2f));
        loadingLabel.addAction(Actions.alpha(0f, 2f));
        ballImage.clearActions();
        ballImage.addAction(
                Actions.sequence(
                        Actions.alpha(0, 1f),
                        Actions.rotateTo(0),
                        Actions.moveTo(562, 44),
                        Actions.alpha(1, 1f)
                )
        );
    }

}
