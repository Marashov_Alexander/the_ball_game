package com.mygdx.ball;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Align;
import com.mygdx.ball.actor_classes.ALabel;
import com.mygdx.ball.actor_classes.ATable;
import com.mygdx.ball.actor_classes.TextImageButtonGroup;

class ManualItem extends ATable {

    ManualItem(float width, float height,
               int textSize, Color textColor,
               InputListener listener,
               Drawable drawable, String enText, String ruText, String enDescription, String ruDescription, String name) {
        super();
        setName(name + "_manual");
        setBounds(0, 0, width, height);

        ALabel parameterLabel = new ALabel(
                enDescription,
                ruDescription,
                textSize, textColor, Align.left
        );

        TextImageButtonGroup image = new TextImageButtonGroup(
                drawable,
                drawable,
                null,
                listener,
                0, 0, height, height,
                enText, ruText, 60 * 2, Color.BLACK, Align.center, null
        );
        image.button.setDisabled(true);
        add(image, height, height).space(30).align(Align.left);
        add(parameterLabel, width - height - 60, height).space(30).align(Align.left);
        parameterLabel.setWrap(true);
    }
}

