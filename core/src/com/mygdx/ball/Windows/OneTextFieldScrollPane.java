//package com.mygdx.game.Windows;
//
//import com.badlogic.gdx.graphics.Color;
//import com.badlogic.gdx.scenes.scene2d.Actor;
//import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
//import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
//import com.badlogic.gdx.utils.Align;
//import com.mygdx.game.Main;
//import com.mygdx.game.actor_classes.AImageTextButton;
//import com.mygdx.game.actor_classes.ALabel;
//import com.mygdx.game.actor_classes.ATable;
//import com.mygdx.game.actor_classes.ATextField;
//
//public class OneTextFieldScrollPane extends ScrollPane {
//
//    private ATable table;
//    private ALabel label;
//    private ATextField nameField;
//    private AImageTextButton btnOk;
//    private ClickListener btnOkListener;
//    private boolean isInitialized;
//
//    public OneTextFieldScrollPane(ClickListener btnOkListener) {
//        super(new ATable(), Main.scrollPaneStyle);
//        this.btnOkListener = btnOkListener;
//        isInitialized = true;
//        table = (ATable) getWidget();
//
//        label = new ALabel(
//                "Enter the opponent nickname",
//                80,
//                Color.WHITE,
//                Align.center
//        );
//
//        nameField = new ATextField(
//                "", 80,
//                Color.WHITE,
//                Align.center,
//                30
//        );
//
//        btnOk = new AImageTextButton(
//                "Invite", 80,
//                Color.BLACK,
//                btnOkListener
//        );
//    }
//
//    public String getPeerUsername() {
//        return nameField.getText();
//    }
//
//    @Override
//    public void setSize(float width, float height) {
//        super.setSize(width, height);
//        if (isInitialized) {
//            table.setSize(width, height);
//            table.clearChildren();
//            table.add(label, table.getWP(0.8f), table.getHP(0.2f))
//                    .align(Align.center).space(table.getHP(0.1f)).row();;
//            table.add(nameField, table.getWP(0.6f), table.getHP(0.2f))
//                    .align(Align.center).space(table.getHP(0.1f)).row();
//            table.add(btnOk, table.getWP(0.2f), table.getHP(0.2f))
//                    .align(Align.bottomRight).space(table.getHP(0.05f)).row();
//        }
//    }
//
//    public Actor getNameField() {
//        return nameField;
//    }
//}
