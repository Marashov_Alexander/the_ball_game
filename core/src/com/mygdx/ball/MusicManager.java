package com.mygdx.ball;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

public class MusicManager {

    public enum Melody {
        menu,
        game,
        gameOver,
        clock
    }

    public enum SoundTrack {
        cageTouch,
        cageHit,
        ballCaught,
    }

    private Music menu;
    private Music game;
    private Music gameOver;
    private Music clock;

    private Sound cageTouch;
    private Sound cageHit;
    private Sound ballCaught;

    private float volume;


    public MusicManager(float volume) {
        menu = Gdx.audio.newMusic(Gdx.files.internal("music/menu.mp3"));
        game = Gdx.audio.newMusic(Gdx.files.internal("music/game.mp3"));
        gameOver = Gdx.audio.newMusic(Gdx.files.internal("music/loose.mp3"));
        clock = Gdx.audio.newMusic(Gdx.files.internal("music/time.mp3"));

        cageTouch = Gdx.audio.newSound(Gdx.files.internal("music/cageTouch.mp3"));
        cageHit = Gdx.audio.newSound(Gdx.files.internal("music/cageHit.mp3"));
        ballCaught = Gdx.audio.newSound(Gdx.files.internal("music/catch.mp3"));

        this.volume = volume;

        menu.setLooping(true);
        game.setLooping(true);
        clock.setLooping(true);

        menu.setVolume(volume);
        game.setVolume(volume);
        gameOver.setVolume(volume);
        clock.setVolume(volume);
    }

    public void playMusic(Melody melody) {
        if (Main.enableMusic) {

            switch (melody) {
                case menu:
                    game.stop();
                    gameOver.stop();
                    clock.stop();
                    menu.play();
                    break;
                case game:
                    menu.stop();
                    gameOver.stop();
                    clock.stop();
                    game.play();
                    break;
                case gameOver:
                    menu.stop();
                    game.stop();
                    clock.stop();
                    gameOver.play();
                    break;
                case clock:
                    menu.stop();
                    game.stop();
                    gameOver.stop();
                    clock.play();
                    break;
            }
        }
    }

    public void stopMusic() {
        menu.stop();
        game.stop();
        gameOver.stop();
        clock.stop();
    }

    public void playSound(SoundTrack soundtrack) {
        if (Main.enableSounds) {

            switch (soundtrack) {
                case cageTouch:
                    cageTouch.play(volume);
                    break;
                case cageHit:
                    cageHit.play(volume);
                    break;
                case ballCaught:
                    ballCaught.play(volume);
                    break;
            }
        }
    }
}
