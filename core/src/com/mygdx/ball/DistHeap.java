package com.mygdx.ball;

public class DistHeap {
    private Cage[] heapArray;
    private int[][] dist;
    private int pointer;

    DistHeap(final int size, int[][] dist) {
        heapArray = new Cage[size + 1];
        pointer = 1;
        this.dist = dist;
    }

    boolean isEmpty() {
        return pointer == 1;
    }

    void clear() {
        pointer = 1;
    }

    void insert(final Cage x) {
        heapArray[pointer] = x;
        swim(pointer);
        ++pointer;
    }

    Cage extractMin() {
        --pointer;
        Cage tmp = heapArray[1];
        heapArray[1] = heapArray[pointer];
        heapArray[pointer] = tmp;
        swim(sinkFirst());
        return heapArray[pointer];
    }

    private void swim(int index) {
        while (index != 1
                && dist[heapArray[index / 2].xIndex][heapArray[index / 2].yIndex]
                > dist[heapArray[index].xIndex][heapArray[index].yIndex]) {
            final Cage tmp = heapArray[index / 2];
            heapArray[index / 2] = heapArray[index];
            heapArray[index] = tmp;
            index /= 2;
        }
    }

    private int sinkFirst() {
        int index = 1;
        int direction = index * 2;
        while (direction < pointer - 1) {
            if (dist[heapArray[direction].xIndex][heapArray[direction].yIndex]
                    > dist[heapArray[direction + 1].xIndex][heapArray[direction + 1].yIndex]) {
                direction++;
            }
            final Cage tmp = heapArray[direction];
            heapArray[direction] = heapArray[index];
            heapArray[index] = tmp;
            index = direction;
            direction *= 2;
        }
        if (direction == pointer - 1) {
            final Cage tmp = heapArray[direction];
            heapArray[direction] = heapArray[index];
            heapArray[index] = tmp;
            index = direction;
        }
        return index;
    }
}