package com.mygdx.ball;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Align;
import com.mygdx.ball.actor_classes.ADrawable;

import static com.mygdx.ball.Cage.DEFAULT_COLOR;
import static com.mygdx.ball.Main.ESCAPED_BALL_DRAWABLE;

public class Ball extends Image {

    public int x;
    public int y;
    int step;

    int colorIndex;
    private float size;
    private float worldSpace;

    World world;

    private boolean ballGameEnabled;
    int bonus;
    int stepRemains;

    private Image border;
    private Drawable movingDrawable;
    private Drawable normalDrawable;

    boolean wantToDoMagic = true;
    Cage prefPortal;
    BallStrategy strategy;

    Ball(Sprite sprite, int x, int y, float size, float worldSpace, int step, int colorIndex, boolean ballGameEnabled) {
        super(sprite);

        if (ballGameEnabled) {
            border = new Image(Cage.cageBorderReadyBall);
            border.setTouchable(Touchable.disabled);
            border.addAction(Actions.forever(Actions.sequence(
                    Actions.alpha(0.25f, 1f),
                    Actions.alpha(1f, 1f)
            )));
            movingDrawable = new ADrawable(new Sprite(Main.movingBallTexture), Progress.colors[colorIndex]);
            normalDrawable = getDrawable();
        }

        setBounds(
                (size + worldSpace) * x,
                (size + worldSpace) * y,
                size, size
        );
        this.colorIndex = colorIndex;

         // HERE
//        colorIndex = 2;
//        this.colorIndex = 2;
//        sprite.setColor(Progress.colors[colorIndex]);

        setName(getBallName(colorIndex));

        setColor(Progress.colors[colorIndex]);
        setOrigin(Align.center);
        if (colorIndex > 0 && colorIndex % 4 == 0) {
            step *= 3;
        }
        this.x = x;
        this.y = y;
        this.size = size;
        this.worldSpace = worldSpace;
        this.step = step;

        setZIndex(100);
        this.ballGameEnabled = ballGameEnabled;
        setTouchable(Touchable.enabled);

        if (ballGameEnabled) {
            setVisible(true);
            toFront();
            bonus = -1;
            stepRemains = step;
        } else {
            setVisible(false);
        }
    }

    public void createBallStrategy(World world, int sizeX, int sizeY) {
        if (strategy == null) {
            this.world = world;
            strategy = new BallStrategy(world, sizeX, sizeY);
        }
    }

    private static String getBallName(int colorIndex) {
        return Progress.colors[colorIndex].toString();
    }

    static String getBallName(Color color) {
        return color.toString();
    }

    // когда на мяч попали
    @Override
    public void setVisible(boolean visible) {

        if (visible == isVisible())
            return;

        if (((visible && !ballGameEnabled) || (!visible && ballGameEnabled)) && world != null) {

            // синий шар красит клетку в синий, становится обычным и уходит на соседнюю
            if (getColor().equals(Progress.colors[1])) {
//                world.world[x][y].button.setColor(Progress.colors[1]);
                world.world[x][y].setImage(ESCAPED_BALL_DRAWABLE);
                world.world[x][y].label.setText("");
                if (moveBall(world)) {
                    if (ballGameEnabled) {
                        nextRound();
                    }
                    setColor(Progress.colors[0]);
                } else {
                    super.setVisible(visible);
                }
                System.out.println(Progress.colors[1]);
                return;

            // зелёный шар отжимает нажатые кнопки
            }
            if (colorIndex > 0 && colorIndex % 2 == 0) {

            // при активации красного шара атакуются и соседние клетки
            }
            if (colorIndex > 0 && colorIndex % 3 == 0) {
                super.setVisible(visible);
                world.hitCross(x, y);

            // жёлтый шар очень быстро перемещается
            }
            if (colorIndex > 0 && colorIndex % 4 == 0) {

            // голубой шар при активации плодит не больше 4х простых шаров
            }
            if (colorIndex > 0 && colorIndex % 5 == 0) {
                super.setVisible(visible);
                world.spawnCross(x, y);

            // чёрный шар красит клетки в чёрный, если дистанция до него меньше его скорости.
                // Сам не перемещается?
                // когда пойман, раскрашивает все клетки в нормальный цвет
            }
            if (colorIndex > 0 && colorIndex % 6 == 0) {
                for (Cage[] cages : world.world) {
                    for (Cage cage : cages) {
                        if (cage.button.getColor().equals(Color.BLACK)) {
                            cage.button.setColor(DEFAULT_COLOR);
                        }
                    }
                }
            }

            if (colorIndex > 0 && colorIndex % 13 == 0 && Math.random() > 0.5) {
                System.out.println("WOW!!!");
                world.world[x][y].setImage(new SpriteDrawable(new Sprite(Main.assetManager.get(Main.SHIELD_PATH, Texture.class))));
                world.world[x][y].button.setChecked(false);
                if (!ballGameEnabled)
                    makeStep(world, new Direction(world, x, y, colorIndex));
                return;
            }

        } else {

        }

        super.setVisible(visible);
    }

    boolean isAlive() {
        return isVisible() && ballGameEnabled || !isVisible() && !ballGameEnabled;
    }


    void setActions(int toX, int toY) {

        if (strategy == null) {
            System.err.println("Ball strategy disabled!");
            return;
        }

        Action[] actions = strategy.getActions(this, world.world[toX][toY]);

        clearActions();
        if (actions == null) {
            addAction(Actions.sequence(
                    Actions.parallel(
                            Actions.moveTo(x * (Main.cageSize + Main.cageSpace), y * (Main.cageSize + Main.cageSpace), 0.25f),
                            Actions.scaleTo(1, 1, 0.25f),
                            Actions.rotateTo(0, 0.25f)
                    ),
                    Actions.run(() -> setDrawable(normalDrawable))
            ));
            return;
        }
        setDrawable(movingDrawable);

        addAction(Actions.sequence(
                Actions.parallel(
                        Actions.moveTo(x * (Main.cageSize + Main.cageSpace), y * (Main.cageSize + Main.cageSpace), 0.25f),
                        Actions.scaleTo(1, 1, 0.25f),
                        Actions.rotateTo(0, 0.25f)
                ),
                Actions.sequence(actions),
                Actions.run(() -> setDrawable(normalDrawable))
        ));
    }

    void buildBridge(int newBallX, int newBallY) {
        boolean isBlue = colorIndex == 1;

        if (isBlue && world.world[newBallX][newBallY].getColor() == null) {
            world.world[newBallX][newBallY].setColor(DEFAULT_COLOR);
            addBallHint();
        }
    }

    static class Direction {
        int dx = 0;
        int dy = 0;
        Direction(World world, int x, int y, int colorIndex) {
            if (Progress.exitEnabled && world.exitCages != null) {
                int minDist = 100_000;
                Cage minCage = null;
                for (Cage exitCage : world.exitCages) {
                    int d = world.getDist(exitCage.xIndex, exitCage.yIndex, x, y);
                    if (minDist > d) {
                        minCage = exitCage;
                        minDist = d;
                    }
                }
                if (minCage == null)
                    return;
                boolean right = x < minCage.xIndex
                        && (colorIndex < 7 || !world.world[x + 1][y].isTrap)
                        && !world.world[x + 1][y].button.isChecked() && world.freeFromBalls(x + 1, y);
                boolean left = x > minCage.xIndex
                        && (colorIndex < 7 || !world.world[x - 1][y].isTrap)
                        && !world.world[x - 1][y].button.isChecked() && world.freeFromBalls(x - 1, y);
                boolean up = y < minCage.yIndex
                        && (colorIndex < 7 || !world.world[x][y + 1].isTrap)
                        && !world.world[x][y + 1].button.isChecked() && world.freeFromBalls(x, y + 1);
                boolean down = y > minCage.yIndex
                        && !world.world[x][y - 1].isTrap
                        && !world.world[x][y - 1].button.isChecked() && world.freeFromBalls(x, y - 1);

                dx += right ? 1 : left ? -1 : 0;
                dy += dx != 0 ? 0 : up ? 1 : down ? -1 : 0;
            } else {
                int direction = (int) Math.round(Math.random() * 5) % 5;
                switch (direction) {
                    case 0: {
                        // up
                        dy = 1;
                        break;
                    }
                    case 1: {
                        // down
                        dy = -1;
                        break;
                    }
                    case 2: {
                        // left
                        dx = -1;
                        break;
                    }
                    case 3: {
                        // right
                        dx = 1;
                        break;
                    }
                    default: {
                        // nothing
                    }
                }
            }
        }

        Direction(int dx, int dy) {
            this.dx = dx;
            this.dy = dy;
        }
    }

    boolean makeStep(World world, Direction direction) {
        boolean result = false;
        this.world = world;

        int indexX = x + direction.dx;
        int indexY = y + direction.dy;

        if (indexX >= 0 && indexX < world.world.length
                && indexY >= 0 && indexY < world.world[0].length
                && world.freeFromBalls(indexX, indexY)
                && world.freeFromBalls(indexX, indexY)) {

            cageRotating(indexX, indexY);

            if (!world.world[indexX][indexY].button.isChecked() || (colorIndex > 0 && colorIndex % 2 == 0)) {
                result = true;
                x += direction.dx;
                y += direction.dy;
                setPosition(
                        x * (size + worldSpace),
                        y * (size + worldSpace)
                );

            }
        }

        result &= cagesEffects();

        if (result && ballGameEnabled) {
            stepRemains--;
            if (stepRemains == 0) {
                border.setVisible(false);
            }
        }
        return result;
    }

    void cageRotating(int indexX, int indexY) {
        boolean isGreen = colorIndex > 0 && colorIndex % 2 == 0;
        boolean isYellow = colorIndex > 0 && colorIndex % 4 == 0;

        if (isGreen && world.world[indexX][indexY].button.isChecked()) {
            world.rotateCageBack(indexX, indexY, () -> world.world[indexX][indexY].button.setColor(isYellow && Math.random() > 0.5 ? Color.YELLOW : DEFAULT_COLOR));
            addBallHint();
        }
    }

    private void addBallHint() {
        if (Progress.ballStrategyGame) {
            world.playerAI.addHintBall(this);
        }
    }

    boolean cagesEffects() {

        if (world.world[x][y].isTrap) {
            world.hitCross(x, y);
            if (isAlive()) {
                world.hitCage(x, y);
            }
            return false;
        }

        // эффекты клеток:
        switch (world.world[x][y].cageType) {
            case simple:
                if (colorIndex > 0 && colorIndex % 11 == 0 && Math.random() > 0.95f) {
                    world.world[x][y].setMagic();
                    world.portalCages.add(world.world[x][y]);
                    addBallHint();
                }

                if (colorIndex > 0 && colorIndex % 14 == 0 && Math.random() > 0.95f) {
                    world.world[x][y].setFire();
                    addBallHint();
                }
                break;
            case wall:
                break;
            case exit: {
                if (!ballGameEnabled)
                    world.gameOver("the Ball has escaped!", "мяч сбежал!");
                else {
                    world.ballEscaped(this);
                }
                break;
            }
            case magic:
                if (wantToDoMagic) {
                    world.magicMoveBall(this, x, y, prefPortal);
                }
                break;
            case bonus:
                if (ballGameEnabled) {
                    bonus = world.world[x][y].bonus;
                    world.world[x][y].clearBonus();
                    addBallHint();
                }
                break;
        }
        return true;
    }

    boolean moveBall(World world) {
        boolean result = false;
        int oldX = x;
        int oldY = y;
        for (int i = 0; i < step; ++i) {
            Direction direction = new Direction(world, x, y, colorIndex);
            result |= makeStep(world, direction);
            if (!isAlive()) {
                break;
            }
        }

        return result && !(x == oldX && y == oldY);
    }

    int getCageX() {
        return x;
    }

    int getCageY() {
        return y;
    }

    int getStep() {
        return step;
    }

    void moveTo(int newX, int newY) {
        x = newX;
        y = newY;
        setPosition(
                x * (size + worldSpace),
                y * (size + worldSpace)
        );
    }

    void setStep(int step) {
        this.step = step;
    }

    boolean hasEnoughStep() {
        return stepRemains > 0;
    }
    void nextRound() {
        stepRemains = step;
        border.setVisible(true);
    }

    @Override
    public void setPosition(float x, float y) {
       if (border != null)
           border.setPosition(x, y);
        super.setPosition(x, y);
    }

    @Override
    public void setSize(float width, float height) {
        if (border != null)
            border.setSize(width, height);
        super.setSize(width, height);
    }

    @Override
    public void setBounds(float x, float y, float width, float height) {
        if (border != null)
            border.setBounds(x, y, width, height);
        super.setBounds(x, y, width, height);
    }

    @Override
    protected void setParent(Group parent) {
        if (border != null && parent != null)
            parent.addActor(border);
        super.setParent(parent);
    }
}
