package com.mygdx.ball;

public interface AdsController {

    public void showRewardedVideoAd();
    public void loadRewardedVideoAd();

    public void showVideoAd();
    public void loadVideoAd();
}
