package com.mygdx.ball;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.mygdx.ball.Windows.AWindow;
import com.mygdx.ball.Windows.ActorsScrollPane;
import com.mygdx.ball.Windows.TextScrollPane;
import com.mygdx.ball.Windows.YesNoScrollPane;
import com.mygdx.ball.actor_classes.ADrawable;
import com.mygdx.ball.actor_classes.AImageTextButton;
import com.mygdx.ball.actor_classes.StageSlider;

import static com.mygdx.ball.Main.enableMusic;
import static com.mygdx.ball.Main.enableSounds;
import static com.mygdx.ball.Main.isRussian;
import static com.mygdx.ball.Main.manual;
import static com.mygdx.ball.Main.screenHeight;
import static com.mygdx.ball.Main.screenWidth;
import static com.mygdx.ball.Main.skipTutorials;

class MenuScreen implements Screen {

    private final Main main;
    private MenuScreenTemplate menuScreenTemplate;

    private AWindow aboutWindow;
    private Stage stage;
    public Progress progress;

    private AWindow adsWindow;
    private AWindow loadGameWindow;
    private AWindow shopWindow;
    private AWindow tutorialWindow;
    private AWindow shopConfirmWindow;
    private ActorsScrollPane shopScrollPane;

    private StageSlider stageSlider;
    private AWindow settingsWindow;
    private ShopItem[] shopItems;

    private ActorsScrollPane settingsScrollPane;

    private Callable afterLoadingCallable;

    MenuScreen(Main main, Callable callable) {
        this.afterLoadingCallable = callable;
        this.main = main;
        stage = new Stage(new StretchViewport(screenWidth, screenHeight, main.camera)) {
            @Override
            public boolean keyDown(int keyCode) {

                if(!Main.loading && keyCode == Input.Keys.BACK){
                    if (settingsWindow.isShowed()) {
                        settingsWindow.hide();
                    } else if (tutorialWindow.isShowed()) {
                        tutorialWindow.hide();
                    } else if (aboutWindow.isShowed()) {
                        aboutWindow.hide();
                    } else if (adsWindow.isShowed()) {
                        adsWindow.hide();
                    } else if (loadGameWindow.isShowed()) {
                        loadGameWindow.hide();
                    } else if (shopWindow.isShowed()) {
                        shopWindow.hide();
                    } else if (menuScreenTemplate.exitConfirmation.isShowed()){
                        menuScreenTemplate.exitConfirmation.hide();
                    } else {
                        menuScreenTemplate.exitConfirmation.show();
                    }

                    return true;
                }

                return super.keyDown(keyCode);
            }
        };

        menuScreenTemplate = new MenuScreenTemplate(
                stage, "Find the Ball", "Find the Ball",
                "Play", "Играть", new MyClickListener(() -> {
            Main.musicManager.playSound(MusicManager.SoundTrack.cageTouch);
            Main.vibrate(50);
            stageSlider.setState(Progress.getLastState(true));
            loadGameWindow.show();
        }),
                "Settings", "Настройки", new MyClickListener(() -> {
            Main.musicManager.playSound(MusicManager.SoundTrack.cageTouch);
            Main.vibrate(50);
            showSettingsWindow();
        }),
                "Shop", "Магазин", new MyClickListener(() -> {
            Main.musicManager.playSound(MusicManager.SoundTrack.cageTouch);
            Main.vibrate(50);
            showShopWindow();
        }),
                "About", "Об игре", new MyClickListener(() -> {
            Main.musicManager.playSound(MusicManager.SoundTrack.cageTouch);
            Main.vibrate(50);
            aboutWindow.show();
        }),
                () -> Gdx.app.exit(),
                () -> {
                },
                true

        );


    }

    public void loadingFinished() {
        progress = new Progress(Main.cageSize, Main.cageSpace);

        // добавление мячей в справочник
        Actor uselessActor = new Actor();
        for (Color color : Progress.colors) {
            uselessActor.setName(Ball.getBallName(color));
            manual.addItem(uselessActor,
                    new ADrawable(new Sprite(Main.ballTexture), 200, 200, color),
                    "", ""
            );
        }

        shopWindow = new AWindow(
                "Shop", "Магазин", "Coins: " + progress.getMoney(), "Монет: " + progress.getMoney(),
                shopScrollPane = new ActorsScrollPane(screenWidth * 0.9f, screenHeight * 0.6f, 70, Color.WHITE) ,
                1, 1, true,
                140, 70, 0, 0, 0, 0
        );


        YesNoScrollPane yesNoScrollPane;
        adsWindow = new AWindow(
                "Get coins", "Получить монеты",
                yesNoScrollPane = new YesNoScrollPane(
                        "Do you want to watch advertisement for 150 coins?", "Хотите посмотреть рекламу и получить 150 монет?",
                        () -> {},
                        () -> {}
                ),
                0.75f, 0.75f,
                true
        );

        yesNoScrollPane.addNoListener(new MyClickListener(() -> {
            adsWindow.hide();
        }));

        yesNoScrollPane.addYesListener(new MyClickListener(() -> {
            Main.adsController.showRewardedVideoAd();
            adsWindow.hide();
            shopWindow.hide();
        }));

        AImageTextButton giftButton = new AImageTextButton("Get coins", "Получить монеты", 60, Color.BLACK,
                new MyClickListener(() -> {
                    adsWindow.show();
                }));

        giftButton.setBounds(screenWidth - 1100, 60, 600, 75);
        shopWindow.addActor(giftButton);

        shopWindow.addBtnAction(new MyClickListener(() -> {
            shopScrollPane.scrollTo(
                    0,
                    0,
                    shopScrollPane.getWidth(),
                    shopScrollPane.getHeight()
            );
        }));

        YesNoScrollPane shopYNSP;
        shopConfirmWindow = new AWindow(
                "Confirmation", "Подтверждение", "", "",
                shopYNSP = new YesNoScrollPane(
                        "", "",
                        () -> {},
                        () -> {}
                ),
                0.75f, 0.75f,
                true, 120, 100, 60, 60, 60, 60
        );

        shopYNSP.addNoListener(new MyClickListener(() -> {
            shopConfirmWindow.hide();
        }));

        shopItems = new ShopItem[3 + Progress.FREE_ITEM_INDEX];
        for (int i = 0; i < 3; ++i) {
            int finalI = i;
            int finalI1 = i;
            shopItems[i] = new ShopItem(
                    shopScrollPane.getWidth(), shopScrollPane.getHeight() * 0.3f,
                    progress.getItemUpdateDescription(true, i), progress.getItemUpdateDescription(false, i),
                    70, Color.WHITE, progress.getUpdateItemCost(i),
                    progress.getUpdateActionDrawable(i), new MyClickListener(() -> {
                Main.musicManager.playSound(MusicManager.SoundTrack.cageTouch);

                if (progress.getUpdateItemCost(finalI) == 0 || progress.getUpdateItemCost(finalI) > progress.getMoney()) {
                    return;
                }

                shopYNSP.setText(progress.getItemUpdateDescription(true, finalI1) + ". Do you want to buy it?",
                        progress.getItemUpdateDescription(false, finalI1) + ". Желаете приобрести?");
                shopConfirmWindow.setSubtitle("", "");
                shopYNSP.setYesListener(
                        new MyClickListener(() -> {
                            if (!progress.buyItem(finalI)) {
                                shopConfirmWindow.setSubtitle("No enough money", "Не хватает монет");
                            } else {
                                Main.musicManager.playSound(MusicManager.SoundTrack.ballCaught);
                                progress.saveProgress();
                                shopItems[finalI].setItem(
                                        progress.getItemUpdateDescription(true, finalI), progress.getItemUpdateDescription(false, finalI),
                                        progress.getUpdateItemCost(finalI),
                                        progress.getUpdateActionDrawable(finalI)

                                );
                                shopWindow.subtitle.setText("Coins: " + progress.getMoney(), "Монет: " + progress.getMoney());
                                updateShopItemsColors();
                            }
                            shopConfirmWindow.hide();
                        })
                );
                shopConfirmWindow.show();
            }));
            shopScrollPane.addPlusRow(shopItems[i],
                    1f, 0.3f, 0.05f, Align.left);
        }

        for (int i = 0; i < Progress.FREE_ITEM_INDEX; ++i) {
            final int finalI = i;
            shopItems[i + 3] = new ShopItem(
                    shopScrollPane.getWidth(), shopScrollPane.getHeight() * 0.3f,
                    progress.getBonusDescription(true, i), progress.getBonusDescription(false, i),
                    70, Color.WHITE, progress.getUpdateBonusCost(i),
                    Progress.getBonusDrawable(i), new MyClickListener(() -> {

                Main.musicManager.playSound(MusicManager.SoundTrack.cageTouch);

                if (progress.getUpdateBonusCost(finalI) > progress.getMoney()) {
                    return;
                }

                shopYNSP.setText(progress.getBonusDescription(true, finalI) + ". Do you want to buy it?",
                        progress.getBonusDescription(false, finalI) + ". Желаете приобрести?");
                shopConfirmWindow.setSubtitle("", "");
                shopYNSP.setYesListener(
                        new MyClickListener(() -> {

                            if (!progress.buyBonus(finalI)) {
                                shopConfirmWindow.setSubtitle("No enough money", "Не хватает монет");
                            } else {
                                Main.musicManager.playSound(MusicManager.SoundTrack.ballCaught);
                                progress.saveProgress();
                                shopItems[finalI + 3].setText(
                                        progress.getBonusCount(finalI)
                                );
                                shopWindow.subtitle.setText("Coins: " + progress.getMoney(), "Монет: " + progress.getMoney());
                                updateShopItemsColors();
                            }
                            shopConfirmWindow.hide();
                        })
                );
                shopConfirmWindow.show();
            }));

            shopScrollPane.addPlusRow(shopItems[i + 3],
                    1f, 0.3f, 0.05f, Align.left);
        }

        settingsWindow = new AWindow(
                "Settings",
                "Настройки",
                "", "",
                settingsScrollPane = new ActorsScrollPane(
                        screenWidth * 0.9f,
                        screenHeight * 0.6f,
                        80, Color.WHITE
                ),
                1f, 1f, true,
                100, 80, 0, 0, 0, 0
        );

        settingsWindow.addBtnAction(new MyClickListener(() -> {
            settingsScrollPane.scrollTo(
                    0,
                    0,
                    settingsScrollPane.getWidth(),
                    settingsScrollPane.getHeight()
            );
        }));

        settingsWindow.addBtnAction(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Main.saveSettings();
            }
        });

        AWindow tmpWindow = new AWindow("Информация", "Information", new TextScrollPane("Перезапустите приложение для смены языка",
                "Restart the app to change the language", Align.center), 0.5f, 0.5f, false);

        settingsScrollPane.addPlusRow(
                new SettingsItem(
                        settingsScrollPane.getWidth(), settingsScrollPane.getHeight() * 0.3f,
                        "Русский язык", "Russian language",
                        80, Color.WHITE,
                        new MyClickListener(() -> {
                            isRussian = !isRussian;
                            Main.vibrate(50);
                            Main.musicManager.playSound(MusicManager.SoundTrack.cageTouch);
                            tmpWindow.show();
                            Main.saveSettings();
                        }),
                        isRussian
                ), 1, 0.3f, 0.05f, Align.left
        );

        settingsScrollPane.addPlusRow(
                new SettingsItem(
                        settingsScrollPane.getWidth(), settingsScrollPane.getHeight() * 0.3f,
                        "Tutorials", "Обучение",
                        80, Color.WHITE,
                        new MyClickListener(() -> {
                            Main.musicManager.playSound(MusicManager.SoundTrack.cageTouch);
                            Main.vibrate(50);
                            Main.skipTutorials = !Main.skipTutorials;
                        }),
                        !Main.skipTutorials
                ), 1, 0.3f, 0.05f, Align.left
        );

        settingsScrollPane.addPlusRow(
                new SettingsItem(
                        settingsScrollPane.getWidth(), settingsScrollPane.getHeight() * 0.3f,
                        "Vibration", "Вибрация",
                        80, Color.WHITE,
                        new MyClickListener(() -> {
                            Main.musicManager.playSound(MusicManager.SoundTrack.cageTouch);
                            Main.vibrate(50);
                            Main.enableVibration = !Main.enableVibration;
                        }),
                        Main.enableVibration
                ), 1, 0.3f, 0.05f, Align.left
        );

        settingsScrollPane.addPlusRow(
                new SettingsItem(
                        settingsScrollPane.getWidth(), settingsScrollPane.getHeight() * 0.3f,
                        "Music", "Музыка",
                        80, Color.WHITE,
                        new MyClickListener(() -> {
                            Main.enableMusic = !Main.enableMusic;
                            Main.vibrate(50);
                            Main.musicManager.playSound(MusicManager.SoundTrack.cageTouch);
                            if (enableMusic) {
                                Main.musicManager.playMusic(MusicManager.Melody.menu);
                            } else {
                                Main.musicManager.stopMusic();
                            }
                        }),
                        Main.enableMusic
                ), 1, 0.3f, 0.05f, Align.left
        );

        settingsScrollPane.addPlusRow(
                new SettingsItem(
                        settingsScrollPane.getWidth(), settingsScrollPane.getHeight() * 0.3f,
                        "Sounds", "Звуки",
                        80, Color.WHITE,
                        new MyClickListener(() -> {
                            enableSounds = !enableSounds;
                            Main.vibrate(50);
                            if (enableSounds) {
                                Main.musicManager.playSound(MusicManager.SoundTrack.ballCaught);
                            }
                        }),
                        enableSounds
                ), 1, 0.3f, 0.05f, Align.left
        );

        Actor actor;
        loadGameWindow = new AWindow(
                "Choose the level",
                "Выберите уровень",
                "", "",
                actor = new Actor(),
                1f,
                1f,
                true,
                100, 80, 0, 0, 20, 20
        );
        loadGameWindow.setContext(stageSlider = new StageSlider(
                        actor.getWidth(), actor.getHeight(),
                        progress
                )
        );

        YesNoScrollPane tutorialYNSP;
        tutorialWindow = new AWindow(
                "Tutorial", "Обучение",
                tutorialYNSP = new YesNoScrollPane(
                        "Training is available for this level. Do you want to pass it? (You can disable this window in the settings)",
                        "Для этого уровня доступно обучение. Хотите пройти его? (Вы можете полностью отключить обучение в настройках)",
                        () -> {
                            if (Progress.setLevel(stageSlider.currentPage, 0, false)) {
                                main.setScreen(new GameScreen(main, progress));
                            }
                        },
                        () -> {
                            if (Progress.setLevel(stageSlider.currentPage, 0, true)) {
                                main.setScreen(new GameScreen(main, progress));
                            }
                        }
                ),
                1f, 1f, true
        );
        tutorialYNSP.addYesListener(new MyClickListener(tutorialWindow::hide));
        tutorialYNSP.addNoListener(new MyClickListener(tutorialWindow::hide));

        for (int i = 0; i < Progress.LEVEL_IN_STAGE_COUNT; ++i) {
            int finalI = i;
            stageSlider.addButton(() -> {
                if (Progress.levelAvailable(stageSlider.currentPage, finalI)) {
                    if (!skipTutorials && Progress.hasTutorial(finalI)) {
                        tutorialWindow.show();
                    } else {
                        if (Progress.setLevel(stageSlider.currentPage, finalI)) {
                            main.setScreen(new GameScreen(main, progress));
                        }
                    }
                }
            });
        }
        stageSlider.setState(stageSlider.currentPage);

        TextScrollPane tsp = new TextScrollPane(

                "AUTHOR:" +
                        "\n- 4iTerILP" +
                        "\n\nSPECIAL THANKS TO" +
                        "\n- Vladimir Andreevich" +
                        "\n\nTHE MAIN TESTER" +
                        "\n- Borscht with Waffle",

                "СОЗДАТЕЛЬ:" +
                        "\n- 4iTerILP" +
                        "\n\nСПЕЦИАЛЬНАЯ БЛАГОДАРНОСТЬ" +
                        "\n- Владимиру Андреевичу" +
                        "\n\nГЛАВНЫЙ ТЕСТИРОВЩИК" +
                        "\n- Борщ с вафелькой", Align.left);

        aboutWindow = new AWindow(
                "About Find-The-Ball", "Об игре Find-The-Ball", "Version from 03.20.2020", "Версия от 20.03.2020",
                tsp,
                1f, 1f, true, 120, 80, 60, 0, 0, 60
        );

        aboutWindow.title.addListener(new ClickListener() {

            @Override
            public void clicked(InputEvent event, float x, float y) {
                Main.clickCount += 1;
                System.out.println(Main.clickCount);
                if (Main.clickCount == 20) {
                    Main.musicManager.playSound(MusicManager.SoundTrack.ballCaught);
                    aboutWindow.subtitle.setText("Привет от ЧитерИЛПа! :) Бонусы добавлены.");
                    tsp.scrollTo(tsp.getMaxX(), tsp.getMaxY(), tsp.getWidth(), tsp.getHeight());
                    for (int i = 0; i < Progress.FREE_ITEM_INDEX; ++i) {
                        Progress.money += progress.getUpdateBonusCost(i);
                        progress.buyBonus(i);
                    }
                    progress.saveProgress();
                }
            }
        });

        stage.addActor(loadGameWindow);
        stage.addActor(tutorialWindow);
        stage.addActor(settingsWindow);
        stage.addActor(shopWindow);
        stage.addActor(shopConfirmWindow);
        stage.addActor(tmpWindow);
        stage.addActor(aboutWindow);
        stage.addActor(adsWindow);

        shopScrollPane.scrollTo(
                0,
                0,
                shopScrollPane.getWidth(),
                shopScrollPane.getHeight()
        );

        settingsScrollPane.scrollTo(
                0,
                0,
                settingsScrollPane.getWidth(),
                settingsScrollPane.getHeight()
        );

        menuScreenTemplate.showButtons();
    }

    public void showShopWindow() {

        updateShopItemsColors();

        shopWindow.subtitle.setText("Coins: " + progress.getMoney(), "Монет: " + progress.getMoney());
        for (int i = 0; i < Progress.FREE_ITEM_INDEX; ++i) {
            shopItems[i + 3].setText(
                    progress.getBonusCount(i)
            );
        }
        shopWindow.show();
        shopScrollPane.scrollTo(
                0,
                shopScrollPane.getMaxY(),
                shopScrollPane.getWidth(),
                shopScrollPane.getHeight()
        );
    }

    private void updateShopItemsColors() {
        for (ShopItem shopItem : shopItems) {
            shopItem.imageButton.button.setColor(shopItem.costLabel.label.getIntValue() > Progress.money || shopItem.costLabel.label.getIntValue() == 0
                    ? Color.WHITE
                    : Cage.BUTTONS_COLOR);
        }
    }

    public void showSettingsWindow() {
        settingsWindow.show();
        settingsScrollPane.scrollTo(
                0,
                settingsScrollPane.getMaxY(),
                settingsScrollPane.getWidth(),
                settingsScrollPane.getHeight()
        );
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
        if (stageSlider != null) {
            stageSlider.setState(progress.getState(true));
        }
        if (Main.musicManager != null) {
            Main.musicManager.playMusic(MusicManager.Melody.menu);
        }
    }

    @Override
    public void render(float delta) {
        main.readyWhite();
        stage.act();
        stage.draw();
        if (Main.loading && Main.assetManager.update()) {
            afterLoadingCallable.call();
            Main.loading = false;
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {
        Main.musicManager.playMusic(MusicManager.Melody.menu);
    }

    @Override
    public void hide() {
        Main.musicManager.stopMusic();
    }

    @Override
    public void dispose() {

    }
}
