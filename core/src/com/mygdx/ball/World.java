package com.mygdx.ball;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Align;
import com.mygdx.ball.Windows.AWindow;
import com.mygdx.ball.Windows.TextScrollPane;
import com.mygdx.ball.actor_classes.AImageButton;
import com.mygdx.ball.actor_classes.AImageTextButton;
import com.mygdx.ball.actor_classes.LevelResults;
import com.mygdx.ball.actor_classes.TextImageButtonGroup;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import static com.mygdx.ball.Cage.BUTTONS_COLOR;
import static com.mygdx.ball.Cage.DEFAULT_COLOR;
import static com.mygdx.ball.Main.helpWindow;
import static com.mygdx.ball.Main.manual;

class World {
    AWindow exitConfirmation;

    AImageTextButton exitButton;
    AImageTextButton helpButton;
    AImageTextButton skipButton;
    AImageTextButton centerButton;

    Cage[][] world;
    CopyOnWriteArrayList<Ball> balls;
    int ballsLeft;
    ButtonGroup buttonGroup;
    TextImageButtonGroup[] actions;
    Group actionsGroup;
    AImageTextButton ballsLeftLabel;
    Group worldGroup;
    AWindow resultsWindow;
    LevelResults levelResults;
    TextScrollPane resultScrollPane;
    AImageTextButton timeLabel;

    Cage[] exitCages;
    CopyOnWriteArrayList<Cage> portalCages;

    Cage chosenCage;

    Ball chosenBall;

    Image chosenCageImage;
    TextImageButtonGroup[] bonuses;

    private Main main;
    Progress progress;

    private int step;
    private int stepTwo;

    int sumStep;

    int bonusesUp;
    int rewardUp;

    PlayerAI playerAI;

    public static SpriteDrawable[] cageAnimDrawables;

    void programmeBall(Ball ball) {
        ball.addListener(new MyClickListener(() -> {
            chosenCage = world[ball.x][ball.y];
            chosenBall = ball;
            focusBall();
            setChosenCageImage();
        }));
        sumStep += ball.getStep();

        // HERE
        ball.createBallStrategy(this, progress.getSizeX(), progress.getSizeY());
    }

    World(final Main main, final Progress progress) {

        this.main = main;
        this.progress = progress;
        worldGroup = new Group();
        world = new Cage[progress.getSizeX()][];

        if (Progress.ballGame) {
            playerAI = new PlayerAI(this);
            portalCages = new CopyOnWriteArrayList<>();
        }

        bonusesUp = 0;
        rewardUp = 0;

        for (int i = 0; i < progress.getSizeX(); ++i) {
            world[i] = new Cage[progress.getSizeY()];
            for (int j = 0; j < progress.getSizeY(); ++j) {
                int finalI = i;
                int finalJ = j;
                world[i][j] = new Cage(
                        null, null, null,
                        new ClickListener() {

                            @Override
                            public void clicked(InputEvent event, float x, float y) {
                                Main.musicManager.playSound(MusicManager.SoundTrack.cageHit);

                                if (helpWindow.isShowed()) {
                                    System.out.println("Ложна тревога");
                                    return;
                                }

                                if (!Progress.ballGame) {

                                    if (world[finalI][finalJ].isBomb() && chosenCage == world[finalI][finalJ]) {
                                        hitCage(finalI, finalJ);
                                        return;
                                    }

                                    if (cannotClickButton(finalI, finalJ) || !canUseItem()) {
                                        return;
                                    }

                                    if (chosenCage != world[finalI][finalJ]) {
                                        chosenCage = world[finalI][finalJ];
                                        setChosenCageImage();
                                        return;
                                    }

                                    ++step;

                                    activateUsedItem(finalI, finalJ);


                                } else {

                                    if (chosenBall == null) {
                                        return;
                                    }

                                    chosenBall.setActions(finalI, finalJ);

                                    // HERE
                                    if (true) {
                                        return;
                                    }

                                    if (chosenBall == null
                                            || !chosenBall.hasEnoughStep()
                                            || getDist(finalI, finalJ, chosenBall.x, chosenBall.y) > 1) {
                                        return;
                                    }

                                    if (chosenBall.makeStep(
                                            World.this, new Ball.Direction(
                                            finalI - chosenBall.x,
                                            finalJ - chosenBall.y
                                            )
                                    )) {
                                        if (chosenBall != null) {
                                            focusBall();
                                            ++step;
                                        }
                                    }
                                    if (chosenBall != null) {
                                        chosenCage = world[chosenBall.x][chosenBall.y];
                                    }
                                    setChosenCageImage();

                                    System.out.println("step: " + step + " / " + sumStep);
                                    if (step == sumStep) {
                                        makeBallGameStep();

                                    }
                                }

                            }

                            private boolean cannotClickButton(int finalI, int finalJ) {
                                return world[finalI][finalJ].cageType == Cage.CageType.exit || world[finalI][finalJ].button.isChecked();
                            }

                        },
                        i * (progress.getCageSize() + progress.getSpaceSize()),
                        j * (progress.getCageSize() + progress.getSpaceSize()),
                        progress.getCageSize(), progress.getCageSize(),
                        "", "", 55, Color.BLACK, Align.center,
                        progress.getCageType(finalI, finalJ), finalI, finalJ
                ) {
                    @Override
                    public void act(float delta) {
                        super.act(delta);
                        if (button.isChecked()) {
                            if (checkStep == 0) {
                                checkStep = Progress.ballGame ? stepTwo : step;
                            } else if ((Progress.ballGame ? stepTwo : step) - checkStep == uncheckCount) {
                                rotateCageBack(xIndex, yIndex, () -> {
                                    if (usedItem == UsedItem.bomb) {
                                        usedItem = UsedItem.none;
                                        setImage(null);
                                    }
                                    if (cageType == CageType.magic) {
                                        setMagic();
                                    }
                                });
                            }
                        } else {
                            checkStep = 0;
                        }
                    }
                };

                if (progress.getLevel() == Progress.LEVEL_IN_STAGE_COUNT) {
                    int pixel = Progress.imageMap.getPixel(i, j);
                    world[i][j].button.setColor(new Color(pixel));

                } else {
                    world[i][j].button.setColor(DEFAULT_COLOR);
                }

                switch (world[i][j].cageType) {

                    case simple:
                        break;
                    case wall:
                        world[i][j].setWall();
                        break;
                    case exit:
                        break;
                    case magic:
                        world[i][j].setMagic();
                        portalCages.add(world[i][j]);
                        break;
                    case bonus:
                        break;
                    case fire:
                        world[i][j].setFire();
                        break;
                }
                world[i][j].setUncheckCount(progress.getUncheckCount(world[finalI][finalJ]));
                worldGroup.addActor(world[i][j]);
            }
        }

        chosenCageImage = new Image(Main.assetManager.get(Main.CHOSEN_PATH, Texture.class));
        chosenCageImage.setVisible(false);
        chosenCageImage.setTouchable(Touchable.disabled);
        worldGroup.addActor(chosenCageImage);

        if (Progress.wallEnabled /*&& Progress.LEVEL_IN_STAGE_COUNT != progress.getLevel()*/) {
            for (int xCounter = 0; xCounter < world.length; xCounter = xCounter + 8) {
                for (int yCounter = 0; yCounter < world[0].length; yCounter = yCounter + 8) {
                    int mazeIndex = Math.min((int) Math.round(Math.random() * progress.mazes.length), progress.mazes.length - 1);
                    for (int i = 0; i < 8; ++i) {
                        if (xCounter + i >= world.length) {
                            continue;
                        }
                        for (int j = 0; j < 8; ++j) {
                            if (yCounter + j >= world[0].length) {
                                continue;
                            }
                            if (progress.mazes[mazeIndex][i][j] == '1' && Math.random() > 0.25
                                    && world[xCounter + i][yCounter + j].cageType.equals(Cage.CageType.simple)) {
                                world[xCounter + i][yCounter + j].setWall();
                            }
                        }
                    }
                }
            }
        }

        if (Progress.ballGame) {
            ballInformation = new ConcurrentHashMap<>();
        } else {
            if (Math.random() > 0.5) {
                int rx = progress.getRandomX(), ry = progress.getRandomY();
                world[rx][ry].cageType = Cage.CageType.bonus;
            }

            if (Math.random() > 0.8) {
                int rx = progress.getRandomX(), ry = progress.getRandomY();
                world[rx][ry].cageType = Cage.CageType.bonus;
            }

            if (Math.random() > 0.9) {
                int rx = progress.getRandomX(), ry = progress.getRandomY();
                world[rx][ry].cageType = Cage.CageType.bonus;
            }

            if (Math.random() > 0.95) {
                int rx = progress.getRandomX(), ry = progress.getRandomY();
                world[rx][ry].cageType = Cage.CageType.bonus;
            }
        }

        step = Progress.ballGame ? 0 : 1;
        stepTwo = 0;
    }

    private void checkBonuses(int finalI, int finalJ) {

        if (world[finalI][finalJ].cageType == Cage.CageType.bonus) {
            world[finalI][finalJ].cageType = Cage.CageType.simple;
            int randomBonusIndex = world[finalI][finalJ].bonus == -1
                    ? (int) Math.round(Math.random() * Progress.FREE_ITEM_INDEX) % Progress.FREE_ITEM_INDEX
                    : world[finalI][finalJ].bonus;
            System.out.println("Random bonus: " + randomBonusIndex);

            Image bonusImage = new Image(Main.bonuses[randomBonusIndex]);
            bonusImage.setBounds(world[finalI][finalJ].getX(), world[finalI][finalJ].getY(),
                    world[finalI][finalJ].getWidth(), world[finalI][finalJ].getHeight());
            worldGroup.addActor(bonusImage);
            bonusImage.addAction(Actions.sequence(
                    Actions.moveBy(0, 100, 1),
                    Actions.parallel(Actions.moveBy(0, 50, 0.5f), Actions.alpha(0, 0.5f)),
                    Actions.run(bonusImage::remove)
            ));
//            world[finalI][finalJ].setImage();

            int tmp = progress.itemsMap.get(randomBonusIndex);
            if (progress.getLevel() > 0) {
                progress.itemsMap.put(randomBonusIndex, ++tmp);
                bonuses[randomBonusIndex].label.setText(progress.getBonusCount(randomBonusIndex) + " ");
            }
        }
    }

    void rotateCageBack(int x, int y, Callable after) {
        world[x][y].button.setChecked(false);
        Drawable tempBackground = world[x][y].button.getUpDrawable();
        Drawable tempImage = world[x][y].imageUp;
        String tempText = world[x][y].label.getValue();
        world[x][y].label.setText("");
        world[x][y].setImage(null);
        Color newColor = new Color(world[x][y].button.getColor());
        AtomicInteger currentFrame = new AtomicInteger();
        currentFrame.set(cageAnimDrawables.length - 1);

        world[x][y].button.addAction(Actions.sequence(

                Actions.repeat(cageAnimDrawables.length, Actions.sequence(
                        Actions.run(() -> {
//                            System.out.println(currentFrame.get());
                            world[x][y].button.setUpDrawable(cageAnimDrawables[currentFrame.getAndDecrement()]);
                        }),
                        Actions.delay(0.02226f)
                )),

                Actions.run(() -> {
                    world[x][y].button.setImage(tempImage);
                    world[x][y].button.setUpDrawable(tempBackground);
                    world[x][y].label.setText(tempText);
                    world[x][y].button.setColor(newColor);

                    after.call();

                })
        ));
    }

    private void rotateCage(int finalI, int finalJ, Callable after) {
        rotateCage(finalI, finalJ, new Color(world[finalI][finalJ].button.getColor()), after);
    }

    private void rotateCage(int finalI, int finalJ, Color tempColor, Callable after) {
        world[finalI][finalJ].button.setChecked(true);
        Drawable tempBackground = world[finalI][finalJ].button.getCheckedDrawable();
        Drawable tempImage = world[finalI][finalJ].imageUp;
        String tempText = world[finalI][finalJ].label.getValue();
        world[finalI][finalJ].label.setText("");
        world[finalI][finalJ].setImage(null);
        Color newColor = new Color(world[finalI][finalJ].button.getColor());
        world[finalI][finalJ].button.setColor(tempColor);

        AtomicInteger currentFrame = new AtomicInteger();

        world[finalI][finalJ].button.addAction(Actions.sequence(

                Actions.repeat(cageAnimDrawables.length, Actions.sequence(
                        Actions.run(() -> world[finalI][finalJ].button.setCheckedDrawable(cageAnimDrawables[currentFrame.getAndIncrement()])),
                        Actions.delay(0.02226f)
                )),

                Actions.run(() -> {
                    world[finalI][finalJ].button.setImage(tempImage);
                    world[finalI][finalJ].button.setCheckedDrawable(tempBackground);
                    world[finalI][finalJ].label.setText(tempText);
                    world[finalI][finalJ].button.setColor(newColor);

                    after.call();

                })
        ));
    }

    private void checkExits() {
        if (exitCages != null) {
            System.out.println("Exit cages not null!");
            return;
        }

        if (!Progress.ballGame && progress.getUncheckCount(world[0][0]) == step - 1
                || Progress.ballGame && stepTwo == progress.getUncheckCount(world[0][0])) {
            /*spawn exit*/
            final int exitCount = (int) Math.round(
                    Math.max(
                            1,
                            Math.min(
                                    Math.random() * (progress.getLevel() + progress.getState(false)),
                                    5
                            )
                    )
            );
            exitCages = new Cage[exitCount];
            for (int i = 0; i < exitCount; ++i) {
                int[][] dist = new int[world.length][];
                int maxA = 0, maxB = 0;
                for (int a = 0; a < world.length; ++a) {
                    dist[a] = new int[world[0].length];
                    for (int b = 0; b < world[0].length; ++b) {
                        NearestBall tmp = getNearestBall(a, b, exitCages);
                        if (tmp == null) {
                            System.out.println("No nearest ball");
                            return;
                        }
                        dist[a][b] = tmp.dist;
                        if (dist[a][b] > dist[maxA][maxB]) {
                            maxA = a;
                            maxB = b;
                        }
                    }
                }
                exitCages[i] = world[maxA][maxB];
                System.out.println("New exit: " + maxA + " " + maxB);
                world[maxA][maxB].setExit();
            }
        }
    }

    private class WorldGameStep {
        Cage cage;
        int maxRadius;
        int minRadius;
        int offset;
        String direction;

        WorldGameStep(Cage cage, int radius, String direction) {
            this.cage = cage;
            this.maxRadius = radius;
            this.minRadius = radius;
            this.direction = direction;
            this.offset = 0;
        }

//        boolean actualData() {
//            return (radius < Math.max(progress.getSizeX(), progress.getSizeY()))
//                    && !(direction != null && cage == null);
//        }
    }

    // ключ - мяч, значение - информация про него
    private ConcurrentHashMap<Ball, CopyOnWriteArrayList<WorldGameStep>> ballInformation;

//    private Cage getRandomCage() {
//
//        // TODO: реализовать выборку не по количеству информаторов, а по количеству допустимых клеток
//
//        CopyOnWriteArrayList<WorldGameStep> historyInfo = null;
//        for (Ball ball: balls) {
//            CopyOnWriteArrayList<WorldGameStep> tmp = ballInformation.get(ball);
//            if (tmp == null)
//                continue;
//            if (historyInfo == null || historyInfo.size() < tmp.size()) {
//                historyInfo = tmp;
//            }
//        }
//
//        boolean hasActive = false;
//        for (Cage[] cages : world) {
//            for (Cage cage : cages) {
//                cage.isActive = historyInfo == null || historyInfo.isEmpty()
//                        || !isUselessCage(cage, historyInfo);
//                if (cage.isActive) {
////                    cage.button.setColor(Color.GREEN);
//                    hasActive = true;
//                } else {
////                    cage.button.setColor(Color.WHITE);
//                }
//            }
//        }
//
//        if (!hasActive) {
//            return null;
//        }
//        int xGoal;
//        int yGoal;
//        try {
//            do {
//                xGoal = progress.getRandomX();
//                yGoal = progress.getRandomY();
//            } while (!world[xGoal][yGoal].isActive);
//            return world[xGoal][yGoal];
//        } catch (Exception e) {
//            System.out.println("Blyat =(\n" + e.toString());
//        }
//        return null;
//    }

//    private boolean isUselessCage(Cage cage, CopyOnWriteArrayList<WorldGameStep> historyInfo) {
//
//        for (WorldGameStep gameStep : historyInfo) {
//            if (gameStep.cage == null) {
//                continue;
//            }
//
//            boolean correctLeft = gameStep.cage.directionString.equals("left") && cage.xIndex < gameStep.cage.xIndex + gameStep.offset;
//            boolean correctRight = gameStep.cage.directionString.equals("right") && cage.xIndex > gameStep.cage.xIndex - gameStep.offset;
//            boolean correctUp = gameStep.cage.directionString.equals("up") && cage.yIndex > gameStep.cage.yIndex - gameStep.offset;
//            boolean correctDown = gameStep.cage.directionString.equals("down") && cage.yIndex < gameStep.cage.yIndex + gameStep.offset;
//
//            int dist = getDist(gameStep.cage.xIndex, gameStep.cage.yIndex, cage.xIndex, cage.yIndex);
//            boolean correctDist = gameStep.minRadius != -1
//                    && (dist <= gameStep.maxRadius && dist >= gameStep.minRadius);
//            if (
//                    cage.button.isChecked() ||
//                    !(correctLeft || correctRight || correctDown || correctUp || correctDist)
//            ) {
//                return true;
//            }
//        }
//        return false;
//    }

    void makeBallGameStep() {
//        updateInfo();

//        Cage randomCage = getRandomCage();
//        if (randomCage != null) {

//            int usedItem = (int) Math.floor(Math.random() * 3) % 3;
//            if (usedItem == 0 && Progress.getPower(0) == 0) {
//                usedItem = 1;
//            }

//            activateUsedItem(usedItem, randomCage.xIndex, randomCage.yIndex);
//            addBallInfo(usedItem, randomCage);

//            if (randomCage.usedItem != Cage.UsedItem.bomb) {
//                hitCage(randomCage.xIndex, randomCage.yIndex);
//            }
//        }

        playerAI.makeStep();

        chosenCageImage.setVisible(false);
        chosenCage = null;
        chosenBall = null;

        focusBall();
        step = 0;
        checkExits();
        stepTwo++;
        sumStep = 0;
        for (Ball ball: balls) {
            if (ball.isAlive()){
                ball.nextRound();
                sumStep += ball.stepRemains;
            }
        }
    }

//    private void addBallInfo(int usedItem, Cage stepCage) {
//        if (usedItem != 0) {
//            NearestBall nearestBall = getNearestBall(stepCage.xIndex, stepCage.yIndex);
//            if (nearestBall != null && nearestBall.ball != null) {
//                CopyOnWriteArrayList<WorldGameStep> tmp = ballInformation.get(nearestBall.ball);
//                if (tmp == null) {
//                    tmp = new CopyOnWriteArrayList<>();
//                }
//                tmp.add(new WorldGameStep(
//                        stepCage,
//                        stepCage.usedItem == Cage.UsedItem.distance
//                                ? stepCage.label.getIntValue()
//                                : -1,
//                        stepCage.directionString
//                ));
//                ballInformation.put(nearestBall.ball, tmp);
//            }
//        }
//    }
//
//    void addBallDistInfo(Cage stepCage) {
//        System.out.println("added ball dist info");
//        NearestBall nearestBall = getNearestBall(stepCage.xIndex, stepCage.yIndex);
//        if (nearestBall != null && nearestBall.ball != null) {
//            CopyOnWriteArrayList<WorldGameStep> tmp = ballInformation.get(nearestBall.ball);
//            if (tmp == null) {
//                tmp = new CopyOnWriteArrayList<>();
//            }
//            tmp.add(new WorldGameStep(
//                    stepCage, nearestBall.dist, null
//            ));
//            ballInformation.put(nearestBall.ball, tmp);
//        }
//    }

//    private void updateInfo() {
//        System.out.println("Updating info: ");
//        for (Ball ball : balls) {
//            CopyOnWriteArrayList<WorldGameStep> list = ballInformation.get(ball);
//            if (list == null)
//                continue;
//            System.out.println("-------");
//            for (WorldGameStep step : list) {
//                System.out.println("Before: minR=" + step.minRadius + "; maxR=" + step.maxRadius + "; offset=" + step.offset);
//                int ballOffset = (ball.getStep() - ball.stepRemains);
//                System.out.println("Ball moved with " + ballOffset + " steps");
//                if (step.minRadius != -1) {
//                    step.minRadius = Math.max(0, step.minRadius - ballOffset);
//                    step.maxRadius += ballOffset;
//                    if (step.maxRadius >= progress.getSizeX() + progress.getSizeY()) {
//                        System.out.println("Info removed because of radius is: " + step.maxRadius);
//                        list.remove(step);
//                        continue;
//                    }
//                } else if (!step.direction.equals("")) {
//                    step.offset += ballOffset;
//                    if (step.direction.equals("left") && step.cage.xIndex + step.offset >= world.length
//                            || step.direction.equals("right") && step.cage.xIndex - step.offset < 0
//                            || step.direction.equals("down") && step.cage.yIndex + step.offset >= world[0].length
//                            || step.direction.equals("up") && step.cage.yIndex - step.offset < 0) {
//                        System.out.println("Info removed because of offset is: " + step.offset);
//                        list.remove(step);
//                    }
//                }
//                System.out.println("after: minR=" + step.minRadius + "; maxR=" + step.maxRadius + "; offset=" + step.offset);
//                System.out.println("-------");
//            }
//            System.out.println("=======");
//        }
//    }

    private void setChosenCageImage() {
        if (chosenCage == null)
            return;

        chosenCageImage.setVisible(true);
        chosenCageImage.setBounds(
                chosenCage.getX(),
                chosenCage.getY(),
                chosenCage.getWidth(),
                chosenCage.getHeight()
        );
    }

    void useBonus(int index) {
        if (chosenCage == null && progress.bonusNeedCage(index) || !progress.canUseBonus(index)) {
            System.out.println(1);
            return;
        }

        if (progress.getLevel() > 0 && progress.itemsMap.get(index) == 0) {
            /*shop.show()*/
            return;
        }

        if (progress.getLevel() > 0) {
            int tmp = progress.itemsMap.get(index);
            progress.itemsMap.put(index, --tmp);
            bonuses[index].label.setText(progress.getBonusCount(index) + " ");
        }

        switch (index) {
            case 0: {
                // снежинка - замедляет все мячи в два раза
                for (Ball ball : balls) {
                    if (ball.isAlive()) {
                        world[ball.x][ball.y].button.setColor(Color.SKY);
                        ball.setStep(Math.max(1, ball.getStep() / 2));
                    }
                }
                break;
            }
            case 1: {
                // взрыв - устраивает взрыв с силой от 1 до 4
                hitCross(chosenCage.xIndex, chosenCage.yIndex, (int) Math.round(Math.random() * 3) + 1);
                moveBalls();
                break;
            }
            case 2: {
                // взрывает все стены крестом
//                boolean exitEnabled = Progress.exitEnabled;
                for (int i = 0; i < world.length; ++i) {
                    for (int j = 0; j < world[0].length; ++j) {
                        if (world[i][j].cageType == Cage.CageType.wall) {
//                            if (exitEnabled) {
//                                hitCage(i, j);
//                            } else {
//                                hitCross(i, j);
//                            }
                            hitCross(i, j);
                        }
                    }
                }
                moveBalls();
                break;
            }
            case 3: {
                // подсвечивает все мячи их цветами
                for (Ball ball : balls) {
                    if (ball.isAlive())
                        world[ball.x][ball.y].button.setColor(ball.getColor());
                }
                break;
            }
            case 4: {
                // предметы+
                for (int i1 = 0; i1 < progress.actions.length; i1++) {
                    progress.actions[i1].remains += 10;
                    actions[i1].label.setText(progress.actions[i1].remains);
                    actions[i1].button.setColor(BUTTONS_COLOR);
                }
                break;
            }
            default: {
                chosenCage.setBomb();
            }
        }
        if (progress.getLevel() > 0) {
            progress.saveProgress();
        }
    }

    public void ballEscaped(Ball ball) {
        if (ball.bonus != -1) {
            Progress.money += 300;
            progress.buyBonus(ball.bonus);
            bonusesUp++;
        }
        int reward = Math.min(ball.colorIndex * 5 + 5, 100);
        Progress.money += reward;
        rewardUp += reward;
        --ballsLeft;
        if (ballsLeft == 0) {
            winGame();
        }

//        sumStep -= ball.getStep();
        step += (ball.stepRemains);
        ballInformation.remove(ball);
        balls.remove(ball);
        ball.clear();
        ball.remove();
        chosenBall = null;
        focusBall();
        ballsLeftLabel.setText("Balls: " + ballsLeft, "Мячей: " + ballsLeft);
    }

    public void clearAction(int index) {
        actions[index].button.setDisabled(true);
        actions[index].button.setChecked(false);
        actions[index].setImage(null);
        actions[index].label.setText("");
    }

    public void focusBall() {
        if (chosenBall == null) {
            for (int i = 0; i < 3; ++i) {
                clearAction(i);
            }
            actions[1].setImage(new Sprite(Main.assetManager.get(Main.ARROW_PATH_RIGHT, Texture.class)),
                    Color.RED);
            actions[2].label.setText("ball ", "мяч ");
            actions[1].label.setText("skip ", "пропуск ");
            actions[0].label.setText("bonus ", "бонус ");
        } else {
            actions[2].label.setText("");
            actions[2].setImage(new Sprite(Main.ballTexture), Progress.colors[chosenBall.colorIndex]);
            actions[1].setImage(new Sprite(Main.assetManager.get(Main.ARROW_PATH_RIGHT, Texture.class)),
                    chosenBall.hasEnoughStep() ? Color.WHITE : Color.RED);
            String stepsString = chosenBall.stepRemains + " / " + chosenBall.getStep() + " ";
            actions[1].label.setText("steps: " + stepsString, "ходы: " + stepsString);
            if (chosenBall.bonus == -1) {
                actions[0].setImage(null);
            } else {
                actions[0].setImage(Progress.getBonusDrawable(chosenBall.bonus));
            }
        }
    }

    public class NearestBall {
        int dist;
        Ball ball;

        NearestBall(Ball ball, int dist) {
            this.ball = ball;
            this.dist = dist;
        }
    }

    public NearestBall getNearestBall(int a, int b, Cage... avoidCages) {
        if (balls.isEmpty()) {
            return null;
        }
        int minDist = getDist(balls.get(0), a, b);
        Ball minDistBall = balls.get(0);
        for (Ball ball: balls) {
            int dist = getDist(ball, a, b);
            if (dist < minDist) {
                minDistBall = ball;
                minDist = dist;
            }
        }
        if (avoidCages != null) {
            for (Cage cage : avoidCages) {
                if (cage != null) {
                    int dist = getDist(cage.xIndex, cage.yIndex, a, b);
                    if (dist < minDist) {
                        minDistBall = null;
                        minDist = dist;
                    }
                }
            }
        }
        return new NearestBall(minDistBall, minDist);
    }

    private int getDist(Ball ball, int xx, int yy) {
        if (!Progress.ballGame && ball.isVisible() || Progress.ballGame && !ball.isVisible()) {
            return 1_000_000_000;
        }
        return getDist(ball.x, ball.y, xx, yy);
    }

    int getDist(int x, int y, int xx, int yy) {
        int distX = Math.abs(x - xx);
        int distY = Math.abs(y - yy);
        return distX + distY;
    }

    private void watchBalls(int finalI, int finalJ) {
        NearestBall nearestBall = getNearestBall(finalI, finalJ);
        if (nearestBall == null) {
            return;
        }

        if (nearestBall.ball.getColor().equals(Color.BLACK)
                && (nearestBall.dist < nearestBall.ball.getStep())) {
            world[finalI][finalJ].button.setColor(Color.BLACK);
            return;
        }

        world[finalI][finalJ].usedItem = Cage.UsedItem.distance;
        world[finalI][finalJ].label.setText("" + nearestBall.dist);

        if (Progress.getPower(2) >= 1 && !Progress.ballGame) {
            world[finalI][finalJ].label.setText(world[finalI][finalJ].label.getText() + "±" + nearestBall.ball.getStep());
        }
    }

    private boolean canUseItem() {
        return progress.actions[buttonGroup.getCheckedIndex()].enabled();
    }

    private void moveBalls() {
        for (Ball ball: balls) {
            if (!ball.isVisible()) {
                ball.moveBall(World.this);
            }
        }
    }

    AImageButton arrRight;

    void gameOver(String enReason, String ruReason) {
        Main.musicManager.playMusic(MusicManager.Melody.gameOver);
        resultsWindow.setTitle("Game over", "Игра окончена");
        arrRight.setVisible(false);
        resultsWindow.setSubtitle(enReason,
                ruReason);

        levelResults.setData(0,
                progress.actions[0].getRemainsPercent(),
                progress.actions[1].getRemainsPercent(),
                progress.actions[2].getRemainsPercent());

        resultsWindow.show();
    }

    private void activateUsedItem(final int finalI, final int finalJ) {

        Color tempColor = new Color(world[finalI][finalJ].button.getColor());

        if (!Main.infinityItems) {
            --progress.actions[buttonGroup.getCheckedIndex()].remains;
        }

        actions[buttonGroup.getCheckedIndex()].label.setText(
                progress.actions[buttonGroup.getCheckedIndex()].remains + " "
        );

        actions[buttonGroup.getCheckedIndex()].setImage(
                progress.actions[buttonGroup.getCheckedIndex()].getItemDrawable()
        );

        Main.vibrate(50);
        int itemPower = Progress.getPower(buttonGroup.getCheckedIndex());
        switch (buttonGroup.getCheckedIndex()) {
            case 0: {
                if (itemPower == 1) {
                    hitCross(finalI, finalJ);
                } else if (itemPower == 2) {
//                    hitCage(finalI, finalJ);
                    world[finalI][finalJ].setBomb();
                    setBombsCross(finalI, finalJ);
                }
                break;
            }
            case 1: {
                directBalls(finalI, finalJ);
                break;
            }
            case 2: {
                watchBalls(finalI, finalJ);
                break;
            }
            default: {

            }
        }

        boolean updatedItem1 = Progress.getPower(1) > 1;
        boolean updatedItem2 = Progress.getPower(2) > 1;

        for (int i = 0; i < world.length; i++) {
            for (int j = 0; j < world[0].length; j++) {

                if (updatedItem1 && world[i][j].usedItem == Cage.UsedItem.direction) {
                    directBalls(i, j);
                }

                if (updatedItem2 && world[i][j].usedItem == Cage.UsedItem.distance) {
                    watchBalls(i, j);
                }

                if (world[i][j].cageType.equals(Cage.CageType.fire) && Math.random() > 0.98) {
                    fireCross(i, j);
                }
            }
        }

        if (!progress.actions[buttonGroup.getCheckedIndex()].enabled()) {
            actions[buttonGroup.getCheckedIndex()].button.setColor(Color.RED);
        }

        if (world[finalI][finalJ].usedItem != Cage.UsedItem.bomb) {
            rotateCage(finalI, finalJ, tempColor, () -> {

                hitCage(finalI, finalJ);

                if (ballsLeft != 0 && progress.noActions()) {
                    gameOver("No more items", "закончились предметы");
                }
                moveBalls();
                checkExits();
            });
        } else {
            if (ballsLeft != 0 && progress.noActions()) {
                gameOver("No more items", "закончились предметы");
            }
            moveBalls();
            checkExits();
        }
    }

    void botActivateUsedItem(final int usedItem, final int finalI, final int finalJ) {
        Main.vibrate(50);
        int itemPower = Progress.getPower(usedItem);
        switch (usedItem) {
            case 0: {
                if (itemPower == 1) {
                    hitCross(finalI, finalJ);
                } else if (itemPower == 2) {
                    world[finalI][finalJ].setBomb();
                    setBombsCross(finalI, finalJ);
                }
                break;
            }
            case 1: {
                directBalls(finalI, finalJ);
                break;
            }
            case 2: {
                watchBalls(finalI, finalJ);
                break;
            }
            default: {

            }
        }

        if (Progress.getPower(2) > 1) {
            for (int i = 0; i < world.length; i++) {
                for (int j = 0; j < world[0].length; j++) {
                    if (world[i][j].usedItem == Cage.UsedItem.distance) {
                        watchBalls(i, j);
                    }
                }
            }
        }

        if (Progress.getPower(1) > 1) {
            for (int i = 0; i < world.length; i++) {
                for (int j = 0; j < world[0].length; j++) {
                    if (world[i][j].usedItem == Cage.UsedItem.direction) {
                        directBalls(i, j);
                    }
                }
            }
        }
    }

    private void setBombsCross(int x, int y) {
        world[x][y].isTrap = true;
        if (x - 1 >= 0) {
            world[x - 1][y].isTrap = true;
        }
        if (x + 1 < world.length) {
            world[x + 1][y].isTrap = true;
        }
        if (y - 1 >= 0) {
            world[x][y - 1].isTrap = true;
        }
        if (y + 1 < world[0].length) {
            world[x][y + 1].isTrap = true;
        }
    }

    private void directBalls(int finalI, int finalJ) {
        if (balls.isEmpty()) {
            return;
        }
        Ball minDistBall = balls.get(0);
        int minDistX = Math.abs(minDistBall.x - finalI);
        int minDistY = Math.abs(minDistBall.y - finalJ);
        int minDist = getDist(minDistBall, finalI, finalJ);

        for (Ball ball: balls) {
            if (ball.isAlive()) {
                int distX = Math.abs(ball.x - finalI);
                int distY = Math.abs(ball.y - finalJ);
                int dist = getDist(ball, finalI, finalJ);
                if (dist < minDist) {
                    minDistBall = ball;
                    minDist = dist;
                    minDistX = distX;
                    minDistY = distY;
                }
            }
        }

        if (minDistBall.getColor().equals(Color.BLACK)
                && (minDistX < minDistBall.getStep() || minDistY < minDistBall.getStep())) {
            world[finalI][finalJ].button.setColor(Color.BLACK);
            return;
        }

        if ((minDistY > minDistX || minDistX == 0) && minDistY != 0) {
            // урезаем Y
            if (minDistBall.y < finalJ) {
                // мяч где-то ниже клетки
                world[finalI][finalJ].setArrow("down");
            } else {
                // мяч где-то выше клетки
                world[finalI][finalJ].setArrow("up");
            }
        } else {
            // урезаем X
            if (minDistBall.x < finalI) {
                // мяч где-то левее клетки
                world[finalI][finalJ].setArrow("left");
            } else {
                // мяч где-то правее клетки
                world[finalI][finalJ].setArrow("right");
            }
        }

        if (Progress.getPower(1) >= 1) {
            world[finalI][finalJ].button.setColor(minDistBall.getColor());
        }
    }

    void hitCross(int x, int y) {
        hitCross(x, y, 0);
    }

    void fireCross(int x, int y) {
        if (x - 1 >= 0) {
            world[x - 1][y].setFire();
        }
        if (x + 1 < world.length) {
            world[x + 1][y].setFire();
        }
        if (y - 1 >= 0) {
            world[x][y - 1].setFire();
        }
        if (y + 1 < world[0].length) {
            world[x][y + 1].setFire();
        }
    }

    private void hitCross(int x, int y, int count) {
        if (count < 0) {
            return;
        }

        if (x - 1 >= 0) {
            if (!world[x - 1][y].button.isChecked()) {
                rotateCage(x - 1, y, () -> {
                    hitCage(x - 1, y);
                    world[x - 1][y].button.setColor(new Color(1f, Math.max(0, 0.75f - 0.25f * count), 0f, 1f));
                });
            }
            hitCross(x - 1, y, count - 1);
        }
        if (x + 1 < world.length) {
            if (!world[x + 1][y].button.isChecked()) {
                rotateCage(x + 1, y, () -> {
                    hitCage(x + 1, y);
                    world[x + 1][y].button.setColor(new Color(1f, Math.max(0, 0.75f - 0.25f * count), 0f, 1f));
                });
            }
            hitCross(x + 1, y, count - 1);
        }
        if (y - 1 >= 0) {
            if (!world[x][y - 1].button.isChecked()) {
                rotateCage(x, y - 1, () -> {
                    hitCage(x, y - 1);
                    world[x][y - 1].button.setColor(new Color(1f, Math.max(0, 0.75f - 0.25f * count), 0f, 1f));
                });
            }
            hitCross(x, y - 1, count - 1);
        }
        if (y + 1 < world[0].length) {
            if (!world[x][y + 1].button.isChecked()) {
                rotateCage(x, y + 1, () -> {
                    hitCage(x, y + 1);
                    world[x][y + 1].button.setColor(new Color(1f, Math.max(0, 0.75f - 0.25f * count), 0f, 1f));
                });
            }
            hitCross(x, y + 1, count - 1);
        }
    }

    void hitCage(int x, int y) {

        boolean isFire = world[x][y].cageType.equals(Cage.CageType.fire);
        if (isFire) {
            world[x][y].cageType = Cage.CageType.simple;
            world[x][y].setImage(null);
            world[x][y].label.setText("");
            world[x][y].button.setColor(DEFAULT_COLOR);
            return;
        }

        checkBonuses(x, y);
        world[x][y].button.setChecked(true);
        if (world[x][y].usedItem == Cage.UsedItem.bomb) {
            world[x][y].setImage(null);
            world[x][y].usedItem = Cage.UsedItem.none;
            hitCross(x, y);
        }
        for (Ball ball: balls) {
            if (x == ball.x && y == ball.y && ball.isAlive()) {
                ball.setVisible(!Progress.ballGame);
                Main.vibrate(150);
                Main.musicManager.playSound(MusicManager.SoundTrack.ballCaught);
//                manual.addItem(ball, new ADrawable(
//                        new Sprite(Main.ballTexture), 200, 200, Progress.colors[ball.colorIndex]
//                ), "", "");
                // image = null, так как на этом этапе не планирую добавлять новый элемент
                manual.addItem(ball, null, "", "");
                if ((ball.isVisible() && !Progress.ballGame || !ball.isVisible() && Progress.ballGame)
                        && --ballsLeft == 0) {
                    ballsLeftLabel.setText("Balls: " + ballsLeft, "Мячей: " + ballsLeft);
                    winGame();
                } else {
                    ballsLeftLabel.setText("Balls: " + ballsLeft, "Мячей: " + ballsLeft);
                }
                if (Progress.ballGame && !ball.isVisible()) {
                    System.out.println("Your ball found!");
                    sumStep -= ball.getStep();
                    ballInformation.remove(ball);
                }
            } else if (Progress.ballGame) {

            }
        }
        world[x][y].isTrap = false;
    }

    int starsCount;
    int reward;
    void winGame() {

        float results = (progress.actions[0].getRemainsPercent() + progress.actions[1].getRemainsPercent() + progress.actions[2].getRemainsPercent()) / 3f;
        starsCount = results > 0.5f ? 3 : results > 0.25f ? 2 : 1;
        System.out.println("RESULTS: " + results);
        System.out.println("STARS COUNT: " + starsCount);
        reward = progress.getLevel() > 0
                ? Math.max(0, (starsCount - Progress.getStarsCount(Progress.getLevelIndex(progress.getState(true), progress.getLevel() - 1))) * 5)
                : 0;
        if (progress.getLevel() == 5) {
            reward += progress.getStateReward(progress.getState(true));
        }

        if (Progress.ballGame) {
            reward += rewardUp;
            resultsWindow.setTitle(
                    "Bonus round is over!",
                    "Бонусный уровень завершён!"
            );

            resultsWindow.setSubtitle(
                    "+" + bonusesUp + " bonuses, +" + reward + " coins",
                    "+" + bonusesUp + " бонусов, +" + reward + " монет"
            );
        } else {
            resultsWindow.setTitle(progress.enGetProgress(false) + " complete!",
                    progress.ruGetProgress(false) + " пройден!");

            if (progress.getLevel() == 5) {
                resultsWindow.setSubtitle("Stage " + progress.getState(false) + " is over! +"
                                + reward + " coins",
                        "Стадия " + progress.getState(false) + " завершена! +" + reward + " монет");
            } else {
                resultsWindow.setSubtitle("+" + reward + " coins", "+" + reward + " монет");
            }

        }

        arrRight.setVisible(true);
        levelResults.setData(starsCount,
                progress.actions[0].getRemainsPercent(),
                progress.actions[1].getRemainsPercent(),
                progress.actions[2].getRemainsPercent());
        for (TextImageButtonGroup bonus : bonuses) {
            bonus.button.setTouchable(Touchable.disabled);
        }
        for (Cage[] cages : world) {
            for (int j = 0; j < world[0].length; ++j) {
                cages[j].button.setTouchable(Touchable.disabled);
                cages[j].button.addAction(
                        Actions.color(Progress.getRandomColor(), 2)
                );
            }
        }
        resultsWindow.show(2f, 0.1f);
    }

    boolean freeFromBalls(int cageX, int cageY) {
        for (Ball ball : balls) {
            if (ball.getCageX() == cageX && ball.getCageY() == cageY) {
                return false;
            }
        }
        return true;
    }

    void spawnCross(int x, int y) {
        if (x - 1 >= 0 && !world[x - 1][y].button.isChecked() && freeFromBalls(x - 1, y)) {
            ++ballsLeft;
            Ball tmp = progress.spawnBall(0, x - 1, y);
            if (Progress.ballGame) programmeBall(tmp);
            balls.add(tmp);
            worldGroup.addActor(tmp);
        }

        if (x + 1 < world.length && !world[x + 1][y].button.isChecked() && freeFromBalls(x + 1, y)) {
            ++ballsLeft;
            Ball tmp = progress.spawnBall(0, x + 1, y);
            if (Progress.ballGame) programmeBall(tmp);
            balls.add(tmp);
            worldGroup.addActor(tmp);
        }

        if (y - 1 >= 0 && !world[x][y - 1].button.isChecked() && freeFromBalls(x, y - 1)) {
            ++ballsLeft;
            Ball tmp = progress.spawnBall(0, x, y - 1);
            if (Progress.ballGame) programmeBall(tmp);
            balls.add(tmp);
            worldGroup.addActor(tmp);
        }

        if (y + 1 < world[0].length && !world[x][y + 1].button.isChecked() && freeFromBalls(x, y + 1)) {
            ++ballsLeft;
            Ball tmp = progress.spawnBall(0, x, y + 1);
            if (Progress.ballGame) programmeBall(tmp);
            balls.add(tmp);
            worldGroup.addActor(tmp);
        }

        ballsLeftLabel.setText("Balls: " + ballsLeft, "Мячей: " + ballsLeft);
    }

    void magicMoveBall(Ball ball, int x, int y, Cage prefPortal) {
        if (prefPortal == null) {
            CopyOnWriteArrayList<Cage> magicList = new CopyOnWriteArrayList<>();
            for (Cage portalCage : portalCages) {
                if (portalCage.xIndex != x && portalCage.yIndex != y
                        && !portalCage.isChecked() && freeFromBalls(portalCage.xIndex, portalCage.yIndex)) {
                    magicList.add(portalCage);
                }
            }

            if (magicList.isEmpty()) {
                return;
            }

            int newX, newY;

            int randomMagicCageIndex = (int) (Math.random() * magicList.size()) % magicList.size();
            newX = magicList.get(randomMagicCageIndex).xIndex;
            newY = magicList.get(randomMagicCageIndex).yIndex;
            ball.moveTo(newX, newY);
        } else {
            ball.moveTo(prefPortal.xIndex, prefPortal.yIndex);
        }
    }
}
