package com.mygdx.ball;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.mygdx.ball.actor_classes.TextImageButtonGroup;

public class Cage extends TextImageButtonGroup {
    public static final Color DEFAULT_COLOR = new Color(0.95f, 0.78f, 0.32f, 1);
    public static final Color BUTTONS_COLOR = new Color(1f, 0.85f, 0.5f, 1);
    private static final Color WALL_COLOR = new Color(0.72f, 0.45f, 0f, 1f);
    public static SpriteDrawable cageBorder;
    public static SpriteDrawable cageBorderReadyBall;

    private Image borderImage;


    public void setFire() {
        System.out.println("fire added");
        cageType = Cage.CageType.fire;
        setImage(Main.fireCageDrawable);
        label.setText("");

        int index = 11;
        setName(Main.isRussian ? Progress.ruCageNames[index] : Progress.enCageNames[index]);
        Main.manual.addItem(this, imageUp, "", "");
    }

    public void setExit() {
        System.out.println("exit added");
        cageType = Cage.CageType.exit;
        setImage(Main.exitDrawable);
        label.setText("");

        int index = 9;
        setName(Main.isRussian ? Progress.ruCageNames[index] : Progress.enCageNames[index]);
        Main.manual.addItem(this, imageUp, "", "");
    }

    public void setWall() {
        button.setColor(WALL_COLOR);
        cageType = CageType.wall;
        button.setChecked(true);
        setImage(Main.bricksDrawable);

        int index = 2;
        setName(Main.isRussian ? Progress.ruCageNames[index] : Progress.enCageNames[index]);
        Main.manual.addItem(this, imageUp, "", "");
    }

    public int bonus = -1;

    public void setBonus() {
        bonus = (int) Math.round(Math.random() * 5) % 5;
        cageType = CageType.bonus;
        setImage(Progress.getBonusDrawable(bonus));

        int index = 10;
        setName(Main.isRussian ? Progress.ruCageNames[index] : Progress.enCageNames[index]);
        Main.manual.addItem(this, Progress.getBonusDrawable(bonus), "", "");
    }

    public void setBonus(int bonusIndex, boolean ballGame) {
        bonus = bonusIndex;
        cageType = CageType.bonus;
        if (ballGame) {
            setImage(Progress.getBonusDrawable(bonus));
        }

        int index = 10;
        setName(Main.isRussian ? Progress.ruCageNames[index] : Progress.enCageNames[index]);
        Main.manual.addItem(this, Progress.getBonusDrawable(bonus), "", "");
    }

    public void clearBonus() {
        bonus = -1;
        setImage(null);
        cageType = Cage.CageType.simple;
        button.setColor(DEFAULT_COLOR);

        int index = button.isChecked() ? 1 : 0;
        setName(Main.isRussian ? Progress.ruCageNames[index] : Progress.enCageNames[index]);
        Main.manual.addItem(this, imageUp, "", "");
    }

    boolean isBomb() {
        return imageUp != null && imageUp.equals(Main.bombDrawable);
    }

    public enum CageType {
        simple,
        wall,
        exit,
        magic,
        bonus,
        fire,
        safe
    }

    public enum UsedItem {
        none,
        direction,
        distance,
        bomb
    }

    public CageType cageType;
    public int xIndex, yIndex;
    public UsedItem usedItem;

    public boolean isTrap;

    public boolean isActive;

    public Cage(Drawable imageUp, Drawable imageDown, Drawable imageChecked, InputListener listenerAction, float x, float y, float width, float height,
                String enText, String ruText, int textSize, Color color, int align,
                CageType cageType, int xIndex, int yIndex) {
        super(imageUp, imageDown, imageChecked, listenerAction, x, y, width, height, enText, ruText, textSize, color, align, "Cage");
        this.cageType = cageType;
        this.xIndex = xIndex;
        this.yIndex = yIndex;
        this.usedItem = UsedItem.none;
        isTrap = false;
        isActive = true;
        directionString = "";
        label.setTouchable(Touchable.disabled);
        borderImage = new Image(cageBorder);
        borderImage.setBounds(0, 0, width, height);
        borderImage.setTouchable(Touchable.disabled);
        addActor(borderImage);

        switch (cageType) {
            case simple: {
                break;
            }
            case wall: {
                break;
            }
            case exit: {
                break;
            }
            case magic: {
                setMagic();
                break;
            }
            case bonus: {
                break;
            }
        }
    }

    public String directionString;

    @Override
    public void setArrow(String direction) {
        label.setText("", "");
        super.setArrow(direction);
        usedItem = UsedItem.direction;
        if (direction != null) {
            this.directionString = direction;
        } else {
            this.directionString = "";
        }

        setName(Main.isRussian ? Progress.ruCageNames[3] : Progress.enCageNames[3]);
        Main.manual.addItem(this, imageUp, "", "");
    }

    private boolean flag = false;

    @Override
    public void act(float delta) {
        super.act(delta);
        borderImage.setVisible(!button.isChecked());
        if (!label.getText().toString().equals("")) {
            if (usedItem == UsedItem.direction || usedItem == UsedItem.bomb) {
                if (cageType != CageType.magic)
                    setImage(null);
                else {
                    setImage(Main.portalDrawable);
                }
            }

            if (!flag) {
                flag = true;
                setName(Main.isRussian ? Progress.ruCageNames[4] : Progress.enCageNames[4]);
                Main.manual.addItem(this, null, label.getValue(), label.getValue());
            }

            usedItem = UsedItem.distance;
        } else {
            flag = false;
        }
    }

    public void setBomb() {
        usedItem = Cage.UsedItem.bomb;
        setImage(Main.bombDrawable);
        label.setText("");

        setName(Main.isRussian ? Progress.ruCageNames[6] : Progress.enCageNames[6]);
        Main.manual.addItem(this, imageUp, "", "");
    }

    public int uncheckCount = 100;
    public int checkStep = 0;

    public void setMagic() {
        cageType = Cage.CageType.magic;
        button.setColor(DEFAULT_COLOR);
        setImage(Main.portalDrawable);
        setUncheckCount(1);

        setName(Main.isRussian ? Progress.ruCageNames[8] : Progress.enCageNames[8]);
        Main.manual.addItem(this, imageUp, "", "");
    }

    public void setUncheckCount(int uncheckCount) {
        this.uncheckCount = uncheckCount;
        checkStep = 0;
    }

}
