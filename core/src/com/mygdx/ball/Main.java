package com.mygdx.ball;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGeneratorLoader;
import com.badlogic.gdx.graphics.g2d.freetype.FreetypeFontLoader;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.mygdx.ball.Windows.AWindow;
import com.mygdx.ball.actor_classes.ADrawable;
import com.mygdx.ball.actor_classes.AImageButton;
import com.mygdx.ball.actor_classes.AImageTextButton;
import com.mygdx.ball.actor_classes.HelpAndExitRow;

import java.util.Locale;
import java.util.TreeMap;

public class Main extends Game {

	public static AdsController adsController;

	public Main(AdsController adsController) {
		Main.adsController = adsController;
	}

	public static float screenWidth;
	public static float screenHeight;
	public static boolean loading;

	public static AssetManager assetManager;
	public static Image loadingBackground;
	private SpriteBatch batch;
	public OrthographicCamera camera;

	private static TreeMap<String, BitmapFont> fonts;
	private static FreeTypeFontGenerator FontGenerator;
	private static final String fontChars = "/±ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzабвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ0123456789][_!$%#@|?-+=()&.;:,{}'<>\"";
	private static FreeTypeFontGenerator.FreeTypeFontParameter fontParameters;
	private final String FONT_PATH = "fonts//StenbergC.otf";

	public static ScrollPane.ScrollPaneStyle scrollPaneStyle;

	public static final String BLACK_SQUARE_PATH = "loading//black.png";

	public static final String CAGE_ANIMATION_PATH = "textures//cageAnimation//cage_0000";
	public static final String LONG_BUTTON_UP_PATH = "textures//white_button.png";
	public static final String LONG_BUTTON_PRESSED_PATH = "textures//white_button_pressed.png";
	public static final String BUTTON_UP_PATH = "textures//white.png";
	public static final String BUTTON_PRESSED_PATH = "textures//white_pressed.png";
	public static final String EXIT_ICON_PATH = "textures//exit_icon.png";
	public static final String HELP_ICON_PATH = "textures//help_icon.png";
	public static final String TITLE_PATH = "textures//label.png";
	public static final String BALL_PATH = "textures//newBall.png";
	public static final String BALL_MOVING_PATH = "textures//newBallMoving.png";
	public static final String BACKGROUND_PATH = "textures//background.png";
	public static final String PLATE_PATH = "textures//plate.png";
	public static final String ARROW_PATH_LEFT = "textures//arrow_left.png";
	public static final String ARROW_PATH_RIGHT = "textures//arrow_right.png";
	public static final String ARROW_PATH_UP = "textures//arrow_up.png";
	public static final String ARROW_PATH_DOWN = "textures//arrow_down.png";
	public static final String TOUCH_PATH = "textures//touch.png";
	public static final String TOUCH_UP_PATH = "textures//touch+.png";
	public static final String DISTANCE_PATH = "textures//distance.png";
	public static final String DISTANCE_UP_PATH = "textures//distance+.png";
	public static final String DIRECTION_PATH = "textures//direction.png";
	public static final String DIRECTION_UP_PATH = "textures//direction+.png";
	public static final String BOMB_PATH = "textures//bomb.png";
	public static final String EXIT_PATH = "textures//exit.png";
	public static final String BRICKS_PATH = "textures//bricks.png";
	public static final String PORTAL_PATH = "textures//portal.png";
	public static final String CHOSEN_PATH = "textures//chosen.png";
	public static final String FIRE_CAGE_PATH = "textures//fireCage.png";

	public static final String CHECKBOX_UNCHECKED_PATH = "textures//checkbox_unchecked.png";
	public static final String CHECKBOX_PRESSED_PATH = "textures//checkbox_pressed.png";
	public static final String CHECKBOX_CHECKED_PATH = "textures//checkbox_checked.png";

	public static final String SNOW_PATH = "textures//snow.png";
	public static final String FIRE_PATH = "textures//fire.png";
	public static final String WALL_PATH = "textures//wall.png";
	public static final String COLORS_PATH = "textures//colors.png";
	public static final String ITEMS_PATH = "textures//items.png";

	public static final String SHIELD_PATH = "textures//shield.png";

	public static final String SLIDER_PATH = "textures//slider.png";
	public static final String GOLD_STAR = "textures//goldStar.png";
	public static final String WHITE_STAR = "textures//whiteStar.png";
	public static final String CAGE_BORDER_PATH = "textures//open_cage.png";
	public static final String CAGE_BORDER_READY_BALL_PATH = "textures//ready_ball.png";
	public static final String COIN_PATH = "textures//coin.png";
	public static final String ESCAPED_BALL = "textures//escaped_ball.png";

	public static final String ARROW = "textures//arrow_";

	public static MenuScreen menuScreen;

	public static boolean enableVibration;
	public static boolean skipTutorials;
	public static boolean enableMusic;
	public static boolean enableSounds;

	public static Texture ballTexture;
	public static Texture movingBallTexture;
	public static SpriteDrawable backgroundDrawable;
	public static SpriteDrawable bombDrawable;
	public static SpriteDrawable exitDrawable;
	public static SpriteDrawable bricksDrawable;
	public static SpriteDrawable portalDrawable;
	public static SpriteDrawable fireCageDrawable;

	public static Drawable GOLD_STAR_DRAWABLE;
	public static Drawable WHITE_STAR_DRAWABLE;
	public static Drawable BALL_DRAWABLE;
	public static Drawable ESCAPED_BALL_DRAWABLE;
	public static Drawable CURRENT_DRAWABLE;
	public static Drawable LOCKED_DRAWABLE;

	public static SpriteDrawable[] bonuses;

	public static boolean isRussian;
	public static boolean infinityItems;

	public static AWindow helpWindow;
	public static Manual manual;

	public static MusicManager musicManager;

	public static float adsTimer = 0;

	private final static String SETTINGS_FILE_PATH = "settings";
	final static String PROGRESS_FILE_PATH = "progress";

	public static Integer clickCount = 0;

	public static float cageSize = 100;
	public static float cageSpace = 10;

	public static Sprite[] animationSprite;

	@Override
	public void create() {

		Gdx.input.setCatchKey(Input.Keys.BACK, true);
		String localeLanguage = Locale.getDefault().getLanguage();
		isRussian = localeLanguage.equals("ru");

		enableVibration = true;
		infinityItems = false;
		skipTutorials = false;
		enableMusic = true;
		enableSounds = true;

		loading = true;

		loadSettings();

		musicManager = new MusicManager(0.2f);

		screenWidth = 1920;
//		screenWidth = 1950;
//		screenHeight = 900;
		screenHeight = 1080;

		camera = new OrthographicCamera();
		camera.setToOrtho(false, screenWidth, screenHeight);
		batch = new SpriteBatch();

		assetManager = new AssetManager();
		assetManager.load(BALL_PATH, Texture.class);
		assetManager.load(EXIT_ICON_PATH, Texture.class);
		assetManager.load(TITLE_PATH, Texture.class);

		FileHandleResolver resolver = new InternalFileHandleResolver();
		assetManager.setLoader(FreeTypeFontGenerator.class, new FreeTypeFontGeneratorLoader(resolver));
		assetManager.setLoader(BitmapFont.class, ".otf", new FreetypeFontLoader(resolver));
		assetManager.load(FONT_PATH, FreeTypeFontGenerator.class);
		assetManager.load(BLACK_SQUARE_PATH, Texture.class);
		assetManager.load(BACKGROUND_PATH, Texture.class);
		assetManager.load(BUTTON_UP_PATH, Texture.class);
		assetManager.load(BUTTON_PRESSED_PATH, Texture.class);

		assetManager.finishLoading();

		AImageTextButton.setButtonsBackground(
				new SpriteDrawable(new Sprite(assetManager.get(BUTTON_UP_PATH, Texture.class))),
				new SpriteDrawable(new Sprite(assetManager.get(BUTTON_PRESSED_PATH, Texture.class))),
				new SpriteDrawable(new Sprite(assetManager.get(BUTTON_PRESSED_PATH, Texture.class)))
		);

		AImageButton.setButtonsBackground(
				new SpriteDrawable(new Sprite(assetManager.get(BUTTON_UP_PATH, Texture.class))),
				new SpriteDrawable(new Sprite(assetManager.get(BUTTON_PRESSED_PATH, Texture.class))),
				new SpriteDrawable(new Sprite(assetManager.get(BUTTON_PRESSED_PATH, Texture.class)))
		);

		fontParameters = new FreeTypeFontGenerator.FreeTypeFontParameter();
		FontGenerator = assetManager.get(FONT_PATH);
		ballTexture = assetManager.get(BALL_PATH, Texture.class);

		Sprite sprite = new Sprite(assetManager.get(BACKGROUND_PATH, Texture.class));
		sprite.setColor(Color.BLACK);
		backgroundDrawable = new SpriteDrawable(sprite);

		fonts = new TreeMap<>();

		scrollPaneStyle = new ScrollPane.ScrollPaneStyle(
				null,
				null,
				null,
				null,
				null
		);

		loadingBackground = new Image(new SpriteDrawable(new Sprite((Texture) assetManager.get(BLACK_SQUARE_PATH))));
		loadingBackground.setBounds(0, 0, screenWidth, screenHeight);
		loadingBackground.setColor(0, 0, 0, 0.5f);

		loadSecondarySources();

		menuScreen = new MenuScreen(this, () -> {

			animationSprite = new Sprite[14];
			World.cageAnimDrawables = new SpriteDrawable[14];
			for (int i = 1; i <= 14; ++i) {
				String path = CAGE_ANIMATION_PATH + (i < 10 ? "0" : "") + i + ".png";
				animationSprite[i - 1] = new Sprite(assetManager.get(path, Texture.class));
				World.cageAnimDrawables[i - 1] = new SpriteDrawable(animationSprite[i - 1]);
			}

			SpriteDrawable[][] spriteDrawables = {
					{
							new SpriteDrawable(new Sprite(assetManager.get(TOUCH_PATH, Texture.class))),
							new SpriteDrawable(new Sprite(assetManager.get(TOUCH_UP_PATH, Texture.class))),
							new ADrawable(new Sprite(assetManager.get(TOUCH_UP_PATH, Texture.class)), Color.RED)
					},
					{
							new SpriteDrawable(new Sprite(assetManager.get(DIRECTION_PATH, Texture.class))),
							new SpriteDrawable(new Sprite(assetManager.get(DIRECTION_UP_PATH, Texture.class))),
							new ADrawable(new Sprite(assetManager.get(DIRECTION_UP_PATH, Texture.class)), Color.RED)
					},
					{
							new SpriteDrawable(new Sprite(assetManager.get(DISTANCE_PATH, Texture.class))),
							new SpriteDrawable(new Sprite(assetManager.get(DISTANCE_UP_PATH, Texture.class))),
							new ADrawable(new Sprite(assetManager.get(DISTANCE_UP_PATH, Texture.class)), Color.RED)
					}
			};

			bonuses = new SpriteDrawable[] {
					new SpriteDrawable(new Sprite(assetManager.get(SNOW_PATH, Texture.class))),
					new SpriteDrawable(new Sprite(assetManager.get(FIRE_PATH, Texture.class))),
					new SpriteDrawable(new Sprite(assetManager.get(WALL_PATH, Texture.class))),
					new SpriteDrawable(new Sprite(assetManager.get(COLORS_PATH, Texture.class))),
					new SpriteDrawable(new Sprite(assetManager.get(ITEMS_PATH, Texture.class))),
					bombDrawable
			};

			Progress.setDrawables(spriteDrawables, bonuses);

			bombDrawable = new SpriteDrawable(new Sprite(assetManager.get(BOMB_PATH, Texture.class)));
			exitDrawable = new SpriteDrawable(new Sprite(assetManager.get(EXIT_PATH, Texture.class)));
			bricksDrawable = new SpriteDrawable(new Sprite(assetManager.get(BRICKS_PATH, Texture.class)));
			portalDrawable = new SpriteDrawable(new Sprite(assetManager.get(PORTAL_PATH, Texture.class)));
			fireCageDrawable = new SpriteDrawable(new Sprite(assetManager.get(FIRE_CAGE_PATH, Texture.class)));

			GOLD_STAR_DRAWABLE = new SpriteDrawable(new Sprite(Main.assetManager.get(Main.GOLD_STAR, Texture.class)));
			WHITE_STAR_DRAWABLE = new SpriteDrawable(new Sprite(Main.assetManager.get(Main.WHITE_STAR, Texture.class)));

			BALL_DRAWABLE = new SpriteDrawable(new Sprite(Main.ballTexture));
			CURRENT_DRAWABLE = new SpriteDrawable(new Sprite(Main.assetManager.get(Main.TOUCH_PATH, Texture.class)));
			LOCKED_DRAWABLE = new SpriteDrawable(new Sprite(Main.assetManager.get(Main.HELP_ICON_PATH, Texture.class)));
			ShopItem.COIN_DRAWABLE = new SpriteDrawable(new Sprite(Main.assetManager.get(Main.COIN_PATH, Texture.class)));
			ESCAPED_BALL_DRAWABLE = new ADrawable(new Sprite(Main.assetManager.get(Main.ESCAPED_BALL, Texture.class)), Progress.colors[1]);
			movingBallTexture = assetManager.get(Main.BALL_MOVING_PATH, Texture.class);

			Cage.cageBorder = new SpriteDrawable(new Sprite(assetManager.get(CAGE_BORDER_PATH, Texture.class)));
			Cage.cageBorderReadyBall = new SpriteDrawable(new Sprite(assetManager.get(CAGE_BORDER_READY_BALL_PATH, Texture.class)));
			HelpAndExitRow.setBackground(
					new SpriteDrawable(new Sprite(assetManager.get(EXIT_ICON_PATH, Texture.class))),
					null,
					new SpriteDrawable(new Sprite(assetManager.get(HELP_ICON_PATH, Texture.class))),
					null
			);

			helpWindow = new AWindow(
					"Manual", "Справочник", "", "",
					manual = new Manual(screenWidth * 0.9f, screenHeight * 0.6f, 80, Color.WHITE),
					1f, 1f, true, 100, 100, 0, 0, 0, 0
			);
			manual.setWindow(helpWindow);

			menuScreen.loadingFinished();
		});
		setScreen(menuScreen);

		System.out.println("Okay");
	}

	private void loadSecondarySources() {
		assetManager.load(LONG_BUTTON_UP_PATH, Texture.class);
		assetManager.load(LONG_BUTTON_PRESSED_PATH, Texture.class);
		assetManager.load(HELP_ICON_PATH, Texture.class);
		assetManager.load(PLATE_PATH, Texture.class);
		assetManager.load(ARROW_PATH_LEFT, Texture.class);
		assetManager.load(ARROW_PATH_UP, Texture.class);
		assetManager.load(ARROW_PATH_RIGHT, Texture.class);
		assetManager.load(ARROW_PATH_DOWN, Texture.class);
		assetManager.load(TOUCH_PATH, Texture.class);
		assetManager.load(TOUCH_UP_PATH, Texture.class);
		assetManager.load(DISTANCE_PATH, Texture.class);
		assetManager.load(DISTANCE_UP_PATH, Texture.class);
		assetManager.load(DIRECTION_PATH, Texture.class);
		assetManager.load(DIRECTION_UP_PATH, Texture.class);
		assetManager.load(SLIDER_PATH, Texture.class);
		assetManager.load(GOLD_STAR, Texture.class);
		assetManager.load(WHITE_STAR, Texture.class);
		assetManager.load(BOMB_PATH, Texture.class);
		assetManager.load(PORTAL_PATH, Texture.class);
		assetManager.load(EXIT_PATH, Texture.class);
		assetManager.load(BRICKS_PATH, Texture.class);

		assetManager.load(SNOW_PATH, Texture.class);
		assetManager.load(FIRE_PATH, Texture.class);
		assetManager.load(COLORS_PATH, Texture.class);
		assetManager.load(WALL_PATH, Texture.class);
		assetManager.load(ITEMS_PATH, Texture.class);
		assetManager.load(CHOSEN_PATH, Texture.class);
		assetManager.load(FIRE_CAGE_PATH, Texture.class);

		assetManager.load(CHECKBOX_UNCHECKED_PATH, Texture.class);
		assetManager.load(CHECKBOX_PRESSED_PATH, Texture.class);
		assetManager.load(CHECKBOX_CHECKED_PATH, Texture.class);
		assetManager.load(SHIELD_PATH, Texture.class);
		assetManager.load(CAGE_BORDER_PATH, Texture.class);
		assetManager.load(CAGE_BORDER_READY_BALL_PATH, Texture.class);
		assetManager.load(COIN_PATH, Texture.class);
		assetManager.load(ESCAPED_BALL, Texture.class);
		assetManager.load(BALL_MOVING_PATH, Texture.class);

		for (int i = 1; i <= 14; ++i) {
			String path = CAGE_ANIMATION_PATH + (i < 10 ? "0" : "") + i + ".png";
			assetManager.load(path, Texture.class);
		}
	}

	private void loadSettings() {
		FileHandle saveFile = Gdx.files.local(SETTINGS_FILE_PATH);
		if (saveFile.exists()) {
			System.out.println("settings file exists");
			String[] strings = saveFile.readString().split("\n");
			try {
				enableVibration = Boolean.parseBoolean(strings[0]);
				skipTutorials = !Boolean.parseBoolean(strings[1]);
				enableMusic = Boolean.parseBoolean(strings[2]);
				enableSounds = Boolean.parseBoolean(strings[3]);
				isRussian = Boolean.parseBoolean(strings[4]);
			} catch (ArrayIndexOutOfBoundsException e) {
				e.printStackTrace();
			}
		} else {
			saveSettings();
		}
	}

	public static void saveSettings() {
		FileHandle saveFile = Gdx.files.local(SETTINGS_FILE_PATH);
		saveFile.writeString(enableVibration + "\n", false);
		saveFile.writeString(!skipTutorials + "\n", true);
		saveFile.writeString(enableMusic + "\n", true);
		saveFile.writeString(enableSounds + "\n", true);
		saveFile.writeString(isRussian + "\n", true);
	}

	@Override
	public void dispose() {
		super.dispose();
	}

	@Override
	public void pause() {
		super.pause();
	}

	@Override
	public void resume() {
		super.resume();
	}

	@Override
	public void render() {
		super.render();
		adsTimer +=Gdx.graphics.getDeltaTime();
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}

	@Override
	public void setScreen(Screen screen) {
		super.setScreen(screen);
	}

	@Override
	public Screen getScreen() {
		return super.getScreen();
	}

	public static BitmapFont getFont(int size, Color color) {
		String params = size + " " + color.toString();
		BitmapFont font = fonts.get(params);
		if (font == null) {
			System.out.println("NEW FONT");
			font = getFont(params, 0, null);
			fonts.put(params, font);
		}
		return font;
	}

	public static BitmapFont getFont(String str, float borderWidth, Color borderColor) {

		str = str + " " + borderWidth + " " + borderColor;
		BitmapFont font = fonts.get(str);
		if (font == null) {
			System.out.println("NEW BORDERED FONT: " + str + " " + borderWidth + " " + (borderColor != null ?borderColor.toString() : null));

			String[] params = str.split(" ");
			fontParameters.size = Integer.parseInt(params[0]);
			fontParameters.color = Color.valueOf(params[1]);
			fontParameters.characters = fontChars;
			fontParameters.borderWidth = borderWidth;
			fontParameters.borderColor = borderColor;

			font = FontGenerator.generateFont(fontParameters);
			fonts.put(str, font);
		}

		return font;

	}

	// очистить экран белым цветом
	private static void clearScreenWhite() {
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	}

	// скоординировать камеру
	private void setCamera() {
		camera.update();
		batch.setProjectionMatrix(camera.combined);
	}

	// подготовить область для отрисовки с белым фоном
	public void readyWhite() {
		setCamera();
		clearScreenWhite();
	}

	public static void vibrate(int milliseconds) {
		if (enableVibration && (milliseconds < 100 || !enableMusic)) {
			Gdx.input.vibrate(milliseconds);
		}
	}

	public static void showAd() {
		if (!infinityItems && adsTimer > 140) {
			adsTimer = 0;
			// HERE!!!
//			adsController.showVideoAd();
		}
	}
}
