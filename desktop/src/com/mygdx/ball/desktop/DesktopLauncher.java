package com.mygdx.ball.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.mygdx.ball.Main;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 683;
//		config.width = 1366;
//		config.height = 768;
		config.height = 384;
		new LwjglApplication(new Main(null), config);
	}
}
