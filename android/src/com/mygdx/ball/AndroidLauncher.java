package com.mygdx.ball;

import android.os.Bundle;
import android.widget.Toast;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

public class AndroidLauncher extends AndroidApplication implements AdsController, RewardedVideoAdListener {

	private RewardedVideoAd rewardedVideoAd;
	private InterstitialAd interstitialAd;

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		initialize(new Main(this), config);

		MobileAds.initialize(this, "ca-app-pub-2362266128502510~8420721121");

		interstitialAd = new InterstitialAd(this);
		interstitialAd.setAdUnitId("ca-app-pub-3940256099942544/8691691433");

		rewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this);
		rewardedVideoAd.setRewardedVideoAdListener(this);
		loadRewardedVideoAd();
		loadVideoAd();
	}

	@Override
	public void onRewardedVideoAdLoaded() {
//		Toast.makeText(this, "Ad loaded", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onRewardedVideoAdOpened() {

	}

	@Override
	public void onRewardedVideoStarted() {

	}

	@Override
	public void onRewardedVideoAdClosed() {
		loadRewardedVideoAd();
	}

	@Override
	public void onRewarded(RewardItem rewardItem) {
		Toast.makeText(this, "Reward: " + rewardItem.getAmount() + " coins!", Toast.LENGTH_SHORT).show();
		Progress.money += rewardItem.getAmount();
		Main.menuScreen.progress.saveProgress();
		Main.menuScreen.showShopWindow();
	}

	@Override
	public void onRewardedVideoAdLeftApplication() {

	}

	@Override
	public void onRewardedVideoAdFailedToLoad(int i) {

	}

	@Override
	public void onRewardedVideoCompleted() {

	}

	@Override
	public void showRewardedVideoAd() {
		runOnUiThread(
				() -> {
					if (rewardedVideoAd.isLoaded()) {
						rewardedVideoAd.show();
					} else {
						loadRewardedVideoAd();
					}
				}
		);
	}

	@Override
	public void loadRewardedVideoAd() {
		rewardedVideoAd.loadAd("ca-app-pub-3940256099942544/5224354917", new AdRequest.Builder().build());
	}

	@Override
	public void showVideoAd() {
		runOnUiThread(
				() -> {
					if (interstitialAd.isLoaded()) {
						interstitialAd.show();
					} else {
						loadVideoAd();
					}
				}
		);
	}

	@Override
	public void loadVideoAd() {
		interstitialAd.loadAd(new AdRequest.Builder().build());
	}
}
